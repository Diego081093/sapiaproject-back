# Register

Used to collect a Token for a registered User.

**URL** : `/register/`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
  "username": "[valid email address or dni number]",
  "password": "[password in plain text]",
  "phonenumber": "[valid phone number]",
  "role": "[valid slug role]"
}
```

**Data example**

```json
{
  "username": "76585686",
  "password": "abcd1234",
  "phonenumber": "934234121",
  "role": "P"
}
```

```json
{
  "username": "iloveauth@example.com",
  "password": "abcd1234",
  "role": "S"
}
```

```json
{
  "username": "iloveauth@example.com",
  "password": "abcd1234",
  "role": "H"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "token": "93144b288eb1fdccbe46d6fc0f241a51766ecd3d"
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to register with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["EndPoint not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
