# Update Password

Service to update user password

**URL** : `/password`

**Method** : `PUT`

**Auth required** : No

**Data constraints**

```json
{
    "username":"marco.condoric@gmail.com",
    "password":"abc",
    "password_new":"123"
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Password Update!",
    "data": {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoyLCJpZFJvbGUiOjIsImlkUHJvZmlsZSI6Miwic2x1ZyI6InNlZC1hbmltaS1hc3N1bWVuZGEiLCJmaXJzdG5hbWUiOiJBbGRlbiIsImxhc3RuYW1lIjoiV2FyZCIsImRuaSI6IjI1ODA5MzkwIiwiZW1haWwiOiJGaWRlbF9Nb3NjaXNraTg5QHlhaG9vLmNvbSIsInBob25lIjoiOTYwNTA2NjIzIiwiZ2VuZGVyIjoiTSIsImJpcnRoZGF5IjoiMjAyMS0wNC0xMyIsImF2YXRhciI6Imh0dHBzOi8vbW90dGl2YS5nbG9ib2F6dWwucGUvaW1hZ2VzLWFwcC9BVkFUQVJTL2F2YXRhcl8yLnBuZyIsInByb2ZpbGVzIjpbeyJpZCI6MiwibmFtZSI6IkFsZGVuIiwiQXZhdGFyIjp7ImlkIjoyLCJpbWFnZSI6Imh0dHBzOi8vbW90dGl2YS5nbG9ib2F6dWwucGUvaW1hZ2VzLWFwcC9BVkFUQVJTL2F2YXRhcl8yLnBuZyJ9LCJVc2VyIjp7ImlkIjoyLCJ1c2VybmFtZSI6Im1hcmNvLmNvbmRvcmljQGdtYWlsLmNvbSJ9fV19LCJpYXQiOjE2MTg0MTgyMTF9.Cwy92L7nEAul0HnONfd-2TnDlcze2ZbpcOILHjNmm-E"
    }
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "message": "Incorrect Password!"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```