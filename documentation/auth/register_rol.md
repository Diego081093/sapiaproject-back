# Rol

Used to create user role.

**URL** : `/rol/`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
  "name": "[valid name rol]",
  "slug": "[slugname of one letter]"
  /**
   * P : Patient
   * S : Psychologist
   * H : Help desk
   */
}
```

**Data example**

```json
{
  "name": "Patient",
  "slug": "P"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "message": "rol created"
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to register with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["EndPoint not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
