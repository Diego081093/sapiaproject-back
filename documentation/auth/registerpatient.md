# Register Patient

Used to collect a Token for a registered User.

**URL** : `/patient`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json           
                "username": "[valid email address or dni number]" ,
                "password": "[password in plain text]",
                "firstname": "[valid name in text]",
                "phone" :  "[valid phone number]", 
                "avatar_id":"[valid id avartar]",
                "secret_question_id":"[valid secret question]" ,
                "secret_question_answer": "[valid answer in plain text]"
}
```

**Data example**

```json
{
    "message": "profile created",
    "data": {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxMSwiaWRSb2xlIjoxLCJpZFByb2ZpbGUiOjExLCJ1c2VybmFtZSI6IjQ4MzEwMTgyIiwic2x1ZyI6bnVsbCwiZmlyc3RuYW1lIjoiZGllZ28iLCJsYXN0bmFtZSI6bnVsbCwiZG5pIjpudWxsLCJlbWFpbCI6bnVsbCwicGhvbmUiOiI5NjA1MDY2MjMiLCJnZW5kZXIiOm51bGwsImJpcnRoZGF5IjpudWxsLCJhdmF0YXIiOiJodHRwczovL21vdHRpdmEuZ2xvYm9henVsLnBlL2ltYWdlcy1hcHAvQVZBVEFSUy9hdmF0YXJfMi5wbmciLCJwcm9maWxlcyI6W3siaWQiOjExLCJuYW1lIjoiZGllZ28iLCJBdmF0YXIiOnsiaWQiOjIsImltYWdlIjoiaHR0cHM6Ly9tb3R0aXZhLmdsb2JvYXp1bC5wZS9pbWFnZXMtYXBwL0FWQVRBUlMvYXZhdGFyXzIucG5nIn0sIlVzZXIiOnsiaWQiOjExLCJ1c2VybmFtZSI6IjQ4MzEwMTgyIn19XX0sImlhdCI6MTYxNzk5OTExNn0.xb6BGjtdO7Dq8m_VcQn5pKowrHPM-TRG0ePfrp3rjN0"
    }
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "messag": "profile created created"
}
```
**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}