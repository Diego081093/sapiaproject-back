# Psycology

Service to get information of the psycology

**URL** : `/psycology`

**Method** : `GET`

**Auth required** : YES Bearer Token

**Data constraints**

```json
{
  "Bearer Token": "[GciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoyLCJ1c2VybmFtZSI6Im1hcmNvLmNvbmRvcmljQGdtYWlsLmNvbSJ9LCJpYXQiOjE2MTY]"

}
```

**Data example**

```json
{
    "data": {
        "psycology": {
            "id": "Integer(id)",
            "firstname": "String",
            "lastname": "String",
            "dni": "Number",
            "gender": "M | F",
            "birthday": "Date",
            "email": "String",
            "tbl_profile_attribute": {
                "availability": "json"
            }
        },
        "specialties": [
            {
                "name": "especialidad",
                "alias": "alias de la especialidad"
            },
            {
                "name": "especialidad",
                "alias": "alias de la especialidad"
            }
        ],
        "attributes": {
            "video": "url video",
            "imagen": "url image"
        }
    }
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": {
        "id": 2,
        "firstname": "Constance",
        "specialties": [
            {
                "name": "Familia",
                "alias": "F"
            },
            {
                "name": "Amor",
                "alias": "A"
            }
        ],
        "attributes": {
            "video": "video demo",
            "imagen": "imagen demo"
        }
    }
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : 

**Content** :

```json
{
  
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
 
}
```