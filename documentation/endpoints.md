# RESTAPIDocs Mottiva

These mottiva were taken from projects mainly using [Nodejs Rest
Framework](https://github.com/tomchristie/django-rest-framework) and so the
JSON responses are often similar to the way in which DRF makes responses.

Where full URLs are provided in responses they will be rendered as if service
is running on 'http://testserver/'.

## Open Endpoints

Open endpoints require no Authentication.

- [Login](auth/login.md) : `POST /login/`
- [Register](auth/register.md) : `POST register/`
- [Register patient](auth/registerpatient.md): `POST /patient`
- [Verify dni](auth/verify_dni.md) : `POST /dni/`
- [Verify number](auth/verify_number.md) : `POST /verification/`
- [avatar](catalog/avatar.md) : `GET /avatar/`
- [Secretquestion](catalog/secretquestion.md) : `GET /SecretQuestion/:dni`
- [Validate Secret Question](catalog/validate_secret_question.md) : `POST /SecretQuestion/validate`



## Endpoints that require Authentication

Closed endpoints require a valid Token to be included in the header of the
request. A Token can be acquired from the Login view above.
- [Role List](catalog/roleList.md) : `GET /role`
- [Role Create](catalog/roleCreate.md) : `POST /role`
- [Role Delete](catalog/roleDelete.md) : `DELETE /role/:id`
- [Session](catalog/session.md) : `GET /session/`
- [Room Emotions](catalog/room.emotions.md) : `GET /room`
- [Psychologist List](catalog/psycologyList.md) : `GET /psycology/list`
- [Update Psychologist](catalog/update_psychologist.md) : `PUT /psycology`
- [Update Phone](catalog/update_phone.md) : `PUT /update-phone`
- [Media List](catalog/media.list.md) : `GET /media`
- [Media Detail](catalog/media.detail.md) : `GET /media/{media_id}`
- [Media Favorite](catalog/media.favorite.md) : `POST /media/favorite`
- [Desactivate Media Favorite](catalog/desactivate_media.favorite.md) : `POST /media/desactivate_favorite`
- [Notes](catalog/notes.md) : `GET /notes/`
- [Patients_by](catalog/patients_by.md) : `GET /patient-by/`
- [Watch Later](catalog/watch_later.md) : `POST /media/:id/watch-later`
- [Benefit](catalog/benefits.md) : `GET /benefit`
- [Category Media List](catalog/category.media.list) : `GET /category/media`
- [Media Done](catalog/media.done.md) : `POST /media/done`
- [Media History](catalog/media.history.md) : `GET /media`
- [Method Media List](catalog/method.media.list.md) : `GET /method`
- [Poll](catalog/poll.md) : `POST /poll`
- [Schedule](catalog/schedule.md) : `GET /schedule`
- [Schedule History](catalog/schedule/schedule.history.md) : `GET /schedule/history`
- [Room Details](catalog/task.details.md) : `GET /room/details`
- [Create Blocked](catalog/blocked.md) : `POST /blocked`
- [Create or Update Diary](catalog/diary.md) : `PUT /diary`



### Current User related

Each endpoint manipulates or displays information related to the User whose
Token is provided with the request:

- [Show info](user/get.md) : `GET /api/user/`
- [Update info](user/put.md) : `PUT /api/user/`

### Account related

Endpoints for viewing and manipulating the Accounts that the Authenticated User
has permissions to access.

- [Show Accessible Accounts](accounts/get.md) : `GET /api/accounts/`
- [Create Account](accounts/post.md) : `POST /api/accounts/`
- [Show An Account](accounts/pk/get.md) : `GET /api/accounts/:pk/`
- [Update An Account](accounts/pk/put.md) : `PUT /api/accounts/:pk/`
- [Delete An Account](accounts/pk/delete.md) : `DELETE /api/accounts/:pk/`
