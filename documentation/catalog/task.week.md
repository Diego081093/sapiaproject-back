# List tasks by week

Service to list tasks by week

**URL** : `/task/week/:week`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
    "week": "1"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Tasks found"
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "message": "Access denied with provided credentials"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```