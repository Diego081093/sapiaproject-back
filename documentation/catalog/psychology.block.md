# Block Psychologist

Service to block psychologist data

**URL** : `/psycology/:id/block`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{
    "reasonId": "32",
    "comment": "Lorem ipsum dolor sit amet"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Psychologist blocked"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```