# Room Create

It is used to created new room

**URL** : `/room`

**Method** : `POST`

**Auth required** : TOKEN



**Data example**

```json
{
    "tittle":"Control de Emociones",
    "start": "2021-04-21 15:36:27",
    "end": "2021-04-24 15:36:27",
    "rules": "controlar sus emociones",
    "participants": "7",
    "image":"https://www.mottiva.pe/imagenes/room/1.png"
   
}
```

## Success Response

**Code** : `200 OK`

**Content example**

### Psychologist data

// actual session
 // total sesions
```json
{
    "message": "Rooms created",
    "room": {
        "sid": "RM26da9b286d44a9c097c7f97b92dbaed8",
        "status": "in-progress",
        "dateCreated": "2021-04-22T14:59:27.000Z",
        "dateUpdated": "2021-04-22T14:59:27.000Z",
        "accountSid": "AC5ca0cbf2ffee60449e85f9fb88cb4d6e",
        "enableTurn": true,
        "uniqueName": "room-r-15",
        "statusCallback": null,
        "statusCallbackMethod": "POST",
        "endTime": null,
        "duration": null,
        "type": "group",
        "maxParticipants": 7,
        "maxConcurrentPublishedTracks": 170,
        "recordParticipantsOnConnect": false,
        "videoCodecs": [
            "VP8",
            "H264"
        ],
        "mediaRegion": "us1",
        "url": "https://video.twilio.com/v1/Rooms/RM26da9b286d44a9c097c7f97b92dbaed8",
        "links": {
            "recordings": "https://video.twilio.com/v1/Rooms/RM26da9b286d44a9c097c7f97b92dbaed8/Recordings",
            "participants": "https://video.twilio.com/v1/Rooms/RM26da9b286d44a9c097c7f97b92dbaed8/Participants",
            "recording_rules": "https://video.twilio.com/v1/Rooms/RM26da9b286d44a9c097c7f97b92dbaed8/RecordingRules"
        }
    }
}
```



## Error Response

**Condition** : If no sessions are found as helpdesk

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "message": "No sessions were found"
}
```


**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
