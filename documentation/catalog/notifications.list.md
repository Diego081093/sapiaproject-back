# List Notifications

Service to list notificiations

**URL** : `/notification`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
    "filter": "Lorem" // Filter by key attribute
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Notifications found",
    "data": [
        {
            "id": 4,
            "type": "voluptatem",
            "title": "Exercitationem ullam eaque.",
            "description": "Voluptatem possimus error est nostrum rerum pariatur. Dolor ducimus et dolor.",
            "state": 1,
            "created_at": "2021-06-11T18:47:35.357Z",
            "NotificationAttributes": [
                {
                    "key": "veniam",
                    "value": "quia"
                }
            ]
        },
        {
            "id": 3,
            "type": "recusandae",
            "title": "Praesentium voluptatem nobis.",
            "description": "Omnis odio reprehenderit dolorem voluptatem incidunt sed illum molestiae. Repellat voluptates non sit sunt accusantium nam.",
            "state": 1,
            "created_at": "2021-06-11T18:46:35.357Z",
            "NotificationAttributes": [
                {
                    "key": "distinctio",
                    "value": "consequatur"
                }
            ]
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```