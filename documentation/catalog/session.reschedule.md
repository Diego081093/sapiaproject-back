# Reschedule session

Service to reschedule session

**URL** : `/sessions/:id/reschedule`

**Method** : `PUT`

**Auth required** : YES

**Data constraints**

```json
{
    "date": "2021-03-25 12:23:27"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Session updated"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```