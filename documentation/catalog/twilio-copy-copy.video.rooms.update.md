# Update Psychologist

Service to create

**URL** : `/twilio/video/rooms/:sid`

**Method** : `PUT`

**Auth required** : YES

**Data constraints**

- **status**: Estado de la sala

  - completed: Se termina la sala y los participantes son expulsados

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Rooms updated",
    "room": {
        "sid": "RMf893444405bf2e815752e5838469df8d",
        "status": "completed",
        "dateCreated": "2021-04-15T22:42:01.000Z",
        "dateUpdated": "2021-04-15T22:42:01.000Z",
        "accountSid": "AC74b4b32e991393aa9bbdcc99b953265d",
        "enableTurn": true,
        "uniqueName": "RMf893444405bf2e815752e5838469df8d",
        "statusCallback": null,
        "statusCallbackMethod": "POST",
        "endTime": "2021-04-15T22:42:51.000Z",
        "duration": 50,
        "type": "go",
        "maxParticipants": 2,
        "maxConcurrentPublishedTracks": 0,
        "recordParticipantsOnConnect": false,
        "videoCodecs": null,
        "mediaRegion": null,
        "url": "https://video.twilio.com/v1/Rooms/RMf893444405bf2e815752e5838469df8d",
        "links": {
            "recordings": "https://video.twilio.com/v1/Rooms/RMf893444405bf2e815752e5838469df8d/Recordings",
            "participants": "https://video.twilio.com/v1/Rooms/RMf893444405bf2e815752e5838469df8d/Participants",
            "recording_rules": "https://video.twilio.com/v1/Rooms/RMf893444405bf2e815752e5838469df8d/RecordingRules"
        }
    }
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```