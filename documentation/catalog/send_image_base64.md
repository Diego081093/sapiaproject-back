# Send Image

Service to get avatar and image

**URL** : `/send-image/base64`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
  "image": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQA..." // File to upload base64 encoded
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "File uploaded"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
