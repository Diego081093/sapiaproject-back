# Update Psychologist

Service to update psychologist data

**URL** : `/twilio/video/rooms`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Rooms found",
    "rooms": [
        {
            "sid": "RMde601adf14e192e17c6fadea09a7acf4",
            "status": "in-progress",
            "dateCreated": "2021-04-15T22:54:01.000Z",
            "dateUpdated": "2021-04-15T22:54:01.000Z",
            "accountSid": "AC74b4b32e991393aa9bbdcc99b953265d",
            "enableTurn": true,
            "uniqueName": "RMde601adf14e192e17c6fadea09a7acf4",
            "statusCallback": null,
            "statusCallbackMethod": "POST",
            "endTime": null,
            "duration": null,
            "type": "go",
            "maxParticipants": 2,
            "maxConcurrentPublishedTracks": 0,
            "recordParticipantsOnConnect": false,
            "videoCodecs": null,
            "mediaRegion": null,
            "url": "https://video.twilio.com/v1/Rooms/RMde601adf14e192e17c6fadea09a7acf4",
            "links": {
                "recordings": "https://video.twilio.com/v1/Rooms/RMde601adf14e192e17c6fadea09a7acf4/Recordings",
                "participants": "https://video.twilio.com/v1/Rooms/RMde601adf14e192e17c6fadea09a7acf4/Participants",
                "recording_rules": "https://video.twilio.com/v1/Rooms/RMde601adf14e192e17c6fadea09a7acf4/RecordingRules"
            }
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```