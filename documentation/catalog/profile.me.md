# Get my profile

Service to get my profile data

**URL** : `/profile/me`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Profile found",
    "data": {
        "id": 2,
        "tbl_role_id": 2,
        "tbl_user_id": 2,
        "tbl_avatar_id": 2,
        "tbl_district_id": 2,
        "slug": "maiores-sed-qui",
        "firstname": "Prueba",
        "lastname": "Tres",
        "dni": "12872177",
        "email": "Tom_Weimann23@gmail.com",
        "phone": "945502075",
        "gender": "F", // Genero F(Femenino) y M(Masculino)
        "birthday": "2021-05-03", // Fecha de nacimiento y edad
        "state": 1,
        "req_sent_at": null, // Fecha de envio de solicitud
        "req_state": false, // Estado de solicitud
        "req_approved_at": null, // Fecha de aprobacion de solicitud
        "created_by": "root",
        "createdAt": "2021-05-03T17:24:25.185Z",
        "edited_by": "root",
        "updatedAt": "2021-05-03T17:24:35.895Z",
        "Avatar": {
            "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_2.png"
        },
        "ProfileAttribute": [ // Atributos
            {
                "key": "video",
                "value": "https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5"
            },
            {
                "key": "image",
                "value": "https://mottiva.globoazul.pe/images-app/IMG_PERFIL_APP_USUARIO/illus_sesion_perfil.svg"
            },
            {
                "key": "available", // Disponibilidad
                "value": "[{\"days\":\"monday\",\"hours\":[{\"name\":\"06:00 - 06:45\",\"state\":0},{\"name\":\"06:45 - 07:30\",\"state\":1},{\"name\":\"07:30 - 08:15\",\"state\":1},{\"name\":\"08:15 - 09:00\",\"state\":0},{\"name\":\"09:00 - 09:45\",\"state\":1},{\"name\":\"09:45 - 10:30\",\"state\":1},{\"name\":\"10:30 - 11:15\",\"state\":0},{\"name\":\"11:15 - 12:00\",\"state\":0},{\"name\":\"12:00 - 12:45\",\"state\":0},{\"name\":\"12:45 - 13:30\",\"state\":0},{\"name\":\"13:30 - 14:15\",\"state\":1},{\"name\":\"14:15 - 15:00\",\"state\":1},{\"name\":\"15:00 - 15:45\",\"state\":0},{\"name\":\"15:45 - 16:30\",\"state\":0},{\"name\":\"16:30 - 17:15\",\"state\":1},{\"name\":\"17:15 - 18:00\",\"state\":1},{\"name\":\"18:00 - 18:45\",\"state\":1},{\"name\":\"18:45 - 19:30\",\"state\":0},{\"name\":\"19:30 - 20:15\",\"state\":1},{\"name\":\"20:15 - 21:00\",\"state\":0},{\"name\":\"21:00 - 21:45\",\"state\":1}]},{\"days\":\"tuesday\",\"hours\":[{\"name\":\"06:00 - 06:45\",\"state\":0},{\"name\":\"06:45 - 07:30\",\"state\":0},{\"name\":\"07:30 - 08:15\",\"state\":1},{\"name\":\"08:15 - 09:00\",\"state\":0},{\"name\":\"09:00 - 09:45\",\"state\":0},{\"name\":\"09:45 - 10:30\",\"state\":1},{\"name\":\"10:30 - 11:15\",\"state\":0},{\"name\":\"11:15 - 12:00\",\"state\":1},{\"name\":\"12:00 - 12:45\",\"state\":1},{\"name\":\"12:45 - 13:30\",\"state\":0},{\"name\":\"13:30 - 14:15\",\"state\":0},{\"name\":\"14:15 - 15:00\",\"state\":0},{\"name\":\"15:00 - 15:45\",\"state\":0},{\"name\":\"15:45 - 16:30\",\"state\":0},{\"name\":\"16:30 - 17:15\",\"state\":1},{\"name\":\"17:15 - 18:00\",\"state\":0},{\"name\":\"18:00 - 18:45\",\"state\":1},{\"name\":\"18:45 - 19:30\",\"state\":1},{\"name\":\"19:30 - 20:15\",\"state\":0},{\"name\":\"20:15 - 21:00\",\"state\":1},{\"name\":\"21:00 - 21:45\",\"state\":0}]},{\"days\":\"wednesday\",\"hours\":[{\"name\":\"06:00 - 06:45\",\"state\":1},{\"name\":\"06:45 - 07:30\",\"state\":0},{\"name\":\"07:30 - 08:15\",\"state\":1},{\"name\":\"08:15 - 09:00\",\"state\":1},{\"name\":\"09:00 - 09:45\",\"state\":1},{\"name\":\"09:45 - 10:30\",\"state\":1},{\"name\":\"10:30 - 11:15\",\"state\":0},{\"name\":\"11:15 - 12:00\",\"state\":1},{\"name\":\"12:00 - 12:45\",\"state\":1},{\"name\":\"12:45 - 13:30\",\"state\":0},{\"name\":\"13:30 - 14:15\",\"state\":0},{\"name\":\"14:15 - 15:00\",\"state\":0},{\"name\":\"15:00 - 15:45\",\"state\":1},{\"name\":\"15:45 - 16:30\",\"state\":0},{\"name\":\"16:30 - 17:15\",\"state\":1},{\"name\":\"17:15 - 18:00\",\"state\":1},{\"name\":\"18:00 - 18:45\",\"state\":1},{\"name\":\"18:45 - 19:30\",\"state\":1},{\"name\":\"19:30 - 20:15\",\"state\":0},{\"name\":\"20:15 - 21:00\",\"state\":0},{\"name\":\"21:00 - 21:45\",\"state\":1}]},{\"days\":\"thursday\",\"hours\":[{\"name\":\"06:00 - 06:45\",\"state\":0},{\"name\":\"06:45 - 07:30\",\"state\":1},{\"name\":\"07:30 - 08:15\",\"state\":1},{\"name\":\"08:15 - 09:00\",\"state\":1},{\"name\":\"09:00 - 09:45\",\"state\":0},{\"name\":\"09:45 - 10:30\",\"state\":0},{\"name\":\"10:30 - 11:15\",\"state\":1},{\"name\":\"11:15 - 12:00\",\"state\":1},{\"name\":\"12:00 - 12:45\",\"state\":1},{\"name\":\"12:45 - 13:30\",\"state\":1},{\"name\":\"13:30 - 14:15\",\"state\":0},{\"name\":\"14:15 - 15:00\",\"state\":1},{\"name\":\"15:00 - 15:45\",\"state\":0},{\"name\":\"15:45 - 16:30\",\"state\":0},{\"name\":\"16:30 - 17:15\",\"state\":0},{\"name\":\"17:15 - 18:00\",\"state\":1},{\"name\":\"18:00 - 18:45\",\"state\":1},{\"name\":\"18:45 - 19:30\",\"state\":0},{\"name\":\"19:30 - 20:15\",\"state\":1},{\"name\":\"20:15 - 21:00\",\"state\":1},{\"name\":\"21:00 - 21:45\",\"state\":1}]},{\"days\":\"friday\",\"hours\":[{\"name\":\"06:00 - 06:45\",\"state\":0},{\"name\":\"06:45 - 07:30\",\"state\":1},{\"name\":\"07:30 - 08:15\",\"state\":1},{\"name\":\"08:15 - 09:00\",\"state\":1},{\"name\":\"09:00 - 09:45\",\"state\":0},{\"name\":\"09:45 - 10:30\",\"state\":0},{\"name\":\"10:30 - 11:15\",\"state\":1},{\"name\":\"11:15 - 12:00\",\"state\":0},{\"name\":\"12:00 - 12:45\",\"state\":1},{\"name\":\"12:45 - 13:30\",\"state\":0},{\"name\":\"13:30 - 14:15\",\"state\":1},{\"name\":\"14:15 - 15:00\",\"state\":1},{\"name\":\"15:00 - 15:45\",\"state\":0},{\"name\":\"15:45 - 16:30\",\"state\":0},{\"name\":\"16:30 - 17:15\",\"state\":1},{\"name\":\"17:15 - 18:00\",\"state\":0},{\"name\":\"18:00 - 18:45\",\"state\":0},{\"name\":\"18:45 - 19:30\",\"state\":0},{\"name\":\"19:30 - 20:15\",\"state\":1},{\"name\":\"20:15 - 21:00\",\"state\":1},{\"name\":\"21:00 - 21:45\",\"state\":0}]}]"
            },
            {
                "key": "description",
                "value": "maiores cumque autem aliquam autem ipsam optio quod nisi culpa dicta ratione praesentium quia ad repellat qui dolor architecto rerum quis suscipit aut mollitia recusandae"
            },
            {
                "key": "short_description",
                "value": "Coach en trabajo"
            },
            {
                "key": "children", // Hijos
                "value": "5"
            }
        ],
        "ProfileInstitutions": [
            {
                "academic_degree": "Magister SCRUM", // Grado obtenido
                "start_date": "2021-05-03T17:24:25.354Z",
                "ending_date": "2021-05-03T17:24:25.354Z",
                "document": "https://picsum.photos/100", // Documento adjunto
                "Institution": {
                    "name": "Confianza" // Institucion educativa
                }
            }
        ],
        "Specialties": [
            {
                "id": 1,
                "name": "Amor",
                "alias": "AM"
            },
            {
                "id": 3,
                "name": "Amistad",
                "alias": "AD"
            }
        ],
        "District": { // Distrito
            "name": "San Miguel",
            "City": { // Departamento
                "name": "Lima",
                "Country": { // Pais
                    "name": "Pakistan"
                }
            }
        }
    }
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```