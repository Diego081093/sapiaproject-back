# Role Delete

Service to delete role

**URL** : `/role/:id`

**Method** : `DELETE`

**Auth required** : YES TOKEN

**Data constraints**

```json
{

}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Role deleted",
    "count": {
        // ...
    }
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```