# Media History List

Service list media history

**URL** : `/media`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
```
history = "1"
```
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": [
        {
            "id": 3,
            "idCategory": 1,
            "idMethod": 4,
            "name": "La hidratación en el desarrollo",
            "type": 1,
            "file": "https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5",
            "image": "https://picsum.photos/id/1/200/300",
            "duration": "20 min",
            "description": "Video o Audio de meditación",
            "points": 200,
            "state": 1,
            "Role": {
                "id": 2
            },
            "onProfile": {
                "id": 3,
                "done": 90,
                "favorite": 1
            }
        },
        {
            "id": 10,
            "idCategory": 1,
            "idMethod": 4,
            "name": "La hidratación en el desarrollo",
            "type": 1,
            "file": "https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5",
            "image": "https://picsum.photos/id/1/200/300",
            "duration": "20 min",
            "description": "Video o Audio de meditación",
            "points": 200,
            "state": 1,
            "Role": {
                "id": 2
            },
            "onProfile": {
                "id": 10,
                "done": 59,
                "favorite": 0
            }
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```