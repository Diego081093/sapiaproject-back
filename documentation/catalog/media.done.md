# Save Done Media

Service enter the done option in the media table

**URL** : `/media/done`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{
  "id": "tbl_media_id",
  "done":"[integer]"
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "The done was successfully saved",
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```