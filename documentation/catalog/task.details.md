# Task List

Service to get details task

**URL** : `/task/:id`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

```json
{
id:tbl_task_id 
  
}
```

**Data example**

```json
{
 
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": [
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 1,
                "tbl_profile_id": 2,
                "tbl_task_id": 2,
                "text": "nota en mi diario 1",
                "audio": "diary1.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 6,
                "tbl_task_id": 2,
                "expiration": "2020-09-05T01:44:19.505Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 1,
                "tbl_profile_id": 2,
                "tbl_task_id": 2,
                "text": "nota en mi diario 1",
                "audio": "diary1.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 7,
                "tbl_task_id": 2,
                "expiration": "2021-03-25T10:54:27.936Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 1,
                "tbl_profile_id": 2,
                "tbl_task_id": 2,
                "text": "nota en mi diario 1",
                "audio": "diary1.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 3,
                "tbl_task_id": 2,
                "expiration": "2021-03-21T08:15:19.178Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 1,
                "tbl_profile_id": 2,
                "tbl_task_id": 2,
                "text": "nota en mi diario 1",
                "audio": "diary1.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 1,
                "tbl_task_id": 2,
                "expiration": "2021-02-22T04:22:32.410Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 1,
                "tbl_profile_id": 2,
                "tbl_task_id": 2,
                "text": "nota en mi diario 1",
                "audio": "diary1.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 2,
                "tbl_task_id": 2,
                "expiration": "2020-11-26T12:38:30.678Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 1,
                "tbl_profile_id": 2,
                "tbl_task_id": 2,
                "text": "nota en mi diario 1",
                "audio": "diary1.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 10,
                "tbl_task_id": 2,
                "expiration": "2021-04-06T10:06:32.429Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 1,
                "tbl_profile_id": 2,
                "tbl_task_id": 2,
                "text": "nota en mi diario 1",
                "audio": "diary1.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 4,
                "tbl_task_id": 2,
                "expiration": "2021-05-04T12:51:50.744Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 1,
                "tbl_profile_id": 2,
                "tbl_task_id": 2,
                "text": "nota en mi diario 1",
                "audio": "diary1.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 10,
                "tbl_task_id": 2,
                "expiration": "2021-03-31T11:59:59.060Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 2,
                "tbl_profile_id": 3,
                "tbl_task_id": 2,
                "text": "nota en mi diario 2",
                "audio": "diary2.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 6,
                "tbl_task_id": 2,
                "expiration": "2020-09-05T01:44:19.505Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 2,
                "tbl_profile_id": 3,
                "tbl_task_id": 2,
                "text": "nota en mi diario 2",
                "audio": "diary2.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 7,
                "tbl_task_id": 2,
                "expiration": "2021-03-25T10:54:27.936Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 2,
                "tbl_profile_id": 3,
                "tbl_task_id": 2,
                "text": "nota en mi diario 2",
                "audio": "diary2.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 3,
                "tbl_task_id": 2,
                "expiration": "2021-03-21T08:15:19.178Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 2,
                "tbl_profile_id": 3,
                "tbl_task_id": 2,
                "text": "nota en mi diario 2",
                "audio": "diary2.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 1,
                "tbl_task_id": 2,
                "expiration": "2021-02-22T04:22:32.410Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 2,
                "tbl_profile_id": 3,
                "tbl_task_id": 2,
                "text": "nota en mi diario 2",
                "audio": "diary2.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 2,
                "tbl_task_id": 2,
                "expiration": "2020-11-26T12:38:30.678Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 2,
                "tbl_profile_id": 3,
                "tbl_task_id": 2,
                "text": "nota en mi diario 2",
                "audio": "diary2.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 10,
                "tbl_task_id": 2,
                "expiration": "2021-04-06T10:06:32.429Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 2,
                "tbl_profile_id": 3,
                "tbl_task_id": 2,
                "text": "nota en mi diario 2",
                "audio": "diary2.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 4,
                "tbl_task_id": 2,
                "expiration": "2021-05-04T12:51:50.744Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        },
        {
            "tbl_method_id": 4,
            "TaskTypes": {
                "id": 1,
                "name": "Formulario",
                "alias": "form"
            },
            "TaskQuestion": null,
            "Diary": {
                "id": 2,
                "tbl_profile_id": 3,
                "tbl_task_id": 2,
                "text": "nota en mi diario 2",
                "audio": "diary2.mp3",
                "feel": 0,
                "date": "2021-05-04T20:56:06.846Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.846Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.846Z"
            },
            "TaskMedia": null,
            "SessionTask": {
                "tbl_session_id": 10,
                "tbl_task_id": 2,
                "expiration": "2021-03-31T11:59:59.060Z",
                "state": 1,
                "created_by": "root",
                "createdAt": "2021-05-04T20:56:06.804Z",
                "edited_by": "root",
                "updatedAt": "2021-05-04T20:56:06.804Z"
            }
        }
    ]
}

    
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to number with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Training not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```