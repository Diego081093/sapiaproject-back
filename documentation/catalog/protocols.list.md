# List Protocols

Service to list protocols

**URL** : `/protocols`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
    "processId": "1", // Protocol process
    "date": "2021-03-25",   // YYYY-MM-DD
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Protocols found",
    "data": [
        {
            "id": 1,
            "code": "00001",
            "goal": "Consequatur aut eius temporibus.",
            "state": 1,
            "createdAt": "2021-06-17T22:32:06.858Z",
            "ProtocolProcess": {
                "id": 1,
                "name": "Botón escuchame"
            },
            "Responsible": {
                "id": 2,
                "slug": "dolor-dolorum-a",
                "firstname": "Cielo",
                "lastname": "Wiza",
                "state": 1
            },
            "Approver": {
                "id": 2,
                "slug": "dolor-dolorum-a",
                "firstname": "Cielo",
                "lastname": "Wiza",
                "state": 1
            }
        },
        {
            "id": 2,
            "code": "00002",
            "goal": "Repudiandae nihil perspiciatis.",
            "state": 1,
            "createdAt": "2021-06-17T22:32:06.858Z",
            "ProtocolProcess": {
                "id": 1,
                "name": "Botón escuchame"
            },
            "Responsible": {
                "id": 2,
                "slug": "dolor-dolorum-a",
                "firstname": "Cielo",
                "lastname": "Wiza",
                "state": 1
            },
            "Approver": {
                "id": 2,
                "slug": "dolor-dolorum-a",
                "firstname": "Cielo",
                "lastname": "Wiza",
                "state": 1
            }
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```