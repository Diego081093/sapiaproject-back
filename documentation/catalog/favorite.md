# Update Psychologist

Service to save item as favorite to mottivateca

**URL** : `/favorite/:id`

**Method** : `POST`

**Auth required** : yes

**Data constraints**

```json
{}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Item saved as favorite successfully"
}
```

## Error Response

**Condition** : If the user don't have PATIENT nor PSYCHOLOGIST role

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "message": "Access denied with provided credentials"
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Media not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```