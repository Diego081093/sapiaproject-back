# Role List

Service to list the roles

**URL** : `/role`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

```json
{

}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": []
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```