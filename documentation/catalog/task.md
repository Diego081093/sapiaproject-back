# Task List

Service to get list task

**URL** : `/task`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

```json
{
 
  
}
```

**Data example**

```json
{
 
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": [
        {
            "id": 1,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 1,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        },
        {
            "id": 2,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 2,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        },
        {
            "id": 3,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 3,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        },
        {
            "id": 4,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 4,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        },
        {
            "id": 5,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 5,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        },
        {
            "id": 6,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 6,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        },
        {
            "id": 7,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 7,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        },
        {
            "id": 8,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 8,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        },
        {
            "id": 9,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 9,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        },
        {
            "id": 10,
            "name": "tarea 10",
            "description": "Descripción de la tarea",
            "TaskTypes": {
                "id": 10,
                "name": "Tipo de tarea 10",
                "description": "Tipos de Tareas"
            }
        }
    ]
}
    
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to number with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Training not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```