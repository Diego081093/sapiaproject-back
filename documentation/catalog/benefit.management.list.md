# Benefit management list

Service to list benefit management

**URL** : `/benefits/management`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Benefits profile found",
    "data": [
        {
            "id": 1,
            "state": 1,
            "Profile": {
                "firstname": "Petra",
                "lastname": "Farrell",
                "phone": "973392170",
                "points": 0
            },
            "Benefit": {
                "name": "Confianza",
                "image": "https://picsum.photos/100",
                "description": "Voluptatem modi voluptatem. Exercitationem accusantium non qui vero sunt. Rerum nostrum totam porro at est et. Eum voluptatem aut maiores harum. Ut quia in odit et. Expedita est modi dolor necessitatibus autem saepe aut.",
                "point": 30,
                "state": 1,
                "CompanyBenefit": {
                    "name": "Depor",
                    "image": "https://picsum.photos/100",
                    "state": 1
                }
            }
        }
    ]
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "message": "Access denied with provided credentials"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```