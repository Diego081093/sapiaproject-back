# Notes

Service to disable notes of the session and room emotions

**URL** : `/notes/:id/disable`

**Method** : `PUT`

**Auth required** : YES

**Data constraints**

- **type**: Tipo de notas

  - session: Default
  - room

**Data example**

```json
{
    "type": "session"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Note disabled"
}
```

## Error Response

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "message": "Note not found"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}