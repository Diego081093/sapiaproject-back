# Media List

Service to list media

**URL** : `/media`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

```json
{
 
  
}
```

**Data example**

```json
{
  ```
method=1,2,3,4
category=1,2,3,4
favorite=1
history=1
```
 
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
        {
    "data": [
        {
            "id": 3,
            "idCategory": 1,
            "idMethod": 4,
            "name": "La hidratación en el desarrollo",
            "type": 1,
            "file": "https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5",
            "image": "https://picsum.photos/id/1/200/300",
            "duration": "20 min",
            "description": "Video o Audio de meditación",
            "points": 200,
            "state": 1,
            "Role": {
                "id": 2
            },
            "onProfile": {
                "id": 3,
                "done": 90,
                "favorite": 1
            }
        }
    ]
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to number with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Training not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```