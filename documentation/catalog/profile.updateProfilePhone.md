# Update Profile Phone Only

Service to update profile phone only

**URL** : `/profile/:id/phone`

**Method** : `PUT`

**Auth required** : YES

**Data constraints**

```json
{
    "phone": "987123456"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Profile photo updated"
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 Invalid data`

**Content** :

```json
{
    "message": "Empty data."
}
```

---

**Condition** : If the provided credentials are incorrect.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "message": "Access denied with provided credentials"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```