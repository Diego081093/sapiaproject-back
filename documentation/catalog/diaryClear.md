# Created Poll

service to clear diary areas

**URL** : `/diary/:id/clear`

**Method** : `PUT`

**Auth required** : YES

**Data constraints**

```text
    "area" = "String(text | audio)"
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Update text of Diary"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```