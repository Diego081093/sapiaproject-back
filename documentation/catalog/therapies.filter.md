# Filter therapies

Service to filter therapies

**URL** : `/therapies/filter`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
    "day": "Lunes",
    "hour": "11:00:00",
    "gender": "F" // or 'M'
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Therapies found",
    "data": [
        {
            "id": 3,
            "day": "Lunes",
            "start_hour": "11:00:00",
            "end_hour": "13:30:00",
            "Profile": {
                "id": 2,
                "slug": "ipsa-enim-vitae",
                "firstname": "Blake",
                "lastname": "Lynch",
                "email": "Lesley25@gmail.com",
                "phone": "971859097",
                "gender": "F",
                "state": 1,
                "Avatar": {
                    "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_2.png"
                }
            }
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```