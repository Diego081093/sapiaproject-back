# validate Secret Question 

Service to validate user secret question

**URL** : `/SecretQuestion/validate`

**Method** : `POST`

**Auth required** : yes

**Data constraints**

```json
{
  "dni":"48310182",
  "answer": "firulais"
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Answer Correct!",
    "data": {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjo0LCJpZFJvbGUiOjMsImlkUHJvZmlsZSI6NCwidXNlcm5hbWUiOiI0ODMxMDE4MiIsInNsdWciOiJjdXBpZGl0YXRlLXF1aS1tb2xlc3RpYWUiLCJmaXJzdG5hbWUiOiJSb3NzaWUiLCJsYXN0bmFtZSI6Ild1Y2tlcnQiLCJkbmkiOiIzNTA0OTgxNSIsImVtYWlsIjoiWWVzc2VuaWEuRGliYmVydDhAaG90bWFpbC5jb20iLCJwaG9uZSI6IjkyMzY0NDUxMSIsImdlbmRlciI6Ik0iLCJiaXJ0aGRheSI6IjIwMjEtMDQtMDUiLCJhdmF0YXIiOiJodHRwczovL21vdHRpdmEuZ2xvYm9henVsLnBlL2ltYWdlcy1hcHAvQVZBVEFSUy9hdmF0YXJfNC5wbmciLCJwcm9maWxlcyI6W3siaWQiOjQsIm5hbWUiOiJSb3NzaWUiLCJBdmF0YXIiOnsiaWQiOjQsImltYWdlIjoiaHR0cHM6Ly9tb3R0aXZhLmdsb2JvYXp1bC5wZS9pbWFnZXMtYXBwL0FWQVRBUlMvYXZhdGFyXzQucG5nIn0sIlVzZXIiOnsiaWQiOjQsInVzZXJuYW1lIjoiNDgzMTAxODIifX1dfSwiaWF0IjoxNjE4MDAxODg2fQ.-jR8wNsXDagw_3xLcKNLLWt4s3K8yg8q5w8kOtCTf10"
    }
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "message": "Incorrect answer"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```