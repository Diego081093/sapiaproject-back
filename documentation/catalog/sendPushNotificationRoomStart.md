# Send Push Notification for start room

Notify the psychologist of the start of your room

**URL** : `/notification/sendPushNotificationRoomStart/`

**Method** : `GET`

**Auth required** : SI

**Data constraints** : NO

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Process successfully",
    "session": [
        {
            "id": "Integer(id)",
            "title": "String",
            "start": "date('Y-m-d H:i:sTZ')",
            "rules": "String",
            "participants": 0,
            "twilio_uniquename": "jd3zc0lw8bjzwdrnss5lrj7n7...",
            "Chat": {
                "id": "Integer(id)",
                "ChatParticipants": {
                    "id": "Integer(id)",
                    "Profile": {
                        "id": "Integer(id)",
                        "User": {
                            "id": "Integer(id)",
                            "token_device": "cAFzam9VQSqz1z3b6s8adC-..."
                        }
                    }
                }
            }
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to register with provided credentials."]
}
```

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```