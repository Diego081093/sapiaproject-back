# Training List

Service to get avatar and image

**URL** : `/trainings`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

- **limit**: Limita los resultados a el valor deseado

- **offset**: Cuantos resultados debe ignorar

- **search**: Termino a buscar

- **role**: Tipo de rol asignado

  - 1: Paciente
  - 2: Psicologo

- **type**: Tipo del medio

  - 1: video
  - 2: audio

- **type_cat_media**: Tipo de categoria del medio

  - 1: capacitaciones
  - 2: libre

- **category**: ID de una categoria en particular

- **method**: ID de un metodo en particular

- **favorite**: Booleano, resultados que han sido destacados

- **archived**: Booleano, resultados que han sido archivados

**Data example**

```json
{
  "limit": 2,
  "offset": 4,
  "search": "fam",
  "role": 1, // paciente
  "type": 2,  // audio
  "type_cat_media": 1, // capacitaciones
  "category": 3,
  "method": 5,
  "archived": true,
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Trainings found",
    "count": 10,
    "previous": null,
    "next": "http://localhost:3000/trainings?offset=2&limit=2",
    "limit": 2,
    "offset": 0,
    "data": [
        {
            "favorite": 1,
            "done": 75,
            "media": {
                "id": 1,
                "file": "https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5",
                "image": "https://picsum.photos/id/1/200/300",
                "duration": "13:59:00",
                "name": "Técnicas sencillas de meditación",
                "type": 1,
                "category_media": {
                    "name": "Transtornos de Ansiedad",
                    "state": 1
                },
                "method": {
                    "name": "Hidratación",
                    "image": "",
                    "state": 1
                }
            }
        },
        {
            "favorite": 0,
            "done": 98,
            "media": {
                "id": 2,
                "file": "https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5",
                "image": "https://picsum.photos/id/1/200/300",
                "duration": "13:59:00",
                "name": "La hidratación en el desarrollo",
                "type": 1,
                "category_media": {
                    "name": "Familia",
                    "state": 1
                },
                "method": {
                    "name": "Hidratación",
                    "image": "",
                    "state": 1
                }
            }
        }
    ]
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unauthorized"]
}
```

---

**Condition** : If route no exist or no conection with server. If no results are found

**Code** : `404 NOT FOUND`

**Content** :

```json
{
    "message": "No trainings were found"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "message": "Something went wrong"
}