# Category Media List

Service to get list category media

**URL** : `/category/media`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

```json
{
 
  
}
```

**Data example**

```json
{
 
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": [
        {
            "id": 1,
            "name": "Familia"
        },
        {
            "id": 2,
            "name": "Transtornos de Ansiedad"
        },
        {
            "id": 3,
            "name": "Terapia de pareja"
        },
        {
            "id": 4,
            "name": "Violencia de Pareja"
        },
        {
            "id": 5,
            "name": "Transtorno Depresivo"
        },
        {
            "id": 6,
            "name": "Terapia Cognitiva"
        },
        {
            "id": 7,
            "name": "Terapia Grupal"
        }
    ]
}
    
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to number with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Training not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```