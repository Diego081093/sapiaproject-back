# Media Detail

Service to get avatar and image

**URL** : `/media/{media_id}`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

```json
{

}
```

**Data example**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
            "id": "integer(id)",
            "idCategory": "integer(id)",
            "idMethod": "integer(id)",
            "name": "String",
            "type": "integer",
            "file": "url",
            "image": "url",
            "duration": "time",
            "description": "text",
            "points": "integer",
            "state": "integer(1-0)",
            "Role": {
                "id": "integer(id)"
            },
            "onProfile": {
                "id": "integer(id)",
                "done": "integer",
                "favorite": "integer"
            }
        }
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to number with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Training not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```