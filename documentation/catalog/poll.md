# Created Poll

service to save a poll

**URL** : `/poll`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{
 "session_id":"[integer]",
    "experience":"[integer]",
    "qualification":"[integer]",
    "comment":"[string]"
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Poll created",
    "data": {
        "state": 1,
        "id": 13,
        "tbl_session_id": 3,
        "experience": 3,
        "qualification": 35,
        "comment": "estuvo buena la sesion",
        "created_by": "root",
        "edited_by": "root",
        "updatedAt": "2021-04-09T15:46:37.191Z",
        "createdAt": "2021-04-09T15:46:37.191Z"
    }
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```