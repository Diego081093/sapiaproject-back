# Update Psychologist

Service to update psychologist data

**URL** : `/emotions-room/:id`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Emotion room found",
    "emotion_room": {
        "id": 2,
        "title": "Product Assurance Executive",
        "start": "2021-10-13T15:55:36.032Z",
        "end": "2021-10-23T02:23:07.670Z",
        "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_1.png"
    },
    "patients": [
        {
            "id": 4,
            "firstname": "Julian",
            "lastname": "Johnston",
            "state": 1
        }
    ],
    "psychologists": [
        {
            "id": 2,
            "firstname": "Rickey",
            "lastname": "Reynolds",
            "state": 1
        },
        {
            "id": 9,
            "firstname": "Nellie",
            "lastname": "Witting",
            "state": 1
        }
    ]
}
```

## Error Response

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Emotion room not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```