# Session Create

It is used to created new session

**URL** : `/sessions`

**Method** : `POST`

**Auth required** : TOKEN



**Data example**

```json
{
    "idPsychologist":"3",
    "date": "2021-04-21 15:36:27"
   
}
```

## Success Response

**Code** : `200 OK`

**Content example**

### Psychologist data

// actual session
 // total sesions
```json
{
    "message": "Rooms created",
    "room": {
        "sid": "RMa96e6ba8ca2e3366e5af00b63e8de1e4",
        "status": "in-progress",
        "dateCreated": "2021-04-22T14:52:28.000Z",
        "dateUpdated": "2021-04-22T14:52:28.000Z",
        "accountSid": "AC5ca0cbf2ffee60449e85f9fb88cb4d6e",
        "enableTurn": true,
        "uniqueName": "room-s-11",
        "statusCallback": null,
        "statusCallbackMethod": "POST",
        "endTime": null,
        "duration": null,
        "type": "go",
        "maxParticipants": 2,
        "maxConcurrentPublishedTracks": 0,
        "recordParticipantsOnConnect": false,
        "videoCodecs": null,
        "mediaRegion": null,
        "url": "https://video.twilio.com/v1/Rooms/RMa96e6ba8ca2e3366e5af00b63e8de1e4",
        "links": {
            "recordings": "https://video.twilio.com/v1/Rooms/RMa96e6ba8ca2e3366e5af00b63e8de1e4/Recordings",
            "participants": "https://video.twilio.com/v1/Rooms/RMa96e6ba8ca2e3366e5af00b63e8de1e4/Participants",
            "recording_rules": "https://video.twilio.com/v1/Rooms/RMa96e6ba8ca2e3366e5af00b63e8de1e4/RecordingRules"
        }
    }
}
```



## Error Response

**Condition** : If no sessions are found as helpdesk

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "message": "No sessions were found"
}
```


**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
