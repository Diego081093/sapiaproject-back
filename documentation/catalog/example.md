# Update Psychologist

Service to update psychologist data

**URL** : `/psycology`

**Method** : `PUT`

**Auth required** : yes

**Data constraints**

```json
{
    "firstname": "Lorem",
    "lastname": "Ipsum",
    "dni": "48******",
    "birthday": "2021-03-25",   // YYYY-MM-DD
    "gender": "F",              // F or M
    "email": "dolor@sit.com",
    "district_id": "1",
    "specialties": [1, 3],      // Array id
    "attributes": [
        {
            "key": "some-key",
            "value": "Some value"
        }
    ],
    "institutions": [
        {
            "id": 2,
            "academic_degree": "Hello world",
            "start_date": "2021-03-25 12:23:27.65-05",
            "ending_date": "2021-03-25 12:23:27.65-05",
            "document": "123"
        }
    ]
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Profile updated"
}
```

## Error Response

**Condition** : Reason

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "message": "Access denied with provided credentials"
}
```

---

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "message": "Access denied with provided credentials"
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Profile not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```