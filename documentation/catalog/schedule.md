# My Schedule

This endpoint shows the agenda of each patient

**URL** : `/schedule`

**Method** : `GET`

**Auth required** : SI

**Data constraints** : NO

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": {
        "tasks": [
            {
                "id": "Integer(id)",
                "expiration": "Date",
                "Session": {
                    "id": "Integer(id)",
                    "date": "Date",
                    "week": "Integer"
                },
                "Task": {
                    "id": "Integer(id)",
                    "week": "Integer",
                    "name": "Integer",
                    "description": "Integer",
                    "TaskTypes": {
                        "id": "Integer(id)",
                        "name": "Integer",
                        "alias": "Integer"
                    }
                }
            }, {...}
        ],
        "nextSession": {
            "date": "Date",
            "ProfilePsychologist": {
                "id": "Integer(id)",
                "firstname": "String",
                "lastname": "String",
                "birthday": "Date(Y-m-d)",
                "Avatar": {
                    "image": "String(url)"
                },
                "ProfileAttribute": {
                    "video": "String(url)",
                    "description": "String"
                },
                "Specialties": [
                    {
                        "id": "Integer(id)",
                        "name": "String",
                    }, {...}
                ]
            }
        },
        "nextRooms": [
            {
                "id": "Integer(id)",
                "title": "String",
                "start": "Date(Y-m-d)",
                "end": "Date(Y-m-d)",
                "rules": "String(slug)",
                "participants": "Integer",
                "image": "String(url)"
            }, {...}
        ]
    }
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to register with provided credentials."]
}
```

**Code** : `401 UNAUTHORIZED`

**Content** :

```txt
Unauthorized
```

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```