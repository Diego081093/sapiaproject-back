# sSession List

It is used to receive a list of all patients to which I as a psychologist belong.

**URL** : `/patient-by`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
  /**
   * Query param, OPTIONAL:
   * Today
   * Week
   * Month
   * Year
   */
  "to": "[String validation filter]"
}
```

**Data example**

```json
{
  "to": "week"
}
```

## Success Response

**Code** : `200 OK`

**Content example**
```json
{
    "message": "Patients found",
    "patients": [
        {
            "id": 1, // patient_id
            "firstname": "Raven",
            "lastname": "Oberbrunner",
            "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_9.png",
            "current_session": 2,
            "total_sessions": 2,
            "sessions": [
                {
                    "id": 2, // session_id
                    "date": "2021-03-26T19:56:41.134Z",
                    "order": 2,
                    "SessionAttributes": [
                        {
                            "key": "state",
                            "value": "Urgente"
                        },
                        {
                            "key": "type",
                            "value": "Depresion"
                        }
                    ]
                },
                {
                    "id": 1,
                    "date": "2021-03-26T19:56:41.134Z",
                    "order": 1,
                    "SessionAttributes": [
                        {
                            "key": "state",
                            "value": "Urgente"
                        },
                        {
                            "key": "type",
                            "value": "Depresion"
                        }
                    ]
                }
            ]
        },
        {
            "id": 5,
            "firstname": "Judy",
            "lastname": "Upton",
            "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_4.png",
            "current_session": 2,
            "total_sessions": 2,
            "sessions": [
                {
                    "id": 9,
                    "date": "2021-03-26T19:56:41.134Z",
                    "order": 2,
                    "SessionAttributes": [
                        {
                            "key": "state",
                            "value": "Urgente"
                        },
                        {
                            "key": "type",
                            "value": "Depresion"
                        }
                    ]
                },
                {
                    "id": 3,
                    "date": "2021-03-26T19:56:41.134Z",
                    "order": 1,
                    "SessionAttributes": [
                        {
                            "key": "state",
                            "value": "Urgente"
                        },
                        {
                            "key": "type",
                            "value": "Depresion"
                        }
                    ]
                }
            ]
        }
    ]
}
```

## Error Response


**Condition** : If the information provided is incomplete or incorrect.

**Code** : `401 Unauthorized`

**Content** :

```text
Unauthorized
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}