# List Psychologist

Service to list psychologist

**URL** : `/psycologist/list`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
    "city": "1", // City ID
    "institution": "3", // Institution ID
    "age": "21", // Patient age, must NOT be used with age_range_min or age_range_max
    "age_range_min": "18", // Patient age min
    "age_range_max": "26", // Patient age max
    "patients": "5", // Patients
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Psychologist found",
    "data": [
        {
            "id": 2,
            "tbl_role_id": 2,
            "tbl_user_id": 2,
            "tbl_avatar_id": 2,
            "tbl_district_id": 2,
            "slug": "id-ex-et",
            "firstname": "Kamron",
            "lastname": "Wolff",
            "dni": "42981425",
            "photo_dni": null,
            "email": "Kiley_Bartoletti@yahoo.com",
            "phone": "997600941",
            "gender": "F",
            "birthday": "2020-06-19",
            "state": 1,
            "points": 0,
            "req_sent_at": null,
            "req_state": false,
            "req_approved_at": null,
            "civil_state": null,
            "ocupation": null,
            "study_nivel": null,
            "created_by": "root",
            "createdAt": "2021-06-19T00:39:18.656Z",
            "edited_by": "root",
            "updatedAt": "2021-06-19T00:39:18.656Z",
            "District": {
                "name": "San Miguel",
                "City": {
                    "id": 2,
                    "name": "Buenos Aires"
                }
            },
            "ProfileInstitutions": [
                {
                    "tbl_institution_id": 1,
                    "Institution": {
                        "name": "Confianza"
                    }
                },
                {
                    "tbl_institution_id": 4,
                    "Institution": {
                        "name": "Huancayo"
                    }
                }
            ],
            "patients": 10,
            "patientsActives": 0
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```