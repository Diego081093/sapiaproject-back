# Room Closed

It is to closed room

**URL** : `/room`

**Method** : `PUT`

**Auth required** : TOKEN



**Data example**

```json
{
    "idRoom":"15",
    "state":"2"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

### Psychologist data


```json
{
    "message": "Room Closed!!"
}
```



## Error Response

**Condition** : If no sessions are found as helpdesk

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "message": "No sessions were found"
}
```


**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
