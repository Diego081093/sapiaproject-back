# Role Create

Service to create role

**URL** : `/role`

**Method** : `POST`

**Auth required** : YES TOKEN

**Data constraints**

```json
{
    "name": "Lorem",
    "alias": "L",   // Just 1 char
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "rol updated",
    "date": {
        // ...
    }
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```