# MOTTIVA REST API

## Documentation

- [Endpoints](documentation/endpoints.md) - Cointains all services' routes.

## In Markdown

Inspired by [@iros](https://github.com/iros)'s [documentation
gist](https://gist.github.com/iros/3426278).

Focus on using the templating Markdown to create comprehensive, structured and
helpful API documentation. Structure should be regular and repeated across
endpoints and between projects.

## Directory structure

This is the main directory structure you end up with following the instructions of this page:

```plaintext
|-- home
|   |-- documentation
|       |-- AUTH
|       |-- PROFILE
|       |-- ROLE
|       |-- endpoints.md
|   |-- src
|       |-- config
|       |-- controllers
|       |-- database
|       |-- lib
|       |-- middleware
|       |-- migrations
|       |-- models
|       |-- routes
|       |-- seeders
|       |-- app.js
|       |-- helpers.js
|       |-- index.js
|       |-- repositories
|   |-- .babelrc
|   |-- .env.example
|   |-- .gitignore
|   |-- .sequelizerc
|   |-- package.json
|   |-- readme.md
```

- `/home/documentation` - Contains directories for documentation endpoints' api
- `/home/src/config` - Contains configuration database
- `/home/src/controllers` -
- `/home/src/database` -

---

## Overview

The REST API installation consists of setting up the following components:

1. [PostgreSQL](https://www.postgresql.org/download/) - This necesary create password of fourth digits
1. [NodeJs](https://nodejs.org/en/) - This necesary install tool
1. [Postman](https://www.postman.com/) - This necesary for testing routes

---

## Development

### Prerequisite

You'll need to have at least [NodeJs](https://nodejs.org/en/) installled to run the application and
[Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) installled to clone the repository and submit a pull request.

### Configuration to run the environment

You need to know a little of Git, which is the tool that helps us controlling our files' versioning.

```bash
git clone git@gitlab.com:globoazul/motiva-backend-api.git
git cd motiva-backend-api/
```

```js
// this instalation project
npm install

// its necesary clone env & add real values
cp .env.example .env
```

### Execute project

```js
// delete database , create new database and run mitrations
npm run fresh

// execute seeders
npx sequelize-cli db:seed --seed 20210302212923-user-seeder

// execute app
npm run dev
```

## Restriction

The code in this repository is protected for globo azul sac.

Do what you want, [Unlicense dot org](http://unlicense.org/), spread the word.
