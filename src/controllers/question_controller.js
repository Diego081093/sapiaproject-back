const {tbl_question} = require('../models')
const {tbl_alternative} = require('../models')

export async function getQuestion(req, res){
    try {
        const question = await tbl_question.findAll({where:{state:1}});
        res.json({
            data: question
        }); 
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getListQuestion(req, res){
    try {
        const questions = await tbl_question.findAll({
            attributes: [
                'id',
                'question'
            ],
            where: {
                state: 1
            }
        });

        await Promise.all(questions.map(async (item, key) => {
            item.dataValues.alternatives = await getAlternatives(item.id)
            return item;
        })).then((result) => {
            res.json({
                data: result
            });
        });

    } catch (error) {
        console.log(error);
    }
}

async function getAlternatives(idQuestion) {
    return await tbl_alternative.findAll({
        attributes: [
            'id',
            'alternative',
            'score'
        ],
        where: {
            state: 1,
            tbl_question_id: idQuestion
        }
    })
}

export async function createQuestion(req, res){
    const{question}=req.body;
    try {
        let newQuestion = await tbl_question.create({
            question,
        },{
            fields: ['question']
        });

        if (newQuestion) {

            res.json({
                message: 'question created',
                date: newQuestion
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}


export async function getAlternativeQuestion(req, res){
    try {
        const qalternative = await tbl_question.findAll({ include: Alternative});
        res.json({
            data: qalternative
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}