const { tbl_secret_question,tbl_user,tbl_user_secret_question, tbl_profile, tbl_avatar  } = require('../models')
import {createToken} from './token_controller'
export async function getSecretQuestion(req, res){
    try {
        const dni = req.params.dni;
        const user = await tbl_user.findOne({where:{username:dni,state:1}})
        const user_id = user.id
        const user_secret_question=await tbl_user_secret_question.findOne({where:{tbl_user_id:user_id,state:1}})
        const secret_question_id=user_secret_question.tbl_secret_question_id
        const secretquestion = await tbl_secret_question.findOne({where:{id:secret_question_id,state:1}});
        const question=secretquestion.question

        res.json({
            data: question
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}

export async function postValidateSecretQuestion(req, res){
    try {
        const {dni,answer} = req.body;
        const user = await tbl_user.findOne({where:{username:dni}})
        const usuarioID = user.id
        const user_secret_question = await tbl_user_secret_question.findOne({where:{tbl_user_id:usuarioID,state:1}})
        const secret_question_id = user_secret_question.tbl_secret_question_id
        const secretquestion = await tbl_user_secret_question.findOne({where:{tbl_secret_question_id:secret_question_id,answer:answer,state:1}});


        const userName = user.username
       console.log(usuarioID)
        if(secretquestion){

            let token =await createToken(req,res,usuarioID,userName)
            return res.json({

                message:"Answer Correct!",
                data:{token}

            })
        }

        return res.status(400).json({
            message: "Answer Incorrect!"
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}

export async function getListSecretQuestion(req, res){
    try {
        const questions = await tbl_secret_question.findAll({where:{state:1}});
        res.json({
            data: questions
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}


export async function createSecretQuestion(req, res){

    const { question }=req.body;

    try {
        let newSecretQuestion = await tbl_secret_question.create({
            question
        });

        if(newSecretQuestion){

            res.json({
                message: 'User secret question created',
                data: newSecretQuestion
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function updateSecretQuestion(req, res){

    const {
        question,
        state
    } = req.body;

    try {

        const SecretQuestion = await tbl_secret_question.findByPk(req.params.id)

        if( SecretQuestion ){

            await SecretQuestion.update({
                question,
                state
            })

            return res.json({
                message: 'User secret question updated',
                data: SecretQuestion
            });
        }

        return res.status(404).json({
            message: 'User secret question not found'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function deleteSecretQuestion(req, res) {
    try {
        const deleteSecretQuestion = await tbl_secret_question.destroy({
            where: {
                id: req.params.id
            }
        });
        res.json({
            message: 'User secret question deleted successfully',
            count: deleteSecretQuestion
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}