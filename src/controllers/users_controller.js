
// import Users from '../models/users_model'
const { tbl_user, tbl_profile, tbl_avatar } = require('../models')
const { isValidPassword } = require('../helpers')
import passport from 'passport'
import jwt from 'jsonwebtoken'
import {createToken} from './token_controller'
import bcrypt from 'bcrypt'

export async function getListUser(req, res){
    try {
        const users = await tbl_user.findAll({where:{state:1}});
        res.json({users});
    } catch (error) {
        console.log(error);
    }
}


export async function createUser(req, res) {
    try {
        const { username, password, role_id } = req.body;
        const passwordEncrypt = await bcrypt.hash(password, parseInt(process.env.BCRYPT_ROUNDS));
        let newUser = await tbl_user.create({
            username,
            password: passwordEncrypt,
            role_id
        }, { fields: ['username', 'password','role_id'] });
        if (newUser)
            signin(req, res)
        else
            res.status(500).json({
                message: 'Something goes wrong',
                data: {}
            })
    } catch(error) {
        console.error('You have a error createUser: ', error);
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        })
    }
}
export async function sendCod(req,res){
    const accountSid = 'ACc70e0bada40bf92e5fce7623325b9b88';
    const authToken = '964357445c25fe2cf26efb21f3b7e86c';
   // const MY_PHONE_NUMBER= '+51934575282';
    const client = require('twilio')(accountSid, authToken);
    const {  phone } = req.body;
    function generateRandom(min,max) {
        return Math.round( Math.random() * (max - min)+ min);
    }

    try {
        let random = generateRandom(1000, 9999)
        const response = await client.messages.create({
            to:`+51${phone}`,
            from:'+19706707670',
            body:' Su codigo de verificacion para entrar a Mottiva es :' + random
        })
        .then(message=> console.log(message.sid));
        res.status(200).send({
            message: "Verification is sent!!",
            data:{
                phone,
                random,
                response
            }

        })
    } catch (error) {
        res.status(400).send({
            message: "Wrong phone number : ",
            phone,
            error
        });
        console.log(error);

    }

}
export async function updateCod(req,res){
    const accountSid = 'ACc70e0bada40bf92e5fce7623325b9b88';
    const authToken = '964357445c25fe2cf26efb21f3b7e86c';
   // const MY_PHONE_NUMBER= '+51934575282';
    const client = require('twilio')(accountSid, authToken);
    const {  phonenumber } = req.body;
    function generateRandom(min,max) {
        return Math.round( Math.random() * (max - min)+ min);
    }

    try {
        let random = generateRandom(1000, 9999)
        const response = await client.messages.create({
            to:`+51${phonenumber}`,
            from:'+19706707670',
            body:' Su codigo de verificacion para entrar a Mottiva es :' + random
        })
        .then(message=> console.log(message.sid));
        res.status(200).send({
            message: "Verification is sent!!",
            data:{
                phonenumber,
                random,
                response
            }

        })
    } catch (error) {
        res.status(400).send({
            message: "Wrong phone number : ",
            phonenumber,
            error
        });
        console.log(error);

    }
}
export async function updatePhoneOld(req,res){
   /* const accountSid = 'ACc33dc3c8a5b3872d521207859fd805e6';
    const authToken = 'f4040991ee9c722905f094fbf094384a';
   // const MY_PHONE_NUMBER= '+51934575282';
    const client = require('twilio')(accountSid, authToken);*/
    const user=req.params.user;
    /*function generateRandom(min,max) {
        return Math.round( Math.random() * (max - min)+ min);
    }*/
    const users = await tbl_user.findAll({ attributes: ['username','phonenumber'],where:{username:user} });
 // const cel = users.phonenumber;
    res.json({users})
    /*
        try {
            let random = generateRandom(1000, 9999)
            const response = await client.messages.create({
                to: `+51${phone}`,
                from: '+14154292145',
                body: ' su codigo de verificacion es :' + random
            })
                .then(message => console.log(message.sid));
            res.status(200).send({
                message: "Verification is sent!!",
                phone,
                random,
                response

            })
        } catch (error) {
            res.status(400).send({
                message: "Wrong phone number : ",
                phone,
                error
            });
            console.log(error);

        }*/

}

async function sendTwilioPhoneCode(phone){

    // await new Promise((resolve, reject) => {
    //     // Llamamos a resolve(...) cuando lo que estabamos haciendo finaliza con éxito, y reject(...) cuando falla.
    //     // En este ejemplo, usamos setTimeout(...) para simular código asíncrono.
    //     // En la vida real, probablemente uses algo como XHR o una API HTML5.
    //     setTimeout(function(){
    //     //   resolve("¡Éxito!"); // ¡Todo salió bien!
    //         reject('Any')

    //     // throw 'Couldn\'t send SMS'

    //     }, 2000);
    //   }).then((successMessage) => {
    //     // succesMessage es lo que sea que pasamos en la función resolve(...) de arriba.
    //     // No tiene por qué ser un string, pero si solo es un mensaje de éxito, probablemente lo sea.
    //     console.log("¡Sí! " + successMessage);
    //   }).catch(e => {

    //     console.log('')
    //     console.log('e')
    //     console.log(e)
    //     console.log('')

    //     throw 'Error 123'

    //   });

    // throw 'Error 123'

    // return true

    const accountSid = 'AC5ca0cbf2ffee60449e85f9fb88cb4d6e';
    const authToken = 'cab1a08b148aab0fd98bbfec1b841c64';

    const client = require('twilio')(accountSid, authToken);

    function generateRandom(min,max) {
        return Math.round( Math.random() * (max - min)+ min);
    }

    let random = generateRandom(1000, 9999)

    await client.messages.create({
        to: `+51${phone}`,
        from: '+17707497065',
        body: 'Su codigo de verificacion es :' + random
    })
    .then(message=> console.log(message.sid))
    .catch(e => {

        console.log('')
        console.log('e')
        console.log(e)
        console.log('')

        throw "Couldn't send SMS"
    })
}

export async function updatePhone(req, res){

    try{

        const user = req.user

        const {old_phone,new_phone} = req.body

        const profile = await tbl_profile.findOne({
            where: {
                tbl_user_id: user.id,
                phone: old_phone
            }
        })

        if( profile ){

            await profile.update({
                phone: new_phone
            })




            const usuarioID = user.id
            const userName = user.name

            let token =await createToken(req,res,usuarioID,userName)
            return res.json({

                message: 'Phone Update!',
                data:{token}

            })
        }

        return res.status(400).json({
            message: 'Incorrect phone number'
        })

    } catch(err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

export async function signin(req, res) {
    try {
        passport.authenticate('signin', async (err, user, info) => {
            try {
                if (err || !user)
                    return res.status(401).json({
                        message: 'Contraseña incorrecta',
                        data: {}
                    })
                req.login(user, { session : false }, async (err) => {
                    if(err)
                        return res.status(402).json({
                            message: 'Contraseña incorrecta',
                            data: {}
                        })
                        const profile = await tbl_profile.findOne({
                            where:{
                                tbl_user_id:user.id
                            }
                        })
                        const token = await jwt.sign({ user: profile }, 'globoazul_token')
                        
                    return res.json({
                        message: 'User successfull',
                        data:{token}
                    })
                })
            } catch (err) {
                console.error('You have a error passport.authenticate', err);
                return res.status(500).json({
                    message: 'Something went wrong',
                    data: {}
                })
            }
        })(req, res)
    } catch(err) {
        console.error(err);
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        })
    }
}
export async function getUserById(req, res) {
    try {
        console.log(req.body.a)
        passport.authenticate('jwt', { session: false }), (req, res) => {
            res.json({
                message: 'You did it! dahsfbaushd',
                user: req.user,
                token: req.query
            })
        }
    } catch(err) {
        console.error('You have a error getUserById', err);
        return   res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        });
    }
}

export async function authDni(req, res){
    const { dni, issue } = req.body;

    res.status(200).send({
        message: "Dni & date correct!",
        data:{dni,issue}
    })
}

export async function getA(req,res,next){

        const token = req.headers['x-access-token'];

        if (!token){
            return res.status(401).json({
                auth: false,
                message: ' no token provided'
            });
        }

        const decoded = jwt.verify(token, 'globoazul_token');
        const user = await User.findByPk(decoded.user.id_user);

        if(!user){
            return res.status(404).send('no user found');
        }

        res.json(user);
}

export async function updatePassword(req, res){

    try{



        const {username,password,password_new} = req.body

        const passwordNewEncrypt = await bcrypt.hash(password_new, parseInt(process.env.BCRYPT_ROUNDS));
        const user = await tbl_user.findOne({
            where: {
                username: username,
               }
        })
       console.log(user.password)
        const validate = await bcrypt.compare(password, user.password)


        if(validate){

            await tbl_user.update({ password: passwordNewEncrypt},{where:{username:username}})




            const usuarioID = user.id
            const userName = user.username

            let token =await createToken(req,res,usuarioID,userName)
            return res.json({

                message: 'Password Update!',
                data:{token}

            })
        }

        return res.status(400).json({
            message: 'Incorrect Password!'
        })

    } catch(err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

/**export async function verifyphone(req,res){
    const accountSid = 'ACb7192d5962a2abf4df526e4604a007d2';
    const authToken = 'b0910ee5b3568393bc7cdd014e71237a';
    //const serviceid = 'VA09c4463ba2a961edeac414bbbc263334';
    const client = require('twilio')(accountSid, authToken);
    //const service = require('twilio')(serviceid);
    const {  phonenumber } = req.body;

    try {

        console.log(client);
        const response = await client.verify.services('VA09c4463ba2a961edeac414bbbc263334')
        .verifications
        .create({to:`+51${phonenumber}`, channel:'sms'})
        .then(verification => console.log(verification));;
        res.status(200).send({
            message: "Verification is sent!!",
            phonenumber,
            response

        })

    } catch (error) {
        res.status(400).send({
            message: "Wrong phone number :(",
            phonenumber,
            error
        });
        console.log(error);
    }

}
 */
export async function updateTokenDevice(req, res){

    try{

        const userID = req.user.id
        console.log(userID)

        const {tokenDevice} = req.body


        const TokenDevice = await tbl_user.findOne({
            where: {
                id: userID,
                token_device: tokenDevice

               }
        })


        if(!TokenDevice){

            await tbl_user.update({ token_device: tokenDevice},{where:{id:userID}})



            return res.json({

                message: 'Token Device Update!',


            })
        }

        return res.json({
            message: 'the token device already exists!'
        })

    } catch(err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

export async function deleteUser(req, res){

    try{
        await tbl_user.update({
                state: 0
            }, {
                where: {
                    id: req.params.id
                }
            })

            return res.json({
                message: 'User deleted'
            })
        

    } catch(err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}
export async function updateTutorial(req, res){

    try{
        await tbl_user.update({
                tutorial: 1
            }, {
                where: {
                    id: req.user.id
                }
            })

            return res.json({
                message: 'Update tutorial succeful'
            })
        

    } catch(err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}