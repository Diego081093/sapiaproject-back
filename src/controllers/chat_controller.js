import psychologist_routes from "../routes/psychologist_routes";

const {tbl_chat, tbl_chat_participants, tbl_chat_participants_message, tbl_profile, tbl_session, tbl_avatar, tbl_room, tbl_room_participants} = require('../models')
const Sequelize = require('sequelize');
const Op = Sequelize.Op

async function getChatHelpdesk(idProfile) {
    const chat = await tbl_chat.findOne({
        attributes: [
            'id'
        ],
        where: {
            helpdesk: 1
        },
        include: [
            {
                model: tbl_chat_participants,
                as: 'ChatParticipant',
                attributes: [
                    'id',
                    ['tbl_chat_id', 'idChat'],
                    ['tbl_profile_id', 'idProfile']
                ],
                where: {
                    tbl_profile_id: idProfile
                }
            }
        ]
    })

    if (chat != null) {
        const messages = await tbl_chat_participants_message.findAll({
            attributes: [
                'id',
                ['tbl_chat_participants_id', 'idChatParticipant'],
                'message',
                [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'],
                [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chat.ChatParticipant.id + ' THEN \'out\' ELSE \'in\' END'), 'type']
            ],
            include: [
                {
                    model: tbl_chat_participants,
                    as: 'ChatPaticipants',
                    attributes: [],
                    include: [
                        {
                            model: tbl_chat,
                            as: 'Chat',
                            where: {
                                id: chat.id
                            }
                        },
                        {
                            model: tbl_profile,
                            as: 'Profile'
                        }
                    ],
                    where: {
                        tbl_chat_id: chat.id
                    }
                }
            ],
            order: [['id', 'ASC']]
        })

        return {
            id: chat.id,
            ChatParticipant: chat.ChatParticipant,
            messages: messages
        }
    } else {
        return null
    }
}

export async function getChat(req, res) {
    try {
        const idProfile = req.user.idProfile
        const idChat = req.params.id

        const chat = await tbl_chat.findOne({
            attributes: [
                'id'
            ],
            include: [
                {
                    model: tbl_chat_participants,
                    as: 'ChatParticipant',
                    attributes: [
                        'id',
                        ['tbl_chat_id', 'idChat'],
                        ['tbl_profile_id', 'idProfile']
                    ],
                    where: {
                        tbl_profile_id: idProfile
                    }
                }
            ],
            where: {
                id: idChat
            }
        })

        const messages = await tbl_chat_participants_message.findAll({
            attributes: [
                'id',
                ['tbl_chat_participants_id', 'idChatParticipant'],
                'message',
                [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'],
                [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chat.ChatParticipant.id + ' THEN \'out\' ELSE \'in\' END'), 'type']
            ],
            include: [
                {
                    model: tbl_chat_participants,
                    as: 'ChatPaticipants',
                    include: [
                        {
                            model: tbl_chat,
                            as: 'Chat',
                            where: {
                                id: idChat
                            },
                            attributes: []
                        },
                        {
                            model: tbl_profile,
                            as: 'Profile',
                            attributes: []
                        }
                    ],
                    where: {
                        tbl_chat_id: idChat
                    },
                    attributes: []
                }
            ],
            order: [['id', 'ASC']]
        })
        const datos = {
            id: idChat,
            ChatParticipant: chat.ChatParticipant,
            messages: messages
        }
        res.json({
            data: datos
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getChatSession(req, res) {
    try {
        const idProfile = req.user.idProfile
        const idSession = req.params.id

        const session = await tbl_session.findOne({
            where: {
                id: idSession
            }
        })

        const chat = await tbl_chat.findOne({
            attributes: [
              'id'
            ],
            where: {
                helpdesk: 0,
                tbl_room_id: null
            },
            include: [
                {
                    model: tbl_chat_participants,
                    as: 'ChatParticipant',
                    attributes: [
                        'id',
                        ['tbl_chat_id', 'idChat'],
                        ['tbl_profile_id', 'idProfile']
                    ],
                    where: {
                        tbl_profile_id: idProfile
                    }
                },
                {
                    model: tbl_chat_participants,
                    as: 'ChatParticipantPsychologist',
                    attributes: [],
                    where: {
                        tbl_profile_id: session.tbl_profile_psychologist_id
                    }
                }
            ]
        })
        let messages = []
        if (chat === null) {
            const chat = await tbl_chat.create({
                start: Date.now(),
                helpdesk: 0
            })

            const chatParticipant = await tbl_chat_participants.create({
                tbl_chat_id: chat.id,
                tbl_profile_id: idProfile
            })

            const chatParticipantPsychologist = await tbl_chat_participants.create({
                tbl_chat_id: chat.id,
                tbl_profile_id: session.tbl_profile_psychologist_id
            })
        }else{
            messages = await tbl_chat_participants_message.findAll({
                attributes: [
                    'id',
                    ['tbl_chat_participants_id', 'idChatParticipant'],
                    'message',
                    [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'],
                    [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chat.ChatParticipant.id + ' THEN \'out\' ELSE \'in\' END'), 'type']
                ],
                include: [
                    {
                        model: tbl_chat_participants,
                        as: 'ChatPaticipants',
                        attributes: [],
                        include: [
                            {
                                model: tbl_chat,
                                as: 'Chat',
                                where: {
                                    id: chat.id
                                }
                            },
                            {
                                model: tbl_profile,
                                as: 'Profile'
                            }
                        ],
                        where: {
                            tbl_chat_id: chat.id
                        }
                    }
                ]
            })
        }

        res.json({
            data: {
                id: chat.id,
                ChatParticipant: chat.ChatParticipant,
                messages: messages
            }
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function joinHelpdesk(req, res) {
    const {id}=req.body
    const chatParticipantHelpdesk = await tbl_chat_participants.create({
        tbl_chat_id: id,
        tbl_profile_id: req.user.idProfile
    })

    const chat = await tbl_chat.findOne({
        attributes: [
            'id'
        ],
        where: {
            id: id
        },
        include: [
            {
                model: tbl_chat_participants,
                as: 'ChatParticipant',
                attributes: [
                    'id',
                    ['tbl_chat_id', 'idChat'],
                    ['tbl_profile_id', 'idProfile']
                ],
                where: {
                    tbl_profile_id: req.user.idProfile
                }
            }
        ]
    })

    const messages = await tbl_chat_participants_message.findAll({
        attributes: [
            'id',
            ['tbl_chat_participants_id', 'idChatParticipant'],
            'message',
            [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'],
            [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chat.ChatParticipant.id + ' THEN \'in\' ELSE \'out\' END'), 'type']
        ],
        include: [
            {
                model: tbl_chat_participants,
                as: 'ChatPaticipants',
                attributes: [],
                include: [
                    {
                        model: tbl_chat,
                        as: 'Chat',
                        where: {
                            id: chat.id
                        }
                    },
                    {
                        model: tbl_profile,
                        as: 'Profile'
                    }
                ],
                where: {
                    tbl_chat_id: chat.id
                }
            }
        ]
    })

    res.json({
        data: {
            id: chat.id,
            ChatParticipant: chat.ChatParticipant,
            messages: messages
        }
    });
}

export async function getHelpdesk(req, res) {
    try {
        let data = await getChatHelpdesk(req.user.idProfile)
        if (data == null) {
            const chat = await tbl_chat.create({
                start: Date.now(),
                helpdesk: 1
            })

            const chatParticipant = await tbl_chat_participants.create({
                tbl_chat_id: chat.id,
                tbl_profile_id: req.user.idProfile
            })

            data = await getChatHelpdesk(req.user.idProfile)
        }

        res.json({
            data
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getAllHelpdesk(req, res) {
    try {
        let where = {
            helpdesk: 1
        }
        if(req.query.reference !== '') {
            where = [
                Sequelize.where(Sequelize.literal('"ChatParticipant->Profile"."firstname"'),
                    {
                        [Op.iLike]: '%' + req.query.reference + '%'
                    }
                ),
                Sequelize.where(Sequelize.literal('"tbl_chat"."helpdesk"'),
                    {
                        [Op.eq]: 1
                    }
                )
            ]
        }
        let chats = await tbl_chat.findAll({
            attributes: [
                'id',
                'start',
                [Sequelize.literal('"ChatParticipantHelpdesk->Profile"."id"'), 'helpdesker'],
            ],
            where: where,
            include: [
                {
                    model: tbl_chat_participants,
                    as: 'ChatParticipant',
                    attributes: ['id'],
                    include: {
                        model: tbl_profile,
                        as: 'Profile',
                        attributes: ['id', 'firstname','image'],
                        
                    }
                },
                {
                    model: tbl_chat_participants,
                    as: 'ChatParticipantHelpdesk',
                    attributes: ['id'],
                    include: {
                        model: tbl_profile,
                        as: 'Profile',
                        attributes: [],
                        where: {
                            tbl_role_id: {
                                [Op.notIn]: [1,2]
                            }
                        }
                    },
                    required: false
                }
            ],
            group: ['"tbl_chat"."id"',
                '"ChatParticipantHelpdesk->Profile"."id"',
                '"ChatParticipant"."id"',
                '"ChatParticipant->Profile"."id"',
                '"ChatParticipant->Profile->Avatar"."id"',
                '"ChatParticipantHelpdesk"."id"'],
            having:
                Sequelize.where(Sequelize.literal('"ChatParticipantHelpdesk->Profile"."id"'),
                    {
                        [Op.or]: [{[Op.eq]: null}, {[Op.eq]: req.user.idProfile}]
                    }
                )
        })



        await Promise.all(chats.map(async (item, key) => {
            const messages = await tbl_chat_participants_message.findOne({
                attributes: [
                    'id',
                    ['tbl_chat_participants_id', 'idChatParticipant'],
                    'message',
                    'created_at',
                    [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'],
                    [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + item.dataValues.ChatParticipant.id + ' THEN \'in\' ELSE \'out\' END'), 'type']
                ],
                include: [
                    {
                        model: tbl_chat_participants,
                        as: 'ChatPaticipants',
                        attributes: [],
                        include: [
                            {
                                model: tbl_chat,
                                as: 'Chat',
                                where: {
                                    id: item.dataValues.id
                                }
                            },
                            {
                                model: tbl_profile,
                                as: 'Profile'
                            }
                        ],
                        where: {
                            tbl_chat_id: item.dataValues.id
                        }
                    }
                ],
                order: [['id', 'DESC']]
            })
            item.dataValues.lastMessage = {}
            if(messages !== null){
                item.dataValues.lastMessage = messages
                item.dataValues.updated = messages.dataValues.created_at
            }
            return item;
        })).then((chats) => {
            chats.sort( compare );
            res.json({
                data: chats
            })
        });


    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getChatPrivate(req, res) {

    if(req.query.idRoom > 0 && req.query.idPatient > 0) {
        try {

            const room = await tbl_room.findOne({
                where: {
                    id: req.query.idRoom
                },
                include: {
                    model: tbl_room_participants,
                    attributes: ['id', ['tbl_profile_participant_id', 'idProfile']],
                    include: {
                        model: tbl_profile,
                        as: 'Profile_participant',
                        where: {
                            'tbl_role_id': 2
                        },
                        attributes: []
                    }
                },
                attributes: ['id', 'title']
            })

            if(room != null) {

                let include = [{
                    model: tbl_chat_participants,
                    as: 'ChatParticipant',
                    attributes: ['id'],
                    where: {
                        tbl_profile_id: req.query.idPatient
                    }
                }]
                if(room.tbl_room_participants[0]) {
                    include.push({
                        model: tbl_chat_participants,
                        as: 'ChatParticipantPsychologist',
                        attributes: ['id'],
                        where: {
                            tbl_profile_id: room.tbl_room_participants[0].dataValues.idProfile
                        }
                    })
                }
                if(room.tbl_room_participants[1]) {
                    include.push({
                        model: tbl_chat_participants,
                        as: 'ChatParticipantPsychologistAssistant',
                        attributes: ['id'],
                        where: {
                            tbl_profile_id: room.tbl_room_participants[1].dataValues.idProfile
                        }
                    })
                }


                let chat = await tbl_chat.findOne({
                    attributes: [
                        'id'
                    ],
                    where: {
                        helpdesk: 0,
                        tbl_room_id: null,
                        state: 1
                    },
                    include: include
                })

                if(chat == null) {

                    chat = await tbl_chat.create({
                        start: Date.now(),
                        helpdesk: 0
                    })

                    //add patient to chat
                    const chatParticipant = await tbl_chat_participants.create({
                        tbl_chat_id: chat.id,
                        tbl_profile_id: req.query.idPatient
                    })

                    //add psychologist to chat
                    let data = await Promise.all(
                        room.tbl_room_participants.map(async (item, key) => {
                            const chatParticipant = await tbl_chat_participants.create({
                                tbl_chat_id: chat.id,
                                tbl_profile_id: item.dataValues.idProfile
                            })
                            return item;
                        }))

                }

                const chatResponse = await tbl_chat.findOne({
                    attributes: [
                        'id'
                    ],
                    include: [
                        {
                            model: tbl_chat_participants,
                            as: 'ChatParticipant',
                            attributes: [
                                'id',
                                ['tbl_chat_id', 'idChat'],
                                ['tbl_profile_id', 'idProfile']
                            ],
                            where: {
                                tbl_profile_id: req.query.idPatient
                            }
                        }
                    ],
                    where: {
                        id: chat.id
                    }
                })

                const messages = await tbl_chat_participants_message.findAll({
                    attributes: [
                        'id',
                        ['tbl_chat_participants_id', 'idChatParticipant'],
                        'message',
                        [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'],
                        [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chatResponse.ChatParticipant.id + ' THEN \'out\' ELSE \'in\' END'), 'type'],
                        'created_at'
                    ],
                    include: [
                        {
                            model: tbl_chat_participants,
                            as: 'ChatPaticipants',
                            include: [
                                {
                                    model: tbl_chat,
                                    as: 'Chat',
                                    where: {
                                        id: chat.id
                                    },
                                    attributes: []
                                },
                                {
                                    model: tbl_profile,
                                    as: 'Profile',
                                    attributes: []
                                }
                            ],
                            where: {
                                tbl_chat_id: chat.id
                            },
                            attributes: []
                        }
                    ],
                    order: [['id', 'ASC']]
                })

                res.json({
                    data: {
                        id: chatResponse.id,
                        ChatParticipant: chatResponse.ChatParticipant,
                        messages: messages
                    }
                })
            } else {
                res.status(404).json({
                    message: 'Room not found'
                });
            }
        } catch (error) {
            res.status(500).json({
                message: 'Something went wrong'
            });
        }
    } else {
        res.status(500).json({
            message: 'Nothing to search...'
        });
    }


}



function compare( a, b ) {
    if ( a.dataValues.updated < b.dataValues.updated ){
        return 1;
    }
    if ( a.dataValues.updated > b.dataValues.updated ){
        return -1;
    }
    return 0;
}