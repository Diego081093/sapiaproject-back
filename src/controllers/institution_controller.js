// import {Op} from "sequelize";
// import {DateTime} from "luxon";

const {
    tbl_institution
} = require('../models')

export async function getAll(req, res){
    try {

        const { type } = req.query

        const list = await tbl_institution.findAll({
            where: { type }
        })
        res.json({
            data: list
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
