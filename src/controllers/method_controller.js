// import Role from '../models/role_model'
const { tbl_method} = require('../models')
const Sequelize = require('sequelize');

export async function getListMethod(req, res) {
            try {
                
                
                const list_method = await tbl_method.findAll({
                    attributes: [
                        'id',
                        'name',
                        'image'
                    ],
                    where: {
                        state: 1
                    }
                    })
                
              
                res.json({
                   data: list_method
                });
                } catch (error) {
                    console.log(error);
                    res.status(500).json({
                        message: 'Something went wrong'
                    });
                }
            }
    