const { tbl_benefit, tbl_company_benefit, tbl_profile, tbl_benefit_profile } =require('../models')
const { DateTime } = require('luxon')
const Sequelize = require('sequelize');
const url = require('url');
const { isValidBase64Img, s3UploadBase64Image, MottivaConstants } = require('./../helpers')
const Op = Sequelize.Op
export async function getListCompanyBenefits(req, res) {
    try {
        const companys = await tbl_company_benefit.findAll()
        res.json({

            data: companys
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function getListBenefits(req, res) {
    try {

        let where = {}
        if (req.query.search) {
            where.description = {
                [Op.like]: '%' + req.query.search + '%'
            }
        }

      let limit = 20,
        offset = 0


        if( req.query.limit ){

            limit = parseInt(req.query.limit)
        }

        if( req.query.offset ){

            offset = parseInt(req.query.offset)
        }

        const benefits = await tbl_benefit.findAll({
            where: where,
            attributes: [
                'id',
                'name',
                'image',
                'description',
                'point',
            ],
            include: [
                {
                    model: tbl_company_benefit,
                    as: 'CompanyBenefit',
                    attributes: [
                        'id',
                        'name',
                        'image'
                    ]
                }
            ]
        })
        const {count, rows: sessions} = await tbl_benefit.findAndCountAll({
            where: where,
            attributes: [
                'id',
                'name',
                'image',
                'description',
                'point',
            ],
            include: [
                {
                    model: tbl_company_benefit,
                    as: 'CompanyBenefit',
                    attributes: [
                        'id',
                        'name',
                        'image'
                    ]
                }
            ],
            limit,
            offset,
            distinct: true
        });
console.log(count)
        let urlPath = req.baseUrl + req.path

        if( urlPath.substring(urlPath.length - 1) == '/' ){

            urlPath = urlPath.substring(0, urlPath.length - 1)
        }

        let previousPageUrl = null,
            nextPageUrl = null

        if( offset > 0 ){

            let p_query_offset = offset - limit

            if( (offset - limit) < 0 ){

                p_query_offset = 0
            }

            previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: p_query_offset,
                    limit
                }
            });
        }

        if( offset + limit < count ){

            nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: offset + limit,
                    limit
                }
            });
        }
        res.json({

            data: {count,
            previous: previousPageUrl,
            next: nextPageUrl,
            limit,
            offset,
            benefits}
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function createBenefit(req, res){

    try{

        const {
            company_id,
            name,
            description,
            point
        } = req.body

        const { isValid, buffer, ext } = isValidBase64Img(req.body.image)

        if( ! isValid ){

            return res.status(422).json({
                message: 'The given data was invalid.',
                errors: {
                    image: [
                        "The image must be a valid base64"
                    ]
                }
            })
        }

        req.body.image = {
            buffer,
            ext
        }

        const imageUploaded = await s3UploadBase64Image('uploads/profile-photo/', req.body.image)

        console.log('')
        console.log('')
        console.log(imageUploaded)
        console.log('')
        console.log('')

        if( ! imageUploaded.success ){

            return res.status(500).json({
                message: 'Something went wrong uploading the image'
            })
        }

        await tbl_benefit.create({
            tbl_company_benefit_id: company_id,
            name,
            image: imageUploaded.data.Location,
            description,
            point
        })

        res.json({
            message: 'Benefit created'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function exchangeBenefits(req, res) {

    try{

        if( req.body.benefit_id ){

            await tbl_benefit_profile.create({
                tbl_benefit_id: req.body.benefit_id,
                tbl_profile_id: req.user.idProfile
            })

            return res.json({
                message: 'Benefit exchanged'
            })
        }

        res.status(400).json({
            message: 'Empty data'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function getBenefit(req, res) {
    try {
        const { id } = req.params;

        const benefit = await tbl_benefit.findOne({
            where: { state: 1, id },
            attributes: [
                'id',
                'name',
                'image',
                'description',
                'point',
            ],
            include: [
                {
                    model: tbl_company_benefit,
                    as: 'CompanyBenefit',
                    attributes: [
                        'id',
                        'name',
                        'image'
                    ]
                }
            ]
        })
        res.json({ data: benefit });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getBenefitsProfile(req, res) {

    try{

        if( req.user.idRole == MottivaConstants.HELPDESK_ROLE ){

            const benefits = await tbl_benefit_profile.findAll({
                include: [
                    {
                        model: tbl_profile,
                        as: 'Profile',
                        attributes: [
                            'firstname',
                            'lastname',
                            'phone',
                            'points'
                        ]
                    },
                    {
                        model: tbl_benefit,
                        as: 'Benefit',
                        include: {
                            model: tbl_company_benefit,
                            as: 'CompanyBenefit',
                            attributes: [
                                'name',
                                'image',
                                'state'
                            ]
                        },
                        attributes: [
                            'name',
                            'image',
                            'description',
                            'point',
                            'state'
                        ]
                    }
                ],
                attributes: [
                    'id',
                    'state'
                ]
            })

            return res.json({
                message: 'Benefits profile found',
                data: benefits
            })
        }

        res.status(401).json({
            message: 'Access denied with provided credentials'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getHistoryBenefits(req, res) {
    try {
        const { idProfile } = req.user
        const now = DateTime.now()
        const benefit = await tbl_benefit.findAll({
            where: { state: 1, created_at: { [Op.lt]: now.toString() } },
            attributes: [
                'id',
                'name',
                'image',
                'description',
                'point',
            ],
            include: [
                {
                    model: tbl_company_benefit,
                    as: 'CompanyBenefit',
                    attributes: [
                        'id',
                        'name',
                        'image'
                    ]
                },
                {
                    model: tbl_benefit_profile,
                    as: 'BenefitProfile',
                    where: { state: 1, tbl_profile_id: idProfile },
                    attributes: [ 'id' ]
                }
            ]
        })
        res.json({ data: benefit });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}