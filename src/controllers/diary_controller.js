const {tbl_diary} = require('../models')
const Sequelize = require('sequelize');


const Op = Sequelize.Op;

export async function putDiary(req, res) {
    try {
        const url = req.file ? req.file.location : null
        const taskId = req.body.taskId ? req.body.taskId : null
        const text = req.body.text ? req.body.text : null
        const profileId = req.user.idProfile
        const {feel, publico, date} = req.body
      if (taskId)
      { 
          let diary = await tbl_diary.findOne({
           where:
           {
            tbl_profile_id: profileId,
            tbl_task_id: taskId
           }
       })
       if (!diary)
       {
        let newDiaryText = await tbl_diary.create({
            tbl_profile_id: profileId,
            tbl_task_id: taskId,
            text: text,
            audio: url,
            feel: feel,
            date: date,
            public: publico
        });
        return res.json({
            message: 'Diary Created',
            data: newDiaryText
        })

       }
       else
       {
        const Newurl = req.file ? req.file.location : diary.url
        const Newfeel = req.body.taskId ? req.body.taskId : diary.feel
        const Newtext = req.body.text ? req.body.text : diary.text
        const Newdate = req.body.date ? req.body.date : diary.date
        const Newpublico = req.body.publico ? req.body.publico : diary.publico
        let DiaryText = await tbl_diary.update({
            text: Newtext,
            audio: Newurl,
            feel: Newfeel,
            date: Newdate,
            public: Newpublico
        },{
        where:
            {
                tbl_profile_id: profileId,
                tbl_task_id: taskId

            }
        });
        return res.json({
            message: 'Diary Update',
            data: DiaryText
        })

       }
    }
    else
    {
        let newDiary = await tbl_diary.create({
            tbl_profile_id: profileId,
            tbl_task_id: null,
            text: text,
            audio: url,
            feel: feel,
            date: date,
            public: publico
        });
        return res.json({
            message: 'Diary Created',
            data: newDiary
        })
    }
        

    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

export async function clearDiary(req, res) {
    try {
        const {idProfile} = req.user
        const {area} = req.body
        const {id} = req.params
        let body = {}
        if (area === 'text')
            body = {text: null}
        if (area === 'audio')
            body = {audio: null}
        await tbl_diary.update(body, {where: {id, tbl_profile_id: idProfile}})
        const diary = await tbl_diary.findOne({
            where: {
                id
            }
        })

        if (diary && !diary.text && !diary.audio) {
            await tbl_diary.destroy({where: {id}})
        }

        return res.json({
            message: `Update ${area} of Diary`
        })
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

export async function getListDateDiary(req, res) {
    try {
        const {idProfile} = req.user
        const month = req.query.month
        const year = req.query.year
        const dates = await tbl_diary.findAll({
            where: [
                Sequelize.where(Sequelize.literal('to_char("date", \'YYYY-MM\')'),
                    {[Op.like]: year + "-" + month}),
                Sequelize.where(Sequelize.literal('tbl_profile_id'),
                    {[Op.eq]: idProfile})
            ],
            attributes: [
                // specify an array where the first element is the SQL function and the second is the alias
                [Sequelize.fn('DISTINCT', Sequelize.literal('to_char("date", \'YYYY-MM-DD\')')), 'date'],

                // specify any additional columns, e.g. country_code
                // 'country_code'

            ]
        })
        //let prueba = Sequelize.where(fn("month", col("date")), month)
        //console.log(prueba)
        /*const dateDiary = await tbl_diary.findOne({ attributes:["date"],where: { 
            tbl_profile_id:idProfile, sequelize.fn("month", sequelize.col("date")), month }})
       let fecha = dateDiary.date
       const asda = new Date(fecha)
       console.log(fecha)
       const mes =asda.getMonth() + 1
        const año =asda.getFullYear()
       */
        return res.json({
            message: `Dates Availabilitys Diary`,
            data: dates
        })
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

export async function getListDateDiaryDetails(req, res) {
    try {
        const {idProfile} = req.user
        const date = req.query.date
        const diarys = await tbl_diary.findAll({
            where: [
                Sequelize.where(Sequelize.literal('to_char("date", \'YYYY-MM-DD\')'),
                    {[Op.like]: date}),
                Sequelize.where(Sequelize.literal('tbl_profile_id'),
                    {[Op.eq]: idProfile})
            ]
        })
        //let prueba = Sequelize.where(fn("month", col("date")), month)
        //console.log(prueba)
        /*const dateDiary = await tbl_diary.findOne({ attributes:["date"],where: { 
            tbl_profile_id:idProfile, sequelize.fn("month", sequelize.col("date")), month }})
       let fecha = dateDiary.date
       const asda = new Date(fecha)
       console.log(fecha)
       const mes =asda.getMonth() + 1
        const año =asda.getFullYear()
       */
        return res.json({
            message: `Dates Availabilitys Diary`,
            data: diarys
        })
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

export async function getListDiaryViewPsychologist(req, res) {
    try {
        const {isPatient, month, year} = req.query
        const where = [
            Sequelize.where(Sequelize.literal('state'), {[Op.eq]: 1}),
            Sequelize.where(Sequelize.literal('tbl_profile_id'), {[Op.eq]: isPatient})
        ]
        if (month && year)
            where.push(
                Sequelize.where(Sequelize.literal('to_char("date", \'YYYY-MM\')'), {[Op.like]: year + '-' + month})
            )
        const dates = await tbl_diary.findAll({where})

        const lista = []
        dates.map((value) => {
            const date = new Date(value.dataValues.date)
            const day = date.toJSON().substr(0, 4)
            const month = date.toJSON().substr(5, 2)
            const year = date.toJSON().substr(8, 2)
            const index = lista.findIndex(function (el) {
                return el.date === `${day}-${month}-${year}` // or el.nombre=='T NORTE';
            })
            if (index === -1) {
                lista.push({
                    date: `${day}-${month}-${year}`,
                    diaries: []
                })
                lista[lista.length - 1].diaries.push(value)
            } else {
                lista[index].diaries.push(value)
            }
        })
        return res.json({
            message: `Dates Availabilitys Diary`,
            data: lista
        })
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

export async function getDetailDiary(req, res) {
    try {
        const {idProfile} = req.user
        const id = req.params.id
        //const month = req.query.month

        console.log(idProfile)

        const detailDiary = await tbl_diary.findOne({where: {tbl_profile_id: idProfile, id: id}})


        return res.json({
            message: `Diary found`,
            data: detailDiary
        })
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

export async function getListDiary(req, res) {
    try {
        const {idProfile} = req.user
        //const month = req.query.month

        console.log(idProfile)

        const Diary = await tbl_diary.findAll({where: {tbl_profile_id: idProfile,},order: [['date', 'DESC']]})


        return res.json({
            message: `Diary List`,
            data: Diary
        })
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}

export async function seenDiary(req, res) {
    try {
        const id = req.params.id

        let updateDiary = await tbl_diary.update(
            { seen: 1 },
            { where: { id : id } }
        )

        if (updateDiary) {
            const diary = await tbl_diary.findOne({
                where: {
                    id: id
                }
            })
            return res.json({
                message: `diary updated`,
                data: diary
            })
        }else{
            res.status(404).json({
                message: 'Diary not found',
            })
        }

    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}