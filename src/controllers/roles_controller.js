// import Role from '../models/role_model'
const { tbl_role } = require('../models')

export async function getListRole(req, res){
    try {
        const roles = await tbl_role.findAll({where:{state:1}});
        res.json({
            data: roles
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function createRole(req, res){
    const{name,alias}=req.body;
    try {
        let newRoles = await tbl_role.create({
            name,
            alias
        },{
            fields: ['name','alias']
        });

        if (newRoles) {
            res.json({
                message: 'rol created',
                date: newRoles
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function deleteRole(req, res) {
    try {
        const { id } = req.params;
        const deleteRowCount = await tbl_role.destroy({
            where: {
                id
            }
        });
        res.json({
            message: 'Role deleted',
            count: deleteRowCount
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
