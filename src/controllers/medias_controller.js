// import Role from '../models/role_model'
const { tbl_medias, tbl_media_profile, tbl_media_role,tbl_category_medias } = require('../models')
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

export async function getMedia(req, res) {
    try {
        let {id} = req.params;
        let whereRole = {}
        if (req.query.role) {
            whereRole.tbl_role_id = req.query.role
        } else {
            whereRole.tbl_role_id = req.user.idRole
        }

        const media = await tbl_medias.findOne({
            where: {
                id: id
            },
            attributes: [
                'id',
                ['tbl_category_media_id', 'idCategory'],
                ['tbl_method_id', 'idMethod'],
                'name',
                'type',
                'file',
                'image',
                'duration',
                'description',
                'points',
                'state'
            ],
            include: [
                {
                    model: tbl_media_role,
                    as: 'Role',
                    where: whereRole,
                    attributes: [
                        'id'
                    ]
                },
                {
                    model: tbl_media_profile,
                    as: 'onProfile',
                    where: {
                        tbl_profile_id: req.user.idProfile
                    },
                    attributes: [
                        'id',
                        'done',
                        'favorite'
                    ],
                    required: false
                }
            ]
        })
        res.json({
            data: media
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getListMedias(req, res) {
    try {
        const { withMethod, type, ref, method, category, role, history, favorite, archived, marked  } = req.query
        console.log('method', withMethod)
        let where = {
            tbl_method_id: (withMethod == 0) ? { [Op.is]: null } : { [Op.not]: null }
        }

        if(type) {
            where.type = type === "video" ? "1" : "2";
        }
        if (ref) {
            where.name ={[Op.iLike]:'%' + ref + '%'}
            if (method) {
                where.tbl_method_id = {[Op.in]: method.split(',') };
            }
            if (category) {
                where.tbl_category_media_id = {[Op.in]: category.split(',') } ;
            }
        
        }
        let whereRole = {}
        if (role && role.length > 0) {
            whereRole.tbl_role_id = role
        } else {
            whereRole.tbl_role_id = req.user.idRole
        }
        let whereMediaProfile = {
            tbl_profile_id: req.user.idProfile,
            state:1
        }
        let requiredDone = false
        if (history == 1) {
            whereMediaProfile.done = { [Op.gte]: 1 }
            requiredDone = true;
        }
        if (favorite == 1) {
            whereMediaProfile.favorite = 1
            requiredDone = true;
        }
        if (archived) {
            whereMediaProfile.state = 0
            requiredDone = true
        }
        if (marked) {
            whereMediaProfile.watch_later = true
            requiredDone = true
        }
       
        const medias = await tbl_medias.findAll(
            {
                attributes: [
                    'id',
                    ['tbl_category_media_id', 'idCategory'],
                    ['tbl_method_id', 'idMethod'],
                    'name',
                    'type',
                    'file',
                    'image',
                    'duration',
                    'description',
                    'points',
                    'state'
                ],
                where: where,
                include: [
                    {
                        model: tbl_media_role,
                        where: whereRole,
                        as: 'Role',
                        attributes: [
                            ['tbl_role_id', 'id']
                        ],
                    },
                    {
                        model: tbl_media_profile,
                        as: 'onProfile',
                        where: whereMediaProfile,
                        attributes: [
                            'id',
                            'done',
                            'favorite'
                        ],
                        required: requiredDone
                    }],
            }
        );
           
        res.json({
            data: medias
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function watchLaterMedia(req, res) {

    try{

        const validRoles = [1, 2]

        if( ! validRoles.includes(req.user.idRole) ){

            return res.status(400).json({
                message: 'Access denied with provided credentials'
            })
        }

        const media = await tbl_medias.findByPk(req.params.id, {
            attributes: ['id']
        })

        if( ! media ){

            return res.status(404).json({
                message: 'Media not found'
            })
        }

        const [media_profile, created] = await tbl_media_profile.findOrCreate({
            where: {
                tbl_media_id: req.params.id,
                tbl_profile_id: req.user.idProfile,
            },
            defaults: {
                done: 0,
                favorite: 0,
                state: 1,
                watch_later: 1,
            }
        })

        if( ! created ){

            await media_profile.update({
                watch_later: 1
            })
        }

        return res.json({
            message: 'Item saved successfully'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function favoriteMedia(req, res) {

    try{

        const validRoles = [1, 2]

        if( ! validRoles.includes(req.user.idRole) ){

            return res.status(400).json({
                message: 'Access denied with provided credentials'
            })
        }

        const media = await tbl_medias.findByPk(req.params.id, {
            attributes: ['id']
        })

        if( ! media ){

            return res.status(404).json({
                message: 'Media not found'
            })
        }

        const [media_profile, created] = await tbl_media_profile.findOrCreate({
            where: {
                tbl_media_id: req.params.id,
                tbl_profile_id: req.user.idProfile,
            },
            defaults: {
                done: 0,
                favorite: 1,
                state: 1,
                watch_later: 0,
            }
        })

        if( ! created ){

            await media_profile.update({
                favorite: 1
            })
        }

        return res.json({
            message: 'Item saved as favorite successfully'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function postFavoriteMedia(req, res) {
    try {
        const IDprofile = req.user.idProfile;
        const {id}=req.body;
        
        const media_favorite = await tbl_media_profile.findOne({ where: { tbl_profile_id: IDprofile,tbl_media_id: id } })
        const favorites= media_favorite ? await tbl_media_profile.update({favorite:1 }, {where: {tbl_profile_id: IDprofile,tbl_media_id: id }}) : await tbl_media_profile.create({tbl_profile_id: IDprofile,tbl_media_id:id,favorite:1 });
      
        res.json({
            message: "Se guardó el favorito satisfactoriamente",
        });
        } catch (error) {
            console.log(error);
            res.status(500).json({
                message: 'Something went wrong'
            });
        }
    }
    export async function postDesactivateFavoriteMedia(req, res) {
        try {
            const IDprofile = req.user.idProfile;
            const {id}=req.body;
            
            const media_favorite = await tbl_media_profile.findOne({ where: { tbl_profile_id: IDprofile,tbl_media_id: id,favorite:1,state:1 } })
            const favorites= media_favorite ? await tbl_media_profile.update({favorite:0}, {where: {tbl_profile_id: IDprofile,tbl_media_id: id }}) : await tbl_media_profile.create({tbl_profile_id: IDprofile,tbl_media_id:id,favorite:0 });
          
            res.json({
                message: "Se desactivó el favorito satisfactoriamente",
            });
            } catch (error) {
                console.log(error);
                res.status(500).json({
                    message: 'Something went wrong'
                });
            }
        }
        export async function ListCategoryMedia(req, res) {
            try {
                
                
                const list_category = await tbl_category_medias.findAll({
                    attributes: [
                        'id',
                        'name'
                    ],
                    where:{
                        state:1
                    }
                    })
                
              
                res.json({
                   data: list_category
                });
                } catch (error) {
                    console.log(error);
                    res.status(500).json({
                        message: 'Something went wrong'
                    });
                }
            }
            export async function postDoneMedia(req, res) {
                try {
                    const IDprofile = req.user.idProfile;
                    const {id}=req.body;
                    const {done}=req.body
                    
                    const media_done = await tbl_media_profile.findOne({ where: { tbl_profile_id: IDprofile,tbl_media_id: id,state:1 } })
                    const insertDone= media_done ? await tbl_media_profile.update({done:done }, {where: {tbl_profile_id: IDprofile,tbl_media_id: id }}) : await tbl_media_profile.create({tbl_profile_id: IDprofile,tbl_media_id:id,done:done });
                  
                    res.json({
                        message: "Se guardó el done satisfactoriamente",
                    
                    });
                    } catch (error) {
                        console.log(error);
                        res.status(500).json({
                            message: 'Something went wrong'
                        });
                    }
                }
    