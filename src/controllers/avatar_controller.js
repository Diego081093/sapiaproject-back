const {tbl_avatar} =require('../models')

export async function getListAvatar(req, res){
    try {
        const avatars = await tbl_avatar.findAll({where:{default:1}});
        res.json({
            data: avatars
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function createAvatar(req, res){
    const {image}=req.body;

    try{
        let newAvatar = await tbl_avatar.create({
            image
        });
        if(newAvatar){

            res.json({
                message: 'avatar created',
                date: newAvatar
            });
        }


    }catch(error){
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}

export async function getAvatar(req,res){
    try {
        const {idProfile} = req.params;
        const profiles = await tbl_profile.findByPk({
            include: {
                model: tbl_avatar,
                attributes: [
                    'id',
                    'image'
                ]
            },
            where: {
                id: idProfile,
                state:1
            }
        });

        res.json({
            data: profiles
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}