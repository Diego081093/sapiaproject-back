const { Op } = require("sequelize")
const { tbl_blocked, tbl_profile, tbl_reason, tbl_session } = require('../models')

export async function createBlocked(req, res){

    const { idProfile } = req.user

    const idPsychologist = req.params.id
  
    const {
        reasonId,
        comment
    } = req.body;

    try{
        let blocked = await tbl_blocked.findOne({
            where: { tbl_profile_psychologist_id : idPsychologist, tbl_profile_pacient_id: idProfile, state: 1 }
        })
        if (blocked === null) {
            let newBlocked = await tbl_blocked.create(
                {
                    tbl_profile_pacient_id: idProfile,
                    tbl_profile_psychologist_id: idPsychologist,
                    tbl_reason_id: reasonId,
                    comment
                },
            )
            if (newBlocked) {
                let updateSession = await tbl_session.update(
                    { state: 0 },
                    { where: { tbl_profile_psychologist_id : idPsychologist, tbl_profile_pacient_id: idProfile } }
                )
                if (updateSession)
                    res.status(200).json({
                        message: 'Psychologist blocked',
                        data: newBlocked
                    })
                else
                    res.status(500).json({
                        message: 'Something went wrong'
                    });
            } else {
                res.status(500).json({
                    message: 'Something went wrong'
                });
            }
        } else {
            await tbl_session.update(
                { state: 0 },
                { where: { tbl_profile_psychologist_id : idPsychologist, tbl_profile_pacient_id: idProfile } }
            )
            res.status(200).json({
                message: 'User already blocked'
            });
        }

    }catch(error){
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getListBlocked(req, res){


    try{

        let listBlocked = await tbl_blocked.findAll({
            include:[
                {
                    model:tbl_profile,
                    as:'Psychologist',
                    attributes:[
                        'id',
                        'lastname',
                        'firstname'
                    ]
                },
                {
                    model:tbl_profile,
                    as:'Patient',
                    attributes:[
                        'id',
                        'lastname',
                        'firstname'
                    ]
                },
                {
                    model:tbl_reason,
                    as:'Reason',
                }
        ]
        });

            res.json({
                message: 'Blocked Found',
                data: listBlocked
            });

    }catch(error){
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}