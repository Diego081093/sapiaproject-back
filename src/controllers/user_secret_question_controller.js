const { tbl_user_secret_question } = require('../models')

export async function getUserSecretQuestion(req, res){
    try {
        const answer = await tbl_user_secret_question.findAll();
        res.json({
            data: answer
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}

export async function createUserSecretQuestion(req, res){

    const {
        user_id,
        secret_question_id,
        answer,
        state
    } = req.body;

    try {
        let newUserSecretAnswer = await tbl_user_secret_question.create({
            user_id,
            secret_question_id,
            answer,
            state
        });

        if(newUserSecretAnswer){

            res.json({
                message: 'User secret answer created',
                date: newUserSecretAnswer
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function validateUserSecretQuestion(req, res){

    try{

        const validated = await tbl_user_secret_question.findOne({
            where: {
                tbl_user_id: req.user.id,
                answer: req.body.answer,
                state:1
            }
        })

        if( validated ){

            return res.json({
                message: 'User secret question validated'
            })
        }

        return res.status(400).json({
            message: 'Incorrect answer'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function updateUserSecretAnswer(req, res){

    const {
        answer,
    } = req.body;

    try {

        const userSecretAnswer = await tbl_user_secret_question.findByPk(req.params.id)

        if( userSecretAnswer ){

            await userSecretAnswer.update({
                answer,
            })

            return res.json({
                message: 'User secret answer updated',
                data: userSecretAnswer
            });
        }

        return res.status(404).json({
            message: 'User secret answer not found'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function deleteUserSecretAnswer(req, res) {
    try {
        const deleteRowCount = await tbl_user_secret_question.destroy({
            where: {
                id: req.params.id
            }
        });
        res.json({
            message: 'User secret question deleted successfully',
            count: deleteRowCount
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}