const {
    tbl_profile,
    tbl_role,
    tbl_institution,
    tbl_district,
    tbl_city,
    tbl_countrie,
    tbl_user,
    tbl_user_secret_question,
    tbl_profile_attribute,
    tbl_profile_specialty,
    tbl_specialty,
    tbl_profile_institution,
    tbl_session,
    tbl_session_attribute,
    tbl_avatar,
    tbl_benefit_profile,
    tbl_benefit,
    tbl_company_benefit,
    tbl_availability,
    tbl_chat,
    tbl_chat_participants
} = require('../models')
const { isValidBase64Img, s3UploadBase64Image } = require('./../helpers')
const { Op } = require('sequelize');
const { DateTime } = require('luxon');
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { session } from 'passport';

export async function getListProfile(req, res) {
    try {

        const profiles = await tbl_profile.findAll({where:{state:1}});

        res.json({
            message: 'Profiles found',
            data: profiles
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function getProfile(req, res) {
    try {

        const profile = await tbl_profile.findByPk(req.user.idProfile,
            {
                where: {
                    state: 1
                },
                include: [
                    {
                        model: tbl_role,
                        as: 'Role',
                        attributes: [
                            'id',
                            'name',
                            'alias'
                        ]
                    },
                    {
                        model: tbl_avatar,
                        as: 'Avatar',
                        attributes: ['image']
                    },
                    {
                        model: tbl_profile_institution,
                        as: 'ProfileInstitutions',
                        include: {
                            model: tbl_institution,
                            as: 'Institution',
                            attributes: ['name']
                        },
                        attributes: [
                            'academic_degree',
                            'start_date',
                            'ending_date',
                            'document',
                        ]
                    },
                    {
                        model: tbl_specialty,
                        as: 'Specialties',
                        through: {
                            where: {
                                state: 1
                            },
                            attributes: []
                        },
                        attributes: [
                            'id',
                            'name',
                            'alias',
                            'icon'
                        ]
                    },
                    {
                        model: tbl_district,
                        as: 'District',
                        include: {
                            model: tbl_city,
                            as: 'City',
                            include: {
                                model: tbl_countrie,
                                as: 'Country',
                                attributes: ['name']
                            },
                            attributes: ['name']
                        },
                        attributes: ['name']
                    },
                    {
                        model: tbl_availability,
                        as: 'Availabilities',
                        where: { state: 1 },
                        required: false,
                        attributes: [ 'id', 'day', 'start_hour', 'end_hour' ]
                    }
                ],
                attributes: [
                    'id',
                    'slug',
                    'firstname',
                    'lastname',
                    'dni',
                    'photo_dni',
                    'email',
                    'phone',
                    'gender',
                    'birthday',
                    'state',
                    'req_sent_at',
                    'req_state',
                    'req_approved_at'
                ]
            }
        );
        const profile_attributes=await tbl_profile_attribute.findAll({
                where: {
                    tbl_profile_id:req.user.idProfile
                },
                attributes: ['key','value']
            })
            const attributes = {}
        profile_attributes.map(item => {
            attributes[item.key] = item.value
        })
        profile.dataValues['Agenda']=attributes

        res.json({
            message: 'Profile found',
            data: profile
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function getPointsUsed(req, res){

    try{

        const benefits = await tbl_benefit_profile.findAll({
            where: {
                tbl_profile_id: req.user.idProfile,
            },
            include: {
                model: tbl_benefit,
                as: 'Benefit',
                attributes: [
                    'id',
                    'name',
                    'image',
                    'description',
                    'point'
                ],
                include: {
                    model: tbl_company_benefit,
                    as: 'CompanyBenefit',
                    attributes: [
                        'name',
                        'image'
                    ]
                }
            },
            attributes: [
                'id',
                'state'
            ]
        })

        res.json({
            message: 'Benefits found',
            data:benefits
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function createProfile(req, res) {
    try {
        const {
            password,
            firstname,
            email,
            program,
            local,
            phone,
            birthday,
            image
        } = req.body;
        const emailNew=email ? email : ''
        const phoneNew=phone ? phone : ''
        const profile=req.body.profile ? req.body.profile : ''
        const passwordEncrypt = await bcrypt.hash(password, parseInt(process.env.BCRYPT_ROUNDS));
        let urlImage=''
        if(image)
        {
        const photo = await savePhoto(req, res, image, 'uploads/profile-photo/')
        urlImage = photo.data.Location
        if (!photo.success)
            return res.status(500).json({
                message: 'Something went wrong uploading the image'
            })
        }
        else
        {
            urlImage = null
        }
           
           const emailExist = await tbl_user.findOne({
               where:
               {username:email}
           })
           if(!emailExist)
           {
            const newUser = await tbl_user.create({
                username: emailNew,
                password: passwordEncrypt,
            })
    
            let string=`${firstname} ${newUser.id}`
            let slug = string.toLowerCase().replace(/\s+/g,'-');

            let newProfile = await tbl_profile.create({
                slug,
                firstname,
                'email':emailNew,
                'phone':phoneNew,
                profile,
                birthday,
                image:urlImage,
                local,
                program,
                tbl_user_id:newUser.id
            });

            const dateNow= new Date();
            let chat = await tbl_chat.create({
                tbl_room_id:1,
                start: dateNow
            })

            await tbl_chat_participants.create({
                tbl_profile_id: newProfile.id,
                tbl_chat_id: chat.id
            })
            
            const token = await jwt.sign({ user: newProfile }, 'globoazul_token')
            res.json({
                message: 'profile created',
                data: {token}
            });
            
           }
           else
           {
            res.status(400).json({
                message: 'Email Exist!!',
            });
           }
            
            
            
            
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function getListPatients(req, res){

    try{

        const { city, search, institution, age, age_range_min, age_range_max, sessions } = req.query

        let whereQuery = {},
            whereCityQuery = {},
            whereInstQuery = {},
            instRequired = false

        if( city ){

            whereCityQuery = {
                id: city
            }
        }

        if( institution ){

            instRequired = true

            whereInstQuery = {
                id: institution
            }
        }

        const now = DateTime.now()

        const commonAttrib = {
            day: 1, month: now.month + 1
        }

        if( age ){

            whereQuery = {
                birthday: {
                    [Op.gte]: DateTime.fromObject({
                        ...commonAttrib,
                        year: now.year - age - 1
                    }).toISO(),
                    [Op.lt]: DateTime.fromObject({
                        ...commonAttrib,
                        year: now.year - age
                    }).toISO()
                }
            }

            // Fecha minima
            // dia: 1
            // month: current month + 1
            // year: current year - age - 1

            // Fecha maxima
            // dia: last current month
            // month: current month
            // year: current year - age

        } else{

            if( age_range_min &&
                age_range_max ){

                whereQuery = {
                    birthday: {
                        [Op.gte]: DateTime.fromObject({
                            ...commonAttrib,
                            year: now.year - age_range_max - 1
                        }).toISO(),
                        [Op.lt]: DateTime.fromObject({
                            ...commonAttrib,
                            year: now.year - age_range_min
                        }).toISO()
                    }
                }
            }
        }

        if (search) {
            whereQuery[Op.or] = [
                { firstname: { [Op.like]: `%${search}%` } },
                { lastname: { [Op.like]: `%${search}%` } }
            ]
        }
        console.log('whereQuery', whereQuery)

        let patient = await tbl_profile.findAll({
            where:
            {
                ...whereQuery,
                tbl_role_id:1
            },
            include:
            [
                {
                  model:tbl_district,
                  as:'District',
                  attributes: ['name'],
                  required: false,
                  include:
                  {
                      model:tbl_city,
                      as:'City',
                      where: whereCityQuery,
                      attributes: ['id', 'name']
                  }
                },
                {
                    model:tbl_profile_institution,
                    as:'ProfileInstitutions',
                    attributes: ['tbl_institution_id'],
                    required: instRequired,
                    include:
                    {
                        model:tbl_institution,
                        as:'Institution',
                        where: whereInstQuery,
                        attributes:['name']
                    }
                }
            ]
        })

        let data = []

        await Promise.all(
            patient.map(item => {
                return new Promise(async resolve => {

                    item.dataValues.sessions = await tbl_session.count({
                        where:{
                            tbl_profile_pacient_id: item.id,
                            state: 1
                        }
                    })
                    item.dataValues.sessionsCompleted = await tbl_session.count({
                        where:{
                            tbl_profile_pacient_id: item.id,
                            completed: true,
                            state: 1
                        }
                    })
                    item.dataValues.sessionsTruncate = await tbl_session.count({
                        where:{
                            tbl_profile_pacient_id: item.id,
                            state: 0
                        }
                    })

                    item.dataValues.program = await tbl_session.findOne({
                        where:{
                            tbl_profile_pacient_id:item.id
                        },
                        order:[['id', 'DESC']],
                        attributes:
                        [
                            'id',
                            'completed'
                        ]
                    })
                    let fechaActual= new Date()
                    const year=fechaActual.getFullYear()
                    let birthdayPatient = item.birthday
                    const newBirthdayPatient = new Date(birthdayPatient)
                    const yearPatient=newBirthdayPatient.getFullYear()
                    const age= year - yearPatient
                    item.setDataValue('age',age)
                    /*item.setDataValue('notificationState','Sesión cancelada')
                    item.setDataValue('psychologist',psychologist)*/
                    data.push(item)
                    resolve(1)
                })
            })
        )
        if( sessions ) {
            data = data.filter(profile => profile.dataValues.sessions == sessions)
        }

        res.json({
            message: 'Patients found',
            data
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function getListPsychologist(req, res) {
    try{
        const { city, institution, age, age_range_min, age_range_max, patients, date } = req.query

        const now = DateTime.now()

        let whereCityQuery = {}, whereInstQuery = {}, whereQuery = {}
        if (city)
            whereCityQuery.id = city
        if (institution)
            whereInstQuery.id = institution

        const commonAttrib = { day: 1, month: now.month + 1 }
        if (age) {
            whereQuery.birthday = {
                [Op.gte]: DateTime.fromObject({
                    ...commonAttrib,
                    year: now.year - age - 1
                }).toISO(),
                [Op.lt]: DateTime.fromObject({
                    ...commonAttrib,
                    year: now.year - age
                }).toISO()
            }

            // Fecha minima
            // dia: 1
            // month: current month + 1
            // year: current year - age - 1

            // Fecha maxima
            // dia: last current month
            // month: current month
            // year: current year - age

        } else {
            if (age_range_min && age_range_max)
                whereQuery.birthday = {
                    [Op.gte]: DateTime.fromObject({
                        ...commonAttrib,
                        year: now.year - age_range_max - 1
                    }).toISO(),
                    [Op.lt]: DateTime.fromObject({
                        ...commonAttrib,
                        year: now.year - age_range_min
                    }).toISO()
                }
        }
        let patient = await tbl_profile.findAll({
            where: {
                ...whereQuery,
                tbl_role_id: 2
            },
            include: [
                {
                    model:tbl_district,
                    as:'District',
                    attributes: ['name'],
                    required: false,
                    include: {
                        model:tbl_city,
                        as: 'City',
                        where: whereCityQuery,
                        attributes: ['id', 'name']
                    }
                },
                {
                    model:tbl_profile_institution,
                    as:'ProfileInstitutions',
                    attributes: ['tbl_institution_id'],
                    required: false,
                    include:
                    {
                        model:tbl_institution,
                        as:'Institution',
                        where: whereInstQuery,
                        attributes: ['name']
                    }
                },
                {
                    model: tbl_availability,
                    as: 'Availabilities',
                    where: { state: 1 },
                    required: false,
                    attributes: [ 'id', 'day', 'start_hour', 'end_hour' ]
                }
            ]
        })
        let data = []
        patient = patient.filter(profile => {
            let status = false
            if (date) {
                let dateHour = date.split(' ')
                const newDate = new Date(dateHour[0])
                profile.Availabilities.forEach(el => {
                    if (el.dataValues.day == (newDate.getDay() + 1) && el.dataValues.start_hour == `${dateHour[1]}:00`) {
                        status = true
                    }
                })
            }
            return status
        })

        await Promise.all(
            patient.map(item => {
                return new Promise(async resolve => {
                    item.dataValues.patients = await tbl_session.count({
                        where: {
                            tbl_profile_psychologist_id: item.id
                        }
                    })
                    item.dataValues.patientsActives = await tbl_session.count({
                        where: {
                            completed: false,
                            tbl_profile_psychologist_id: item.id
                        }
                    })

                    // item.setDataValue('notificationState','Sesión cancelada')
                    // item.setDataValue('psychologist',psychologist)
                    data.push(item)
                    resolve(1)
                })

            })
        )
        if (patients)
            data = data.filter(profile => profile.dataValues.patients == patients)

        res.json({
            message: 'Psychologist found',
            count: data.length,
            data: data
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function indexPatients(req, res){

    try{

        let to = req.query.to

        console.log('')
        console.log('to')
        console.log(to)
        console.log('')

        let whereQuery

        // Today, 00:00:00
        const now = DateTime.fromObject({day: DateTime.now().day})

        switch (to) {
            case 'today':

                whereQuery = {
                    date: {
                        [Op.gte]: now.toISO(),
                        [Op.lt]: now.plus({days: 1}).toISO()
                    }
                }

                break;

            case 'week':

                const weekday = now.weekday

                whereQuery = {
                    date: {
                        // Day month - ( Day week - 1 )
                        [Op.gte]: now.plus({days: (1 - weekday)}).toISO(),
                        // Day month + ( 7 - Day week ) + 1
                        [Op.lt]: now.plus({days: (8 - weekday)}).toISO()
                    }
                }

                break;

            case 'month':

                whereQuery = {
                    date: {
                        [Op.gte]: DateTime.fromObject({day: 1}).toISO(),
                        [Op.lt]: DateTime.fromObject({day: 1, month: (now.month + 1)}).toISO()
                    }
                }

                break;

            case 'year':

                whereQuery = {
                    date: {
                        [Op.gte]: DateTime.fromObject({day: 1, month: 1}).toISO(),
                        [Op.lt]: DateTime.fromObject({day: 1, month: 1, year: (now.year + 1)}).toISO()
                    }
                }

                break;

            default:

                whereQuery = {}

                break;
        }

        const today = new Date()

        const patients = await tbl_session.findAll({
            where: {
                ...whereQuery,
                tbl_profile_psychologist_id: req.user.idProfile,
                order: 1
            },
            group: 'tbl_profile_pacient_id',
            attributes: [
                ['tbl_profile_pacient_id', 'id']
            ]
        })

        let data = []

        await Promise.all(
            patients.map(({id}) => {

                return new Promise(async resolve => {

                    const p_profile = tbl_profile.findByPk(id, {
                        attributes: [
                            'firstname',
                            'lastname'
                        ],
                        include: {
                            model: tbl_avatar,
                            as: 'Avatar',
                            attributes: ['image']
                        }
                    })

                    const p_sessions = tbl_session.findAll({
                        where: {
                            tbl_profile_psychologist_id: req.user.idProfile,
                            tbl_profile_pacient_id: id,
                            state:1
                        },
                        include: {
                            model: tbl_session_attribute,
                            as: 'SessionAttributes',
                            attributes: [
                                'key',
                                'value'
                            ]
                        },
                        attributes: [
                            'id',
                            'date',
                            'order'
                        ],
                        order: [
                            [
                                'order',
                                'DESC'
                            ]
                        ]
                    })

                    const [patient, sessions] = await Promise.all([p_profile, p_sessions])

                    let current_session = sessions[0].order

                    if( sessions.length > 1 ){

                        const last_session = sessions[0]

                        if( today < last_session.date ){

                            // Current Date Less Than Session Date
                            // cdltsd
                            let cdltsd = true,
                                index_last_session = 1,
                                w_i = 0

                            while( cdltsd ){

                                if( w_i < sessions.length ){

                                    if( ! (today < sessions[w_i].date) ){

                                        index_last_session = w_i

                                        cdltsd = false
                                    }

                                    w_i += 1

                                } else{

                                    // console.log('')
                                    // console.log('Salvado por el length')
                                    // console.log('')
                                    cdltsd = false
                                }
                            }

                            current_session = sessions[index_last_session].order
                        }
                    }

                    data.push({
                        id,
                        firstname: patient.firstname,
                        lastname: patient.lastname,
                        image: patient.Avatar.image,
                        current_session,
                        total_sessions: sessions.length,
                        sessions
                    })

                    resolve(1)
                })
            })
        )

        res.json({
            message: 'Patients found',
            patients: data
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function createPatient(req, res) {

    const {

        username,
        password,
        firstname,
        lastname,
        email,
        phone,
        avatar_id,
        secret_question_id,
        secret_question_answer
    } = req.body;
    const Avatar = await tbl_avatar.findOne({
        attributes:['id'],
        order:[['id','DESC']]
     })
    const idAvatar= Avatar.id
    const avatarNew= avatar_id ? avatar_id : idAvatar
    const lastnameNew=lastname ? lastname : ''
    const emailNew=email ? email : ''
    const dni=username ? username : ''
    const phoneNew=phone ? phone : ''
    const gender='F'
    const birthday="1994-11-12"

    const passwordEncrypt = await bcrypt.hash(password, parseInt(process.env.BCRYPT_ROUNDS));
    try {
        const newUser = await tbl_user.create({
            username,
            password: passwordEncrypt,
        })
        const usuarioID = newUser.id
        const userName = newUser.username

        let string=`${firstname} ${lastnameNew} ${newUser.id}`
        let slug = string.toLowerCase().replace(/\s+/g,'-');
        let newProfile = await tbl_profile.create({
            slug,
            firstname,
            'lastname':lastnameNew,
            'dni':dni,
            'email':emailNew,
            'phone':phoneNew,
            gender,
            birthday,
            tbl_user_id: newUser.id,
            tbl_role_id: 1,
            tbl_avatar_id: avatarNew
        });

        const newAnswer = await tbl_user_secret_question.create({
            tbl_user_id: newUser.id,
            tbl_secret_question_id: secret_question_id,
            answer: secret_question_answer
        })

        let token =await createToken(req,res,usuarioID,userName)
        if (newProfile) {
            res.json({
                message: 'profile created',
                data: {token}
            });
        }

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function updateProfile(req, res) {

    const {

        username,
        firstname,
        lastname,
        civilState,
        ocupation,
        studyNivel

    } = req.body;
    const id = req.user.idProfile
    const lastnameNew=lastname ? lastname : ''
    const gender='F'
    const birthday="1994-11-12"
    /*
    nombre, apellidos, estado civil, ocupacion, nivel de estudios
    */


    try {
        const usuarioID = req.user.id
        const userName = req.user.username

        let string=`${firstname} ${lastnameNew} ${usuarioID}`
        let slug = string.toLowerCase().replace(/\s+/g,'-');
        let newProfile = await tbl_profile.update({
            slug,
            firstname,
            'lastname':lastnameNew,
            gender,
            birthday,
            'civil_state':civilState,
            ocupation,
            'study_nivel': studyNivel,
            tbl_user_id: usuarioID,
            tbl_role_id: req.user.idRole

        },
    {
        where:
        {
            id:id
        }
    });



        let token =await createToken(req,res,usuarioID,userName)
        if (newProfile) {
            res.json({
                message: 'profile update',
                data: {token}
            });
        }

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}


export async function updateProfileOnly(req, res){

    try{

        const idProfile = req.user.idProfile

        if( idProfile != req.params.id ){

            return res.status(401).json({
                message: 'Access denied with provided credentials'
            })
        }

        /*

        {
            firstname,
            lastname,
            dni,
            phone,
            gender,
            birthday
        }

         */

        const profile = await tbl_profile.findByPk(idProfile)

        await profile.update(req.body,
            {
                fields: [
                    'firstname',
                    'lastname',
                    'dni',
                    'phone',
                    'gender',
                    'birthday'
                    // 'tbl_district_id'
                ]
            }
        )

        res.json({
            message: 'Profile updated'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function updateProfilePhoto(req, res){

    try{

        const idProfile = req.user.idProfile
        const usuarioID= req.user.id
        const userName= req.user.username

        const { image } = req.body

        if( idProfile != req.params.id ){

            return res.status(401).json({
                message: 'Access denied with provided credentials'
            })
        }

        const photo = await savePhoto(req, res, image, 'uploads/profile-photo/')

        if(!photo.success){

            return res.status(500).json({
                message: 'Something went wrong uploading the image'
            })
        }

        const avatar = await tbl_avatar.findOne({
            include: {
                model: tbl_profile,
                as: 'Profile',
                where: {
                    id: idProfile
                },
                attributes: []
            }
        })

        await tbl_avatar.create({
            image: photo.data.Location,
            default : 0
        })
        const Avatar = await tbl_avatar.findOne({
            attributes:['id'],
            order:[['id','DESC']]
         })
         console.log(Avatar.id)
         console.log(idProfile)
        await tbl_profile.update({
            tbl_avatar_id:Avatar.id},
            {where:{id:idProfile}}
        )
        
        let token =await createToken(req,res,usuarioID,userName)

        res.json({
            message: 'Profile photo updated',
            data:{token}
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function updatePhoto(req, res){
    try{
        const { idProfile } = req.user
        const { image } = req.body

        const photo = await savePhoto(req, res, image, 'uploads/profile-photo/')

        if(!photo.success){
            return res.status(500).json({
                message: 'Something went wrong uploading the image'
            })
        }
        await tbl_avatar.create({
            image: photo.data.Location,
            default : 0
        })
        const avatar = await tbl_avatar.findOne({
            attributes: [ 'id' ],
            order: [ [ 'id', 'DESC' ] ]
        })
        await tbl_profile.update(
            { tbl_avatar_id: avatar.id },
            { where: { id: idProfile } }
        )
        res.json({
            state: true,
            message: 'Profile photo updated'
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}
export async function updateProfileAvatar(req, res){

    try{

        const idProfile = req.user.idProfile
        const usuarioID= req.user.id
        const userName= req.user.username

        if( idProfile != req.params.id ){

            return res.status(401).json({
                message: 'Access denied with provided credentials'
            })
        }

        if( req.body.idAvatar ){

            await tbl_profile.update({
                tbl_avatar_id: req.body.idAvatar
            }, {
                where: {
                    id: idProfile
                }
            })
            let token =await createToken(req,res,usuarioID,userName)

            return res.json({
                message: 'Profile phone updated',
                data:{token}
            })
        }

        res.status(400).json({
            message: 'Empty data'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function updateProfilePhone(req, res){

    try{

        const idProfile = req.user.idProfile
        const usuarioID= req.user.id
        const userName= req.user.username

        if( idProfile != req.params.id ){

            return res.status(401).json({
                message: 'Access denied with provided credentials'
            })
        }

        if( req.body.phone ){

            await tbl_profile.update({
                phone: req.body.phone
            }, {
                where: {
                    id: idProfile
                }
            })
            let token =await createToken(req,res,usuarioID,userName)

            return res.json({
                message: 'Profile phone updated',
                data:{token}
            })
        }

        res.status(400).json({
            message: 'Empty data'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function deleteProfile(req, res) {
    try {
        const {id} = req.params;
        const deleteRowCount = await tbl_profile.destroy({
            where: {
                id
            }
        });
        res.json({
            message: 'Profile deleted successfully',
            count: deleteRowCount
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function getPsycology(req, res) {
    try {

        const profile = await tbl_profile.findOne({
            where: {
                tbl_user_id: req.user.id
            },
            attributes: [
                'id',
                'firstname',
                'lastname',
                'dni',
                'gender',
                'birthday',
                'email'
            ],
            include: {
                model: tbl_profile_attribute,
                attributes: [['value', 'availability']],
                where: { state: 1, key: 'available' }
            }
        })
        const profilespe = await tbl_specialty.findAll( {
            attributes: [
                //'id',
                'name',
                'alias'
                //'duration',
                //'name',
                //'type'
            ],
            include: {
                model: tbl_profile_specialty,
                where:{
                    tbl_profile_id: profile.id,
                    state:1
                },
                attributes: []
            }
        })
        const profileatri = await tbl_profile_attribute.findAll({
            where:{
                tbl_profile_id: profile.id
            },
            attributes: [
                'key', 'value'
            ]
        }
        )
        /**   include: {
            model: tbl_profile_attribute,
            attributes: ['id']
        }*/

        if( ! profile ){

            return res.status(404).json({
                message: 'Training not found'
            })
        }

        const attributes = {}
        profileatri.map(item => {
            attributes[item.key] = item.value
        })

        let data = {
            psycology: profile,
            specialties: profilespe,
            attributes
        }
        //item.setDataValue('total', attributesx)
        /*.then(() => { res.json({
            // data,
            attributesx
        })*/

        res.json({
            data
        });

    } catch (error) {
        console.log(error);
    }
}

export async function updatePsycology(req, res){
    try {
        const { idProfile } = req.user
        const {
            firstname,
            lastname,
            dni,
            imageDni,
            birthday,
            gender,
            email,
            phone,
            district_id,
            attributes,
            specialties,
            institutions,
            availabilities
        } = req.body;
        // Updated profile
        await tbl_profile.update(
            {
                firstname, lastname, dni, birthday, gender, email, phone, district_id
            },
            {
                where: { id: idProfile }
            }
        )
        await tbl_availability.update(
            { state: 0 },
            { where: { tbl_profile_id: idProfile } }
        )
        if (typeof availabilities == 'object') {
            await Promise.all(
                availabilities.map(el => {
                    el.hours.map(hour => {
                        return new Promise(async resolve => {
                            const [ availability, created ] = await tbl_availability.findOrCreate({
                                where: {
                                    day: String(el.day),
                                    start_hour: hour.startHour
                                },
                                defaults: {
                                    tbl_profile_id: idProfile,
                                    day: String(el.day),
                                    start_hour: hour.startHour,
                                    end_hour: hour.endHour,
                                    state: 1
                                }
                            })
                            if (!created) {
                                await availability.update({
                                    state: 1
                                })
                            }
                            resolve(1)
                        })
                    })
                })
            )
        }
        await tbl_profile_specialty.destroy({
            where: {
                tbl_profile_id: idProfile
            }
        })
        if (typeof specialties == 'object') {
            await Promise.all(
                specialties.map(el => {
                    return new Promise(async resolve => {
                        if (el.value) {
                            await tbl_profile_specialty.create({
                                tbl_profile_id: idProfile,
                                tbl_specialty_id: el.id,
                                state: 1
                            })
                        }
                        resolve(1)
                    })
                })
            )
        }
        if (imageDni) {
            const saveImageDni = await savePhoto(req, res, imageDni, 'uploads/profile-photo/')
            if (!saveImageDni.success)
                return res.status(500).json({
                    message: 'Something went wrong uploading the image'
                })
            attributes.push({
                key: 'imageDni',
                value: saveImageDni.data.Location
            })
        }
        if(typeof attributes == 'object'){
            await Promise.all(
                attributes.map(attribute => {
                    return new Promise(async resolve => {
                        const value = (typeof attribute.value === 'object') ? JSON.stringify(attribute.value) : attribute.value
                        const [profile_attribute, pa_created] = await tbl_profile_attribute.findOrCreate({
                            where: {
                                tbl_profile_id: idProfile,
                                key: attribute.key,
                            },
                            defaults: {
                                value,
                                state: 1
                            }
                        })
                        if( ! pa_created ){
                            await profile_attribute.update({
                                value
                            })
                        }
                        resolve(1)
                    })
                })
            )
        }
        if (typeof institutions == 'object') {
            await Promise.all(
                institutions.map(institution => {
                    return new Promise(async resolve => {
                        //*por corregir*//
                        institution.id = 1

                        const [profile_institution, pi_created] = await tbl_profile_institution.findOrCreate({
                            where: {
                                tbl_profile_id: idProfile,
                                tbl_institution_id: institution.id
                            },
                            defaults: {
                                state: 1,
                                academic_degree: institution.academic_degree,
                                start_date: institution.start_date,
                                ending_date: institution.ending_date,
                                document: institution.document
                            }
                        })
                        if( ! pi_created ){
                            await profile_institution.update({
                                academic_degree: institution.academic_degree,
                                start_date: institution.start_date,
                                ending_date: institution.ending_date,
                                document: institution.document
                            })
                        }
                        resolve(1)
                    })
                })
            )
        }
        return res.json({
            message: 'Profile updated',
            data: req.body
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}
async function savePhoto(req, res, image, path) {
    try {
        const { isValid, buffer, ext } = isValidBase64Img(image)
        if(!isValid) {
            return res.status(422).json({
                message: 'The given data was invalid.',
                errors: {
                    image: [
                        "The image must be a valid base64"
                    ]
                }
            })
        }
        const imageUploaded = await s3UploadBase64Image(path, { buffer, ext })
        return imageUploaded
    } catch (error) {
        console.log('Something went error:', error)
        return null
    }
}