const { tbl_availability, tbl_profile } =require('../models')
const { DateTime } = require('luxon');

const pushAvilabilityTime = (day_array, availability) => {

    day_array.push(DateTime.fromFormat(availability.start_hour, 'HH:mm:ss').toFormat('HH:mm'))
}

const getEndHour = start_hour => DateTime.fromFormat(start_hour, 'HH:mm').plus({hours: 1}).toFormat('HH:mm:ss')

export async function listAvailability(req, res){
    try {

        const data = await tbl_availability.findAll({
            where: {
                tbl_profile_id: req.user.idProfile
            }
        })

        let lunes_availability = [],
            martes_availability = [],
            miercoles_availability = [],
            jueves_availability = [],
            viernes_availability = [],
            sabado_availability = [],
            domingo_availability = []

        data.filter(availability => {

            // Lunes

            switch (availability.day) {
                case 'Lunes':

                    pushAvilabilityTime(lunes_availability, availability)

                    break;

                case 'Martes':

                    pushAvilabilityTime(martes_availability, availability)

                    break;

                case 'Miercoles':

                    pushAvilabilityTime(miercoles_availability, availability)

                    break;

                case 'Jueves':

                    pushAvilabilityTime(jueves_availability, availability)

                    break;

                case 'Viernes':

                    pushAvilabilityTime(viernes_availability, availability)

                    break;

                case 'Sabado':

                    pushAvilabilityTime(sabado_availability, availability)

                    break;

                case 'Domingo':

                    pushAvilabilityTime(domingo_availability, availability)

                    break;
            }
        })

        res.json({
            data: {
                Lunes: lunes_availability,
                Martes: martes_availability,
                Miercoles: miercoles_availability,
                Jueves: jueves_availability,
                Viernes: viernes_availability,
                Sabado: sabado_availability,
                Domingo: domingo_availability
            }
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function listPsycologistAvailability(req, res){
    try {

        const data = await tbl_profile.findAll({
            where: {
                tbl_role_id: 2
            },
            include:
            {
                model:tbl_availability,
                as:'Availability'
            }
        })

        

        res.json({
            data: {data}
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}


export async function storeAvailability(req, res){

    try{

        const availability = req.body.availability

        const idProfile = req.user.idProfile

        const data = await tbl_availability.findAll({
            where: {
                tbl_profile_id: idProfile
            }
        })

        let lunes_availability = [],
            martes_availability = [],
            miercoles_availability = [],
            jueves_availability = [],
            viernes_availability = [],
            sabado_availability = [],
            domingo_availability = []

        data.filter(availability => {

            // Lunes

            switch (availability.day) {
                case 'Lunes':

                    pushAvilabilityTime(lunes_availability, availability)

                    break;

                case 'Martes':

                    pushAvilabilityTime(martes_availability, availability)

                    break;

                case 'Miercoles':

                    pushAvilabilityTime(miercoles_availability, availability)

                    break;

                case 'Jueves':

                    pushAvilabilityTime(jueves_availability, availability)

                    break;

                case 'Viernes':

                    pushAvilabilityTime(viernes_availability, availability)

                    break;

                case 'Sabado':

                    pushAvilabilityTime(sabado_availability, availability)

                    break;

                case 'Domingo':

                    pushAvilabilityTime(domingo_availability, availability)

                    break;
            }
        })


        if( availability ){

            // Lunes

            //      Delete
            const availability_delete_lu = lunes_availability.filter(e => ! availability.Lunes.includes(e))

            //      Add
            const availability_add_lu = availability.Lunes.filter(e => ! lunes_availability.includes(e))

            const p_availability_delete_lu = availability_delete_lu.map(start_hour => {

                return tbl_availability.destroy({
                    where: {
                        tbl_profile_id: idProfile,
                        day: 'Lunes',
                        start_hour
                    }
                })
            })

            const p_availability_add_lu = availability_add_lu.map(start_hour => {

                return tbl_availability.create({
                    tbl_profile_id: idProfile,
                    day: 'Lunes',
                    start_hour,
                    end_hour: getEndHour(start_hour)
                })
            })

            // Martes

            //      Delete
            const availability_delete_ma = martes_availability.filter(e => ! availability.Martes.includes(e))

            //      Add
            const availability_add_ma = availability.Martes.filter(e => ! martes_availability.includes(e))

            const p_availability_delete_ma = availability_delete_ma.map(start_hour => {

                return tbl_availability.destroy({
                    where: {
                        tbl_profile_id: idProfile,
                        day: 'Martes',
                        start_hour
                    }
                })
            })

            const p_availability_add_ma = availability_add_ma.map(start_hour => {

                return tbl_availability.create({
                    tbl_profile_id: idProfile,
                    day: 'Martes',
                    start_hour,
                    end_hour: getEndHour(start_hour)
                })
            })

            // Miercoles

            //      Delete
            const availability_delete_mi = miercoles_availability.filter(e => ! availability.Miercoles.includes(e))

            //      Add
            const availability_add_mi = availability.Miercoles.filter(e => ! miercoles_availability.includes(e))

            const p_availability_delete_mi = availability_delete_mi.map(start_hour => {

                return tbl_availability.destroy({
                    where: {
                        tbl_profile_id: idProfile,
                        day: 'Miercoles',
                        start_hour
                    }
                })
            })

            const p_availability_add_mi = availability_add_mi.map(start_hour => {

                return tbl_availability.create({
                    tbl_profile_id: idProfile,
                    day: 'Miercoles',
                    start_hour,
                    end_hour: getEndHour(start_hour)
                })
            })

            // Jueves

            //      Delete
            const availability_delete_ju = jueves_availability.filter(e => ! availability.Jueves.includes(e))

            //      Add
            const availability_add_ju = availability.Jueves.filter(e => ! jueves_availability.includes(e))

            const p_availability_delete_ju = availability_delete_ju.map(start_hour => {

                return tbl_availability.destroy({
                    where: {
                        tbl_profile_id: idProfile,
                        day: 'Jueves',
                        start_hour
                    }
                })
            })

            const p_availability_add_ju = availability_add_ju.map(start_hour => {

                return tbl_availability.create({
                    tbl_profile_id: idProfile,
                    day: 'Jueves',
                    start_hour,
                    end_hour: getEndHour(start_hour)
                })
            })

            // Viernes

            //      Delete
            const availability_delete_vi = viernes_availability.filter(e => ! availability.Viernes.includes(e))

            //      Add
            const availability_add_vi = availability.Viernes.filter(e => ! viernes_availability.includes(e))

            const p_availability_delete_vi = availability_delete_vi.map(start_hour => {

                return tbl_availability.destroy({
                    where: {
                        tbl_profile_id: idProfile,
                        day: 'Viernes',
                        start_hour
                    }
                })
            })

            const p_availability_add_vi = availability_add_vi.map(start_hour => {

                return tbl_availability.create({
                    tbl_profile_id: idProfile,
                    day: 'Viernes',
                    start_hour,
                    end_hour: getEndHour(start_hour)
                })
            })

            // Sabado

            //      Delete
            const availability_delete_sa = sabado_availability.filter(e => ! availability.Sabado.includes(e))

            //      Add
            const availability_add_sa = availability.Sabado.filter(e => ! sabado_availability.includes(e))

            const p_availability_delete_sa = availability_delete_sa.map(start_hour => {

                return tbl_availability.destroy({
                    where: {
                        tbl_profile_id: idProfile,
                        day: 'Sabado',
                        start_hour
                    }
                })
            })

            const p_availability_add_sa = availability_add_sa.map(start_hour => {

                return tbl_availability.create({
                    tbl_profile_id: idProfile,
                    day: 'Sabado',
                    start_hour,
                    end_hour: getEndHour(start_hour)
                })
            })

            // Domingo

            //      Delete
            const availability_delete_do = domingo_availability.filter(e => ! availability.Domingo.includes(e))

            //      Add
            const availability_add_do = availability.Domingo.filter(e => ! domingo_availability.includes(e))

            const p_availability_delete_do = availability_delete_do.map(start_hour => {

                return tbl_availability.destroy({
                    where: {
                        tbl_profile_id: idProfile,
                        day: 'Domingo',
                        start_hour
                    }
                })
            })

            const p_availability_add_do = availability_add_do.map(start_hour => {

                return tbl_availability.create({
                    tbl_profile_id: idProfile,
                    day: 'Domingo',
                    start_hour,
                    end_hour: getEndHour(start_hour)
                })
            })

            await Promise.all([
                p_availability_delete_lu,
                p_availability_add_lu,
                p_availability_delete_ma,
                p_availability_add_ma,
                p_availability_delete_mi,
                p_availability_add_mi,
                p_availability_delete_ju,
                p_availability_add_ju,
                p_availability_delete_vi,
                p_availability_add_vi,
                p_availability_delete_sa,
                p_availability_add_sa,
                p_availability_delete_do,
                p_availability_add_do
            ])

            return res.json({
                message: 'Availability stored',
            });
        }

        res.status(400).json({
            message: 'Empty data'
        })

    }catch(error){
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}

// export async function getAvatar(req,res){
//     try {
//         const {idProfile} = req.params;
//         const profiles = await tbl_profile.findByPk({
//             include: {
//                 model: tbl_avatar,
//                 attributes: [
//                     'id',
//                     'image'
//                 ]
//             },
//             where: {
//                 id: idProfile,
//                 state:1
//             }
//         });

//         res.json({
//             data: profiles
//         });

//     } catch (error) {
//         console.log(error);
//     }
// }