const {tbl_poll} = require('../models')



export async function createPoll(req, res){
    const {session_id,experience,qualification,comment }=req.body;

    try{
        let newPoll = await tbl_poll.create({
            tbl_session_id:session_id,
            experience:experience,
            qualification:qualification,
            comment:comment,
            created_by:'root',
            edited_by:'root'
        });
        
        if(newPoll){
            let poll ={
                id:newPoll.id,
                idSession:newPoll.tbl_session_id,
                experience:newPoll.experience,
                qualification:newPoll.qualification,
                comment:newPoll.comment
            }


            res.json({
                message: 'Poll created',
                data: poll
            });
        }


    }catch(error){
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}