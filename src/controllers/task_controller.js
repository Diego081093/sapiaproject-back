'use strict'

const {
    tbl_task,
    tbl_task_types,
    tbl_task_question,
    tbl_diary,
    tbl_task_media,
    tbl_session_task,
    tbl_session,
    tbl_profile,
    tbl_medias,
    tbl_media_profile,
    tbl_room,
    tbl_chat,
    tbl_chat_participants
} = require('../models')

const { DateTime } = require('luxon')
const { Op } = require('sequelize')

export async function getListTask(req, res){
    try {
        const tasks = await tbl_task.findAll({
            where: {
                state: 1
            },
            attributes: [
                'id',
                'name',
                'description'
            ],
            include: [
                {
                    model: tbl_task_types,
                    as: 'TaskTypes',
                    attributes: [
                        'id',
                        'name',
                        'description'
                    ]
                }
            ]
        })

        res.json({
            data: tasks
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getListTaskByWeek(req, res){

    try{

        const week = req.params.week

        const tasks = await tbl_task.findAll({
            where: {
                state: 1,
                week
            },
            attributes: [
                'id',
                'name',
                'description'
            ],
            include: [
                {
                    model: tbl_task_types,
                    as: 'TaskTypes',
                    attributes: [
                        'id',
                        'name',
                        'description'
                    ]
                }
            ]
        })

        res.json({
            message: 'Tasks found',
            data: tasks
        })

    }catch(error){
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getTasksPatient(req, res) {
    try {
        const { idPatient, week, types } = req.query
        const tasks = await tbl_task.findAll({
            where: { state: 1, week, tbl_task_types_id: { [Op.in]: types } },
            attributes: [
                'id',
                'name',
                'description'
            ],
            include: [
                {
                    model: tbl_session_task,
                    as: 'SessionTask',
                    where: { state: 1 },
                    required: false,
                    include: [
                        {
                            model: tbl_session,
                            as: 'Session',
                            attributes: [ 'id' ],
                            where: { state: 1, tbl_profile_pacient_id: idPatient }
                        }
                    ]
                },
                {
                    model: tbl_task_types,
                    as: 'TaskTypes',
                    attributes: [
                        'id',
                        'name',
                        'description'
                    ]
                },
                {
                    model: tbl_task_media,
                    as: 'TaskMedia',
                    attributes: [ 'id' ],
                    where: { state: 1 },
                    include: [
                        {
                            model: tbl_medias,
                            as: 'Media',
                            attributes: [ 'name', 'image','file', 'duration', 'points' ],
                            where: { state: 1 }
                        }
                    ]
                }
            ]
        })

        res.json(tasks)

    }catch(error){
        console.log('You have a error on getTasksPatient:', error)
        res.status(500).json({
            message: 'Something went wrong'
        })
    }
}

export async function createSessionTask(req, res) {
    try {
        const { idSession, idTask, state } = req.body
        const sessionTask = await tbl_session_task.findOne({
            where: { tbl_session_id: idSession, tbl_task_id: idTask }
        })
        console.log(sessionTask)
        let sessionTaskUpdate
        if (sessionTask) {
            console.log(sessionTask.dataValues.id)
            sessionTaskUpdate = await tbl_session_task.update(
                { state },
                { where: { id: sessionTask.dataValues.id } }
            )
        } else {
            sessionTaskUpdate = await tbl_session_task.create({
                tbl_session_id: idSession,
                tbl_task_id: idTask,
                state: 1
            })
        }
        res.json({
            state: true,
            message: 'Save sessionTask',
            data: sessionTaskUpdate
        })
    } catch (error) {
        console.log('You have a error on createSessionTask:', error)
        res.status(500).json({
            message: 'Something a went error'
        })
    }
}

export async function createAvatar(req, res){
    const {image}=req.body;

    try{
        let newAvatar = await tbl_avatar.create({
            image
        })

        if(newAvatar){

            res.json({
                message: 'avatar created',
                date: newAvatar
            });
        }

    }catch(error){
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getAvatar(req,res){
    try {
        const {idProfile} = req.params;
        const profiles = await tbl_profile.findByPk({
            include: {
                model: tbl_avatar,
                attributes: [
                    'id',
                    'image'
                ]
            },
            where: {
                id: idProfile,
                state:1
            }
        })

        res.json({
            data: profiles
        })

    } catch (error) {
        console.log(error);
    }
}

export async function getTask(req, res) {
    try {
        const { idProfile } = req.user
        const { id } = req.params
        const taskID = await tbl_session_task.findOne({where:{tbl_task_id:id}})
        const IDsession=taskID.id

        const task = await tbl_task.findOne({
            where: { state: 1, id },
            attributes: ['tbl_method_id'],
            include:[ {
                model: tbl_task_types,
                as: 'TaskTypes',
                where: { state: 1 },
                attributes: [ 'id', 'name', 'alias' ]
            },
            {
                model: tbl_session_task,
                as: 'SessionTask',
                where: { state: 1,tbl_task_id:id,id:IDsession},
                required: false
            },
            {
                model: tbl_task_question,
                as: 'TaskQuestion',
                where: { state: 1,tbl_task_id:id },
                required: false
            }]
        })

        const taskDiary= await tbl_diary.findAll({
            where:{
                tbl_task_id:id
            }
        })

        task.dataValues['Diary'] = taskDiary
        

        const taskMedia= await tbl_task_media.findAll({
            where:{
                tbl_task_id:id
            }
        })

        
        const date = new Date();
        const taskRoom= await tbl_room.findAll({
            where:{
                tbl_method_id: task.tbl_method_id,
                start:
                {
                    [Op.gt]:date
                }
            }
        })

        task.dataValues['Room'] = taskRoom

        let data = await Promise.all(
            taskMedia.map(async (item, key) => {
                
                const Medias = await tbl_medias.findAll({
                    where: {
                        id: item.tbl_media_id
                    }
                })
                
                
                item.setDataValue('tbl_category_media_id', Medias[0].tbl_category_media_id)
                item.setDataValue('tbl_method_id', Medias[0].tbl_method_id)
                item.setDataValue('name', Medias[0].name)
                item.setDataValue('type', Medias[0].type)
                item.setDataValue('file', Medias[0].file)
                item.setDataValue('image', Medias[0].image)
                item.setDataValue('duration', Medias[0].duration)
                item.setDataValue('description', Medias[0].description)
                item.setDataValue('points', Medias[0].points)
                return item;
            }))
            task.dataValues['Medias'] = data


        res.json({
            data: task
        });

    } catch(error) {
        console.error('You have a error in getTask:', error)
        res.status(500).json({
            non_field_errors: 'Something went wrong'
        });
    }
}

export async function getChallenge(req, res) {
    try {
        const now = DateTime.now()
        const date = now.toString();
        const { idProfile } = req.user
        const lastSession = await tbl_session.findOne({
            attributes: [ 'id', 'date', 'week' ],
            where: { state: 1, date: { [Op.lt]: date } },
            include: {
                model: tbl_profile,
                as: 'ProfilePatient',
                attributes: [ ['id', 'idProfilePatient'] ],
                where: { state: 1, id: idProfile  }
            },
            order: [['date', 'DESC']]
        })

        // console.log('')
        // console.log('')
        // console.log('lastSession')
        // console.log(lastSession)
        // console.log('')
        // console.log('')

        let tasks = []
        const challenges = []

        if (lastSession !== null) {

            tasks = await tbl_session_task.findAll({
                where: { state: 1 },
                include: [
                    {
                        model: tbl_session,
                        as: 'Session',
                        attributes: [ 'id', 'date', 'week' ],
                        where: { state: 1, id: lastSession.dataValues.id }
                    }, {
                        model: tbl_task,
                        as: 'Task',
                        attributes: [ 'id', 'week', 'name', 'description', ['tbl_method_id', 'idMethod'] ],
                        where: { state: 1, week: lastSession.dataValues.week },
                        include: [
                            {
                                model: tbl_task_types,
                                as: 'TaskTypes',
                                where: { state: 1, [Op.or]: [ { alias: 'media' }, { alias: 'room' } ] },
                                attributes: [ 'id', 'name', 'alias' ]
                            },
                            {
                                model: tbl_task_media,
                                as: 'TaskMedia',
                                where: { state: 1 },
                                required: false,
                                include: {
                                    model: tbl_medias,
                                    as: 'Media',
                                    where: { state: 1 },
                                    attributes: [ 'type', 'points' ]
                                }
                            }
                        ]
                    }
                ],
                attributes: [ 'id', 'expiration' ]
            })

            // console.log('')
            // console.log('')
            // console.log('tasks')
            // console.log(tasks.length)
            // console.log('')
            // console.log('')

            const countMovies = tasks.reduce((acc, el) => {
                if (el.Task.dataValues.TaskMedia.dataValues.Media.dataValues.type === 1)
                    acc++
                return acc
            }, 0)

            const countAudios = tasks.reduce((acc, el) => {
                if (el.Task.dataValues.TaskMedia.dataValues.Media.dataValues.type === 2)
                    acc++
                return acc
            }, 0)

            for(let task of tasks) {

                // console.log('')
                // console.log('')
                // console.log('task')
                // console.log(task)
                // console.log('')
                // console.log('')

                if (task.Task.dataValues.TaskTypes.dataValues.alias === 'media') {

                    // console.log('')
                    // console.log('')
                    // console.log('media')
                    // console.log(task.Task.TaskMedia.Media)
                    // console.log('')
                    // console.log('')

                    const taskType = task.Task.dataValues.TaskMedia.dataValues.Media.dataValues.type
                    const media_profile = await tbl_medias.findAll({
                        where: { state: 1, id: task.Task.dataValues.TaskMedia.dataValues.tbl_media_id },
                        include: {
                            model: tbl_media_profile,
                            as: 'onProfile',
                            where: { state: 1, tbl_profile_id: idProfile, done: { [Op.lt] : 95 } },
                            attributes: [ 'done' ]
                        },
                        attributes: [ 'id' ]
                    })
                    const countComplete = media_profile.reduce((acc, el) => acc++, 0)
                    const count = (taskType === 1) ? countMovies - countComplete : countAudios - countComplete
                    let nametype= ''
                    if (taskType==1){
                        nametype='Video'
                    }
                    else
                    {
                        nametype='Audio'
                    }
                    challenges.push({
                        count,
                        point: (count * Number(task.Task.dataValues.TaskMedia.dataValues.Media.dataValues.points)),
                        type: nametype,
                        week: lastSession.dataValues.week,
                        date: lastSession.dataValues.date
                    })
                }

                if (task.Task.dataValues.TaskTypes.dataValues.alias === 'room') {

                        const taskType = task.Task.dataValues.TaskMedia.dataValues.Media.dataValues.type
                        const rooms = await tbl_room.findAll({
                            where: {
                                state: 1,
                                // start: { [Op.gt]: startRoom }
                            },
                            include: {
                                model: tbl_chat,
                                as: 'Chat',
                                where: { state: 1 },
                                attributes: [],
                                include: {
                                    model: tbl_chat_participants,
                                    as: 'ChatParticipant',
                                    where: { state: 1, tbl_profile_id: idProfile },
                                    attributes: []
                                }
                            },
                            attributes: [ 'id', 'title', 'start', 'end', 'rules', 'participants', 'image' ]
                        })

                        // console.log('')
                        // console.log('')
                        // console.log('rooms')
                        // console.log(rooms)
                        // console.log('')
                        // console.log('')

                        const countComplete = rooms.reduce((acc, el) => acc++, 0)
                        const count = (taskType === 1) ? countMovies - countComplete : countAudios - countComplete
                        let nametype= ''
                        if (taskType==1){
                            nametype='Video'
                        }
                        else
                        {
                            nametype='Audio'
                        }
                        challenges.push({
                            count,
                            point: (count * Number(task.Task.dataValues.TaskMedia.dataValues.Media.dataValues.points)),
                            type: nametype
                        })
                }
            }
        }

        res.json( { data: challenges } )

    } catch(error) {
        console.error('You have a error in getListChangelles:', error)
        res.status(500).json({
            non_field_errors: 'Something went wrong'
        });
    }
}