import Chat from 'twilio/lib/rest/Chat';

const AccessToken = require('twilio').jwt.AccessToken;
const VideoGrant = AccessToken.VideoGrant;
const Sequelize = require('sequelize');
const {isValidBase64Img, s3UploadBase64Image} = require('./../helpers')

const {
    tbl_profile,
    tbl_room,
    tbl_chat,
    tbl_method,
    tbl_thematic,
    tbl_chat_participants,
    tbl_room_participants,
    tbl_room_note,
    tbl_avatar
} = require('../models')
const {Op} = require('sequelize');
const {DateTime} = require('luxon');

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;

const client = require('twilio')(accountSid, authToken);

export async function getListRoomByProfile(req, res) {
    try {
        const {idRole} = req.user
        let whereQuery = {
            state: 1
        }
        if (req.query.search) {
            whereQuery.title = {[Op.iLike]: '%' + req.query.search + '%'}
        }
        if (req.query.idThematic) {
            whereQuery.tbl_thematic_id = req.query.idThematic
        }
        if (req.query.idMethod) {
            whereQuery.tbl_method_id = req.query.idMethod
        }
        if (idRole === 1) {
            const now = DateTime.fromISO(DateTime.now(), {locale: 'es-ES', zone: 'UTC'})
            const date = now.plus({hour: -5, minutes: -50}).toString();
            whereQuery.start = {[Op.gt]: date}
        }
        const room = await tbl_room.findAll({
            where: whereQuery,
            order: [['start', 'ASC']],
            include: [
                {
                    model: tbl_chat,
                    as: 'Chat',
                    attributes: ['id'],
                    where: {
                        state: 1,
                    },
                    include: {
                        model: tbl_chat_participants,
                        as: 'ChatParticipant',
                        attributes: [
                            'id',
                            ['tbl_chat_id', 'idChat'],
                            ['tbl_profile_id', 'idProfile']
                        ],
                        where: {
                            'tbl_profile_id': req.user.idProfile,
                            'state': 1
                        },
                        required: false
                    }
                },
                {
                    model: tbl_method,
                    as: 'Method',
                    attributes: [
                        'id',
                        'name'
                    ]
                },
                {
                    model: tbl_thematic,
                    as: 'Thematic',
                    attributes: [
                        'id',
                        'name'
                    ]
                }
            ],
            attributes: [
                'id',
                'title',
                'start',
                'end',
                'rules',
                'participants',
                'maxParticipants',
                'inscription',
                'image',
                'twilio_uniquename',
                'created_at'
            ]
        });
        let data = await Promise.all(
            room.map(async (item, key) => {
                const n_participants = await tbl_room_participants.count({
                    where: {
                        tbl_room_id: item.id
                    },
                    include: [
                        {
                            model: tbl_profile,
                            as: 'Profile_participant',
                            where: {tbl_role_id: 1},
                            attributes: [],
                            required: true
                        }
                    ]
                })
                const psychologist = await tbl_room_participants.findAll({
                    where: {
                        tbl_room_id: item.id
                    },
                    include: [
                        {
                            model: tbl_profile,
                            as: 'Profile_participant',
                            where: {tbl_role_id: 2},
                            attributes: []
                        }
                    ],
                    attributes: [
                        [Sequelize.literal('"Profile_participant"."firstname"'), 'name'],
                        [Sequelize.literal('"Profile_participant"."id"'), 'id']
                    ]
                })
                const participants = await tbl_room_participants.findAll({
                    where: {
                        tbl_room_id: item.id
                    },
                    include: [
                        {
                            model: tbl_profile,
                            as: 'Profile_participant',
                            where: {tbl_role_id: 1},
                            attributes: []
                        }
                    ],
                    attributes: [
                        [Sequelize.literal('"Profile_participant"."firstname"'), 'name'],
                        [Sequelize.literal('"Profile_participant"."id"'), 'id']
                    ]
                })
                item.setDataValue('participants', n_participants)
                item.setDataValue('Participants', participants)
                item.setDataValue('psychologist', psychologist)
                item.setDataValue('status', 'error')
                return item;
            }))

        res.json({
            message: 'Rooms found',
            data: data
        })

    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getRoomDetails(req, res) {
    try {
        const roomId = req.params.id
        const participants = await tbl_room_participants.count({
            where: {
                tbl_room_id: roomId
            }
        })
        const roomDetails = await tbl_room.findOne({
            attributes: ['id', 'title', 'twilio_uniquename', 'start', 'rules', 'image'],
            where: {
                id: roomId,
                state: 1
            }
        });
        roomDetails['participants'] = participants
        res.json({data: roomDetails})
    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getEmotionRooms(req, res) {
    try {
        let to = req.query.to,
            search = req.query.search

        let whereQuery

        // Today, 00:00:00
        const now = DateTime.fromObject({day: DateTime.now().day})

        switch (to) {
            case 'yesterday':

                whereQuery = {
                    start: {
                        [Op.gte]: now.minus({days: 1}).toISO(),
                        [Op.lt]: now.toISO()
                    }
                }

                break;

            case 'week':

                const weekday = now.weekday

                whereQuery = {
                    start: {
                        // Day month - ( Day week - 1 )
                        [Op.gte]: now.plus({days: (1 - weekday)}).toISO(),
                        // Day month + ( 7 - Day week ) + 1
                        [Op.lt]: now.plus({days: (8 - weekday)}).toISO()
                    }
                }

                break;

            case 'month':

                whereQuery = {
                    start: {
                        [Op.gte]: DateTime.fromObject({day: 1}).toISO(),
                        [Op.lt]: DateTime.fromObject({day: 1, month: (now.month + 1)}).toISO()
                    }
                }

                break;

            case 'quarter':

                const quarter = Math.ceil(now.month / 3)

                whereQuery = {
                    start: {
                        [Op.gte]: DateTime.fromObject({day: 1, month: (3 * quarter - 2)}).toISO(),
                        [Op.lt]: DateTime.fromObject({day: 1, month: (3 * quarter)}).plus({months: 1}).toISO()
                    }
                }

                break;

            // case 'year':

            //     whereQuery = {
            //         date: {
            //             [Op.gte]: DateTime.fromObject({day: 1, month: 1}).toISO(),
            //             [Op.lt]: DateTime.fromObject({day: 1, month: 1, year: (now.year + 1)}).toISO()
            //         }
            //     }

            //     break;

            case 'custom':

                whereQuery = {
                    start: {
                        [Op.gte]: DateTime.fromISO(req.query.c_from).toISO(),
                        [Op.lt]: DateTime.fromISO(req.query.c_to).plus({days: 1}).toISO()
                    }
                }

                break;

            default:

                whereQuery = {}

                break;
        }

        if (search) {

            whereQuery.title = {
                [Op.iLike]: '%' + search + '%'
            }
        }

        const emotion_rooms = await tbl_room.findAll({
            where: whereQuery,
            include: {
                model: tbl_room_participants,
                where: {
                    tbl_profile_participant_id: req.user.idProfile,
                    state: 1
                },
                attributes: []
            },
            attributes: [
                'id',
                'title',
                'start',
                'end',
                'image'
            ]
        });

        return res.json({
            message: 'Rooms found',
            rooms: emotion_rooms
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function showEmotionRoom(req, res) {

    try {

        const emotion_room = await tbl_room.findByPk(req.params.id, {
            include: [{
                model: tbl_room_participants,
                where: {
                    tbl_profile_participant_id: req.user.idProfile
                },
                attributes: []
            }, {
                model: tbl_chat,
                as: 'Chat',
                attributes: ['id']
            }],
            attributes: [
                'id',
                'title',
                'start',
                'end',
                'image',
                'twilio_uniquename',
                'state'
            ]
        })

        if (emotion_room) {

            const profileAttributes = [
                'id',
                'firstname',
                'lastname'
            ]

            const p_patients = tbl_profile.findAll({
                where: {
                    tbl_role_id: 1
                },
                include: {
                    model: tbl_room_participants,
                    as: 'RoomParticipants',
                    where: {
                        tbl_room_id: emotion_room.id
                    },
                    attributes: []
                },
                attributes: profileAttributes
            })

            const p_psychologists = tbl_profile.findAll({
                where: {
                    tbl_role_id: 2
                },
                include: {
                    model: tbl_room_participants,
                    as: 'RoomParticipants',
                    where: {
                        tbl_room_id: emotion_room.id
                    },
                    attributes: []
                },
                attributes: profileAttributes
            })

            const p_note = tbl_room_note.findOne({
                where: {
                    tbl_room_id: emotion_room.id,
                    tbl_profile_psychologist_id: req.user.idProfile
                }
            })

            let [patients, psychologists, note] = await Promise.all([p_patients, p_psychologists, p_note])

            return res.json({
                message: 'Emotion room found',
                emotion_room,
                patients,
                psychologists,
                note
            })
        }

        res.status(404).json({
            message: 'Emotion room not found'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function twilioListRooms(req, res) {

    try {

        const rooms = await client.video.rooms.list({})

        res.json({
            message: 'Rooms found',
            rooms
        })

    } catch (error) {
        console.error('You have a error in twilioListRooms:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function twilioCreateRoom(req, res, type, nameRoom, participants) {

    try {

        let room_type = type

        switch (room_type) {
            case 'group':
            case 'go':

                break;

            default:

                room_type = 'go'

                break;
        }

        const room = await client.video.rooms.create({
            type: room_type,
            uniqueName: nameRoom,
            maxParticipants: participants
        })

        return room
        /*res.json({
            message: 'Rooms created',
            room
        })*/

    } catch (error) {
        console.error('You have a error in twilioCreateRoom:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function twilioUpdateRoom(req, res) {

    try {

        const room = await client
            .video
            .rooms(req.params.sid)
            .update({
                status: 'completed',
            })

        res.json({
            message: 'Rooms updated',
            room
        })

    } catch (error) {
        console.error('You have a error in twilioUpdateRoom:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function postRoom(req, res) {
    try {
        const {isValid, buffer, ext} = isValidBase64Img(req.body.image)

        if (!isValid) {

            return res.status(422).json({
                message: 'The given data was invalid.',
                errors: {
                    image: [
                        "The image must be a valid base64"
                    ]
                }
            })
        }

        req.body.image = {
            buffer,
            ext
        }
        let imageUploaded = await s3UploadBase64Image('uploads', req.body.image)
        console.log('')
        console.log('')
        console.log(imageUploaded.data.location)
        console.log('')
        console.log('')
        const image = imageUploaded.data.Location
        const participants = req.body.participants
        const sessionId = await tbl_room.findOne({
            attributes: ['id'],
            order: [['id', 'DESC']]
        })
        const idSession = sessionId.id + 1
        const nameRoom = 'room-r-' + idSession
        const room = await tbl_room.create({
            id: idSession,
            title: req.body.tittle,
            tbl_thematic_id: req.body.idThematic,
            tbl_method_id: req.body.idMethod,
            start: req.body.start,
            end: req.body.end,
            rules: req.body.rules,
            participants: participants,
            maxParticipants: participants,
            image,
            twilio_uniquename: nameRoom
        });
        await tbl_room_participants.create({
            tbl_room_id: idSession,
            tbl_profile_participant_id: req.body.idPsychologist1
        });
        await tbl_room_participants.create({
            tbl_room_id: idSession,
            tbl_profile_participant_id: req.body.idPsychologist2
        });
        const chat = await tbl_chat.create({
            tbl_room_id: idSession,
            start: req.body.start,
            helpdesk: 0
        });
        await tbl_chat_participants.create({
            tbl_chat_id: chat.id,
            tbl_profile_id: req.body.idPsychologist1
        });
        await tbl_chat_participants.create({
            tbl_chat_id: chat.id,
            tbl_profile_id: req.body.idPsychologist2
        });
        const type = 'group'

        let twilio = await twilioCreateRoom(req, res, type, nameRoom, participants)


        res.json({
            data: room, twilio
        });
    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function postEnterRoom(req, res) {
    try {

        res.json({
            message: 'You entered the room'
        })

    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function postSignupRoom(req, res) {
    try {

        const room_id = req.params.id
        const idProfile = req.user.idProfile
        const room = await tbl_room.findByPk(room_id)

        if (room) {

            const [chat, chat_created] = await tbl_chat.findOrCreate({
                where: {
                    tbl_room_id: room_id
                }
            })

            const chat_participant = await tbl_chat_participants.findOne({
                where: {
                    tbl_chat_id: chat.id,
                    tbl_profile_id: req.user.idProfile
                }
            })

            if (!chat_participant) {

                await tbl_chat_participants.create({
                    tbl_chat_id: chat.id,
                    tbl_profile_id: req.user.idProfile
                })
                await tbl_room_participants.create({
                    tbl_room_id: room_id,
                    tbl_profile_participant_id: req.user.idProfile
                })
            }

            return res.json({
                message: 'Subscribed to room'
            })
        }

        res.status(404).json({
            message: 'Room not found'
        })

    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function deleteUnsubscribeRoom(req, res) {
    try {

        const room_id = req.params.id

        const room = await tbl_room.findByPk(room_id)

        if (room) {

            const chat = await tbl_chat.findOne({
                where: {
                    tbl_room_id: room_id
                }
            })

            if (chat) {

                await tbl_chat_participants.destroy({
                    where: {
                        tbl_chat_id: chat.id,
                        tbl_profile_id: req.user.idProfile
                    }
                })

                await tbl_room_participants.destroy({
                    where: {
                        tbl_room_id: room_id,
                        tbl_profile_participant_id: req.user.idProfile
                    }
                })

            }

            return res.json({
                message: 'Unsubscribed from room'
            })
        }

        res.status(404).json({
            message: 'Room not found'
        })

    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function closedRoom(req, res) {
    try {
        const closedRoom = await tbl_room.update({
                state: req.body.state
            },
            {
                where: {
                    id: req.body.idRoom,
                }
            });
        res.json({
            message: "Room Closed!!"
        });
    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function twilioAccessToken(req, res) {
    try {
        const config = require('../config/twilio.js');
        let identity = req.user.firstname;
        if (req.body.name) {
            identity = req.body.name
        }
        const videoGrant = new VideoGrant({
            room: req.body.room,
        });
        const token = new AccessToken(
            config.twilioAccountSid,
            config.twilioApiKey,
            config.twilioApiSecret,
            {identity: identity}
        );
        token.addGrant(videoGrant);

        res.json({
            message: "Token created",
            data: {
                token: token.toJwt()
            }
        });
    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getParticipants(req, res) {
    try {
        const profileAttributes = [
            'id',
            'firstname',
            'lastname'
        ]

        const participants = await tbl_profile.findAll({
            where: {
                tbl_role_id: 1
            },
            include: [
                {
                    model: tbl_room_participants,
                    as: 'RoomParticipants',
                    where: {
                        tbl_room_id: req.params.id
                    },
                    attributes: []
                },
                {
                    model: tbl_avatar,
                    as: 'Avatar',
                    attributes: ['image']
                }],
            attributes: profileAttributes
        })
        return res.json({
            message: 'Emotion room found',
            data: participants
        })
    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function addRoomNote(req, res) {
    try {

        const {note, priority} = req.body
        if (note) {
            let priorityVal = 0
            if (priority)
                priorityVal = priority

            console.log("aki llega: " + req.user.idProfile)
            const currentNote = await tbl_room_note.findOne({
                where: {
                    tbl_room_id: req.params.id,
                    tbl_profile_psychologist_id: req.user.idProfile
                }
            })

            if (currentNote) {
                await tbl_room_note.update(
                    {
                        notes: note,
                        priority: priorityVal
                    }, {
                        where: {
                            id: currentNote.id
                        }
                    })
            } else {
                await tbl_room_note.create(
                    {
                        tbl_room_id: req.params.id,
                        tbl_profile_psychologist_id: req.user.idProfile,
                        notes: note,
                        priority: priorityVal,
                        state: 1
                    })
            }

            res.status(200).json({
                message: 'Note registered'
            })
        } else {
            res.status(200).json({
                message: 'Nothing to do'
            });
        }
    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function putEmotionRooms(req, res) {

    try {

        console.log(req.params.id)
        res.status(200).json({
            message: 'updated'
        });
    } catch (error) {
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}