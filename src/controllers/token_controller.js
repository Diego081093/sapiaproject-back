const {
    tbl_profile,
    tbl_user,
    tbl_avatar
} = require('../models')
import jwt from 'jsonwebtoken'
export async function createToken(req, res, usuarioID, username) {
    try {
        const profile = await tbl_profile.findOne({
            where: {
                tbl_user_id: usuarioID,
                state:1
            },
            include:[
            {
                model: tbl_user,
                as:'User',
                attributes: [
                    'id',
                    'username'
                ]
            }]
        })
        const body = {
            id : usuarioID,
            idProfile: profile.id,
            username : username,
            slug: profile.slug,
            firstname: profile.firstname,
            email: profile.email,
            phone: profile.phone,
            birthday: profile.birthday,            
        }
        const token = await jwt.sign({ user: body }, 'globoazul_token')
        return token;
    } catch (error) {
        res.status(500).json({
            message: 'Something went error'
        })
    }
}