import { Op } from "sequelize";
import { DateTime } from "luxon";

const {
    tbl_protocol,
    tbl_protocol_process,
    tbl_profile
} = require('../models')
const MottivaConstants = require('./../helpers')

export async function getAllProtocol(req, res){
    try {

        const processId = req.query.processId,
            date = req.query.date

        let whereProtProcess = {},
            whereDate = {}

        const profileAttributes = [
            'id',
            'slug',
            'firstname',
            'lastname',
            'state'
        ]

        if( processId ){

            whereProtProcess = {
                tbl_prot_process_id: processId
            }
        }

        if( date ){

            whereDate = {
                created_at: {
                    [Op.gte]: date,
                    [Op.lt]: DateTime.fromFormat(date, 'yyyy-MM-dd').plus({days: 1}).toISO()
                }
            }
        }

        const protocols = await tbl_protocol.findAll({
            where: {
                state: 1,
                ...whereProtProcess,
                ...whereDate
            },
            include: [
                {
                    model: tbl_protocol_process,
                    as: 'ProtocolProcess',
                    attributes: [
                        'id',
                        'name'
                    ]
                },
                {
                    model: tbl_profile,
                    as: 'Responsible',
                    attributes: profileAttributes
                },
                {
                    model: tbl_profile,
                    as: 'Approver',
                    attributes: profileAttributes
                }
            ],
            attributes: [
                'id',
                'code',
                'goal',
                'state',
                'createdAt'
            ]
        })

        res.json({
            message: 'Protocols found',
            data: protocols
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getProtocol(req, res){

    try{

        const profileAttributes = [
            'id',
            'slug',
            'firstname',
            'lastname',
            'state'
        ]

        const protocol = await tbl_protocol.findOne({
            where: {
                id: req.params.id,
                state: 1
            },
            include: [
                {
                    model: tbl_protocol_process,
                    as: 'ProtocolProcess',
                    attributes: [
                        'id',
                        'name'
                    ]
                },
                {
                    model: tbl_profile,
                    as: 'Responsible',
                    attributes: profileAttributes
                },
                {
                    model: tbl_profile,
                    as: 'Approver',
                    attributes: profileAttributes
                }
            ],
            attributes: [
                'id',
                'code',
                'goal',
                'introduction',
                'procedure',
                'state',
                'createdAt'
            ]
        })

        if( protocol ){

            return res.json({
                message: 'Protocol found',
                data: protocol
            })
        }

        res.status(404).json({
            message: 'Protocol not found'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function createProtocol(req, res){

    try{

        if( req.user.idRole == MottivaConstants.HELPDESK_ROLE ){

            const {
                code,
                goal,
                responsibleId,
                processId,
                introduction,
                procedure
            } = req.body

            await tbl_protocol.create({
                code,
                tbl_prot_process_id: processId,
                goal,
                tbl_profile_responsible_id: responsibleId,
                introduction,
                procedure
            })

            return res.json({
                message: 'Protocol created'
            })
        }

        res.status(401).json({
            message: 'Access denied with provided credentials'
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}