'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING(45)
      },
      start: {
        type: Sequelize.DATE
      },
      end: {
        allowNull: true,
        type: Sequelize.DATE
      },
      rules: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      participants: {
        type: Sequelize.INTEGER
      },
      maxParticipants: {
        type: Sequelize.INTEGER
      },
      inscription: {
        type: Sequelize.STRING
      },
      image: {
        type: Sequelize.STRING
      },
      twilio_uniquename: {
        type: Sequelize.STRING(100)
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_rooms');
  }
};