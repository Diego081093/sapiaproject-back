'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_profiles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_users',
          key: 'id'
        }
      },
      slug: {
        type: Sequelize.STRING(250)
      },
      firstname: {
        type: Sequelize.STRING(250)
      },
      profile: {
        type: Sequelize.TEXT
      },
      image: {
        type: Sequelize.STRING
      },
      email: {
        allowNull: true,
        type: Sequelize.STRING(45)
      },
      phone: {
        allowNull: true,
        type: Sequelize.STRING(9)
      },
      local: {
        allowNull: true,
        type: Sequelize.STRING(200)
      },
      program: {
        allowNull: true,
        type: Sequelize.STRING(200)
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_profiles');
  }
};