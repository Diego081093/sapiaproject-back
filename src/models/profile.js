'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      

      
      this.hasOne(models.tbl_chat_participants, { //le brindo a este modelo
        foreignKey: 'tbl_profile_id',
        as:'ChatParticipant'                          //este campo
      })
      
      this.hasMany(models.tbl_room_participants, {
        foreignKey: 'tbl_profile_participant_id',
        as:'RoomParticipants'
      })
    }
  };
  tbl_profile.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_user_id: DataTypes.INTEGER,
    slug: DataTypes.STRING,
    firstname: DataTypes.STRING,
    profile: DataTypes.TEXT,
    image: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    local: DataTypes.STRING,
    program: DataTypes.STRING,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_profile',
  });
  return tbl_profile;
};