'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

// Load env vars
require('dotenv').config()

const basename = path.basename(__filename);
const config = require('../config/database.js');
const db = {};

let sequelize;
if(process.env.NODE_ENV === 'development'){
  sequelize = new Sequelize(config.development.database, config.development.username, config.development.password, {
    logging: true,
    dialect: 'postgres',
    query: true,
    host: config.development.host
  })
}else{
  sequelize = new Sequelize(config.production)
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
module.exports = db;