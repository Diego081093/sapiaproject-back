'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_room_participants extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_room, {
        foreignKey: 'tbl_room_id',
        as:'Room'
      })
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_participant_id',
        as:'Profile_participant'
      })
    }
  };
  tbl_room_participants.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_room_id: DataTypes.INTEGER,
    tbl_profile_participant_id: DataTypes.INTEGER,
    state: DataTypes.INTEGER,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_room_participants',
  });
  return tbl_room_participants;
};