'use strict';

const faker = require('faker')
const { tbl_room } = require('../models')

async function roomAll() {
  return await tbl_room.findAll()
}

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const date = new Date()
    const rooms = await roomAll()

    const data = [...Array(10)].map((room, i) => {
      let start = faker.date.past(0, date)
      let end = faker.date.future(0, start)
      let startRoom = faker.date.between(start, end)
      let endRoom = new Date(startRoom)
      endRoom.setDate(end.getDate() + faker.random.number({ min: 10, max: 25 }))
      return ({
        id: rooms.length + i + 1,
        title: faker.name.title(),
        start: startRoom,
        end: faker.date.between(startRoom, endRoom),
        rules: faker.lorem.slug(),
        participants: 0,
        maxParticipants: 0,
        inscription: '¿!?',
        image: 'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_1.png',
        twilio_uniquename: faker.random.alphaNumeric(30),
        state: 1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      })
    })

    return queryInterface.bulkInsert('tbl_rooms', data)
  },

  down: async (queryInterface, Sequelize) => {
  }
};
