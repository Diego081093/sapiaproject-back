const env = process.env

module.exports = {
    development: {
        dialect: 'postgres',
        host:   'localhost',
        database: 'mottiva',
        username: 'postgres',
        password: '12345678'
    },
    production: {
        dialect: 'postgres',
        host: env.DB_HOST,
        database: 'mottiva',
        username: 'postgres',
        password: '26;Q~E_5Ez!_us5N'
    }
}
