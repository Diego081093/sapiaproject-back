const AWS = require('aws-sdk');

// Load env vars
require('dotenv').config()

const env = process.env

AWS.config.update({
    region: 'us-east-2',
    credentials: {
        accessKeyId: env.AWS_ACCESS_KEY_ID,
        secretAccessKey: env.AWS_SECRET_ACCESS_KEY
    }
})

const s3 = new AWS.S3()

exports.s3 = s3

exports.Bucket = env.AWS_BUCKET