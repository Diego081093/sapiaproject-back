import {initSocket, initSocketHttps} from "./socket";
require('dotenv').config()
import "@babel/polyfill";
import app from "./app";


const fs = require('fs');
const https = require('https');

async function main() {

    const node_port = process.env.NODE_PORT
    if(process.env.NODE_ENV === 'development'){
        //develop
        await app.listen(node_port);
        console.log('Server on port ' + node_port);
        await initSocket()
    }else{
        //production
        https.createServer({
            key: fs.readFileSync('../../cert/privkey.pem', 'utf8'),
            cert: fs.readFileSync('../../cert/cert.pem', 'utf8'),
            ca: fs.readFileSync('../../cert/chain.pem', 'utf8')
        }, app).listen(node_port, function(){
            console.log("My HTTPS server listening on port " + node_port + "...");
        });
        await initSocketHttps()
    }

}

main();