import passport from 'passport'
import { ExtractJwt } from 'passport-jwt'
const LocalStrategy = require('passport-local').Strategy
const JWTStrategy = require('passport-jwt').Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt
// import Users, { isValidPassword } from '../models/users_model'
const { tbl_user } = require('../models')
const { isValidPassword } = require('../helpers')

passport.use('signin', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
}, async (username, password, done) => {
    try {
        const user = await tbl_user.findOne({ where: { username } })
        if (!user)
            return done(null, false, { message: 'User not found' })
        const validate = await isValidPassword(password, user.password)
        if (!validate)
            return done(null, false, { message: 'Wrong password' })
        return done(null, user, { message: 'Login successfully' })
    } catch (err) {
        done(err, false, 'Something went wrong')
    }
}))

passport.use(new JWTStrategy({
    secretOrKey: 'globoazul_token',
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
}, async (token, done) => {
    try {
        return done(null, token.user)
    } catch(err) {
        console.error('You have error passport.use JWTStrategy: ', err)
    }
}))