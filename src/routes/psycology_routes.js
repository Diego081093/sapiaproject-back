import {Router} from 'express';
import passport from 'passport'
const router = Router();

import { getPsycology, updatePsycology, updatePhoto } from '../controllers/profiles_controller'
import { getListPsicology } from '../controllers/profile_attribute_controller'
import { createBlocked } from '../controllers/blocked_controller'

//patient
router.get('/', passport.authenticate('jwt', { session: false }), getPsycology);
router.get('/list', passport.authenticate('jwt', { session: false }), getListPsicology);
router.post('/:id/block', passport.authenticate('jwt', { session: false }), createBlocked);
router.put('/', passport.authenticate('jwt', { session: false }), updatePsycology);
router.put('/photo', passport.authenticate('jwt', { session: false }), updatePhoto);
export default router;