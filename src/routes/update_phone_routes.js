import {Router} from 'express';
import passport from 'passport';
const router = Router();

import { updatePhone } from '../controllers/users_controller'

//api/rol
// router.get('/', getListRole);
router.put('/', passport.authenticate('jwt', { session: false }), updatePhone );
// router.delete('/',deleteRole);

export default router;