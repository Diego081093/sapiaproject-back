import {Router} from 'express';
import passport from 'passport'
const router = Router();

import {
    setSessionAttribute
} from '../controllers/session_attribute_controller'

router.post('/:id', passport.authenticate('jwt', { session: false }), setSessionAttribute)

export default router;