import {Router} from 'express';
const router = Router();
import passport from 'passport'
import{ putDiary, clearDiary, getListDateDiary, getDetailDiary, getListDiary, getListDateDiaryDetails, getListDiaryViewPsychologist, seenDiary } from '../controllers/diary_controller'
const {s3Upload} = require('./../helpers')


router.post('/',passport.authenticate('jwt', { session: false }),s3Upload.single('file'),putDiary);
router.get('/dates',passport.authenticate('jwt', { session: false }),getListDateDiary);
router.get('/dates-details',passport.authenticate('jwt', { session: false }),getListDateDiaryDetails);
router.get('/viewPsychologist',passport.authenticate('jwt', { session: false }),getListDiaryViewPsychologist);
router.put('/:id/clear', passport.authenticate('jwt', { session: false }), clearDiary);
router.get('/:id', passport.authenticate('jwt', { session: false }), getDetailDiary);
router.get('/', passport.authenticate('jwt', { session: false }), getListDiary);
router.put('/seen/:id', passport.authenticate('jwt', { session: false }), seenDiary);

export default router;