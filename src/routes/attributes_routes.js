import {Router} from 'express';
const router = Router();
import{getListAttributes} from '../controllers/attributes_controller'


router.get('/',getListAttributes);

export default router;