import {Router} from 'express';
import passport from 'passport'
const router = Router();

import { getListPsychologist } from '../controllers/profiles_controller'


//patient
router.get('/list', passport.authenticate('jwt', { session: false }), getListPsychologist);
export default router;