import { Router } from 'express';
import passport from 'passport';
// import { getListProfile } from '../controllers/profiles_controller';
const router = Router();
import { deleteUser, updateTutorial } from '../controllers/users_controller' ;

// /api/users/
router.delete('/:id', passport.authenticate('jwt', { session: false }), deleteUser)
router.put('/tutorial', passport.authenticate('jwt', { session: false }), updateTutorial);
//router.get('/getProfile',getProfile);
// router.post('/', createUser);
// router.get('/:idprofile', getListProfile)
// router.put('/tokenDevice', updateTokenDevice)
//router.get('/avatar2',getAvatar);

export default router;