import {Router} from 'express';
import {getAll} from '../controllers/statistics_controller';
import passport from "passport";
const router = Router();

router.get('/', passport.authenticate('jwt', { session: false }), getAll);

export default router;