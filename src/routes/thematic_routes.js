import passport from 'passport'
import { Router } from 'express'
import { getListThematic } from '../controllers/thematic_controller'

const router = Router();

router.get('/', passport.authenticate('jwt', { session: false }), getListThematic)

export default router