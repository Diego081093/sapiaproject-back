import { Router } from 'express';
const router = Router();
import {getListMedias,getListMediasPrueba, getMedia, postFavoriteMedia, postDesactivateFavoriteMedia, watchLaterMedia,postDoneMedia} from '../controllers/medias_controller'
import passport from "passport";


router.get('/', passport.authenticate('jwt', { session: false }), getListMedias);
router.get('/:id', passport.authenticate('jwt', { session: false }), getMedia);
router.post('/favorite', passport.authenticate('jwt', { session: false }), postFavoriteMedia);
router.post('/desactivate-favorite', passport.authenticate('jwt', { session: false }), postDesactivateFavoriteMedia);
router.post('/:id/watch-later', passport.authenticate('jwt', { session: false }), watchLaterMedia);
router.post('/done', passport.authenticate('jwt', { session: false }), postDoneMedia);


export default router;