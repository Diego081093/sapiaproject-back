import {Router} from 'express';
import passport from 'passport'
const router = Router();

import {
    getListTask,
    getListTaskByWeek,
    getTasksPatient,
    getTask,
    getChallenge,
    createSessionTask
} from '../controllers/task_controller'

//api/rol
// router.get('/', getListRole);
router.get('/', passport.authenticate('jwt', { session: false }), getListTask);

router.get('/week/:week', passport.authenticate('jwt', { session: false }), getListTaskByWeek);

router.get('/patient',passport.authenticate('jwt', { session: false }), getTasksPatient );

router.get('/challenges', passport.authenticate('jwt', { session: false }), getChallenge)

router.get('/:id', passport.authenticate('jwt', { session: false }), getTask);

router.post('/sessionTask', passport.authenticate('jwt', { session: false }), createSessionTask );

export default router;