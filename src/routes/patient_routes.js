import {Router} from 'express';
const router = Router();

import {createPatient} from '../controllers/profiles_controller'

//patient
router.post('/', createPatient);

export default router;