import {Router} from 'express';
const router = Router();
import passport from 'passport'
import{ listAvailability, listPsycologistAvailability, storeAvailability } from '../controllers/availability_controller'

//avatar
router.get('/', passport.authenticate('jwt', { session: false }), listAvailability);
router.get('/psychologist', passport.authenticate('jwt', { session: false }), listPsycologistAvailability);
router.post('/', passport.authenticate('jwt', { session: false }), storeAvailability);

export default router;

