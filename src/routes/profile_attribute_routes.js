import {Router} from 'express';
const router = Router();

import { getProfileAttribute, setProfileAttribute } from '../controllers/profile_attribute_controller'

//api/profile
router.get('/:id', getProfileAttribute);
router.post('/:id', setProfileAttribute);

export default router;