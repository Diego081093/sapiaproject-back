import passport from 'passport'
import { Router } from 'express'
import { twilioListRooms, twilioCreateRoom, twilioUpdateRoom, twilioAccessToken } from '../controllers/room_controller'

const router = Router();

router.get('/video/rooms', passport.authenticate('jwt', { session: false }), twilioListRooms)
router.post('/video/rooms', passport.authenticate('jwt', { session: false }), twilioCreateRoom)
router.put('/video/rooms/:sid', passport.authenticate('jwt', { session: false }), twilioUpdateRoom)
router.post('/accessToken', passport.authenticate('jwt', { session: false }), twilioAccessToken)

export default router