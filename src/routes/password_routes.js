import {Router} from 'express';

const router = Router();

import { updatePassword } from '../controllers/users_controller'

//api/rol
// router.get('/', getListRole);
router.put('/', updatePassword);
// router.delete('/',deleteRole);

export default router;