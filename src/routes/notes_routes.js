import {Router} from 'express';
import passport from 'passport'
const router = Router();

import { getNotesList, disableNote } from '../controllers/session_controller'

//patient
router.get('/', passport.authenticate('jwt', { session: false }), getNotesList);
router.put('/:id/disable', passport.authenticate('jwt', { session: false }), disableNote);

export default router;