import {Router} from 'express';
import passport from 'passport'
const router = Router();

import { getEmotionRooms, showEmotionRoom, getParticipants, putEmotionRooms } from '../controllers/room_controller'

router.get('/', passport.authenticate('jwt', { session: false }), getEmotionRooms);
router.get('/:id', passport.authenticate('jwt', { session: false }), showEmotionRoom);
router.get('/participant/:id', passport.authenticate('jwt', { session: false }), getParticipants);
router.put('/:id', passport.authenticate('jwt', { session: false }), putEmotionRooms);

export default router;