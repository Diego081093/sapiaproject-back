import { Router } from 'express';
const router = Router();
import {authDni} from '../controllers/users_controller'


router.post('/', authDni);

export default router;