import { Router } from 'express';
const router = Router();
import {updateTokenDevice} from '../controllers/users_controller' ;
import passport from "passport";


router.put('/',passport.authenticate('jwt', { session: false }), updateTokenDevice)
//router.get('/avatar2',getAvatar);

export default router;