import {Router} from 'express';
import passport from 'passport'
const router = Router();

import{ getAllProtocolProcess } from '../controllers/protocol_process_controller'


router.get('/', passport.authenticate('jwt', { session: false }), getAllProtocolProcess);

export default router;