import {Router} from 'express';
import passport from 'passport'
const router = Router();

import {
    getListProfile,
    getProfile,
    getPointsUsed,
    createProfile,
    updateProfile,
    deleteProfile,
    updateProfileOnly,
    updateProfilePhoto,
    updateProfilePhone,
    updateProfileAvatar
} from '../controllers/profiles_controller'
import {
    createToken
} from '../controllers/token_controller'

//api/profile
router.get('/', getListProfile);
router.get('/me', passport.authenticate('jwt', { session: false }), getProfile);
router.get('/points-used', passport.authenticate('jwt', { session: false }), getPointsUsed);
router.post('/', createProfile);
router.post('/token', createToken);
router.put('/', passport.authenticate('jwt', { session: false }), updateProfile);
router.put('/:id', passport.authenticate('jwt', { session: false }), updateProfileOnly);
router.put('/:id/photo', passport.authenticate('jwt', { session: false }), updateProfilePhoto);
router.put('/:id/phone', passport.authenticate('jwt', { session: false }), updateProfilePhone);
router.put('/:id/avatar', passport.authenticate('jwt', { session: false }), updateProfileAvatar);
router.delete('/:id', deleteProfile);

export default router;