import {Router} from 'express';
import passport from 'passport'
import {getHelpdesk, getChat, getChatSession, getAllHelpdesk, joinHelpdesk, getChatPrivate} from "../controllers/chat_controller";
import {postChatMessage} from "../controllers/chat_message_controller";
const router = Router();

router.get('/helpdesk', passport.authenticate('jwt', { session: false }), getHelpdesk);
router.post('/helpdesk', passport.authenticate('jwt', { session: false }), joinHelpdesk);
router.get('/helpdesk/list', passport.authenticate('jwt', { session: false }), getAllHelpdesk);
router.get('/private', passport.authenticate('jwt', { session: false }), getChatPrivate);
router.get('/:id', passport.authenticate('jwt', { session: false }), getChat);
router.get('/sessions/:id', passport.authenticate('jwt', { session: false }), getChatSession);
router.post('/message', passport.authenticate('jwt', { session: false }), postChatMessage);

export default router;