import {Router} from 'express';
import passport from 'passport'
const router = Router();

import {
    getSessionCases,
    getSessionCaseDetails,
    getListSessionCases
} from '../controllers/session_controller'

router.get('/', passport.authenticate('jwt', { session: false }), getListSessionCases);
router.get('/:id', passport.authenticate('jwt', { session: false }), getSessionCases);
router.get('/:id/details', passport.authenticate('jwt', { session: false }), getSessionCaseDetails);

export default router;