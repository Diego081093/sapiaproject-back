import { watchLaterMedia } from '../controllers/medias_controller'
import passport from 'passport';
import { Router } from 'express';
const router = Router();

router.post('/:id', passport.authenticate('jwt', { session: false }), watchLaterMedia);

export default router;