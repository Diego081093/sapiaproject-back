import express, { json } from "express";
import bodyParser from "body-parser";
import morgan from "morgan";

//importing routes
import userRoutes from './routes/user_routes'
import profileroutes from './routes/profile_routes'
import profileAttributeRoutes from './routes/profile_attribute_routes'
import roleRoutes from './routes/role_routes';
import secretQuestionRoutes from './routes/secret_question_routes';
import userSecretQuestionRoutes from './routes/user_secret_question_routes';
import avatarRoutes from './routes/avatar_routes';
import availabilityRoutes from './routes/availability_routes';
import passwordRoutes from "./routes/password_routes";
import questionRoutes from './routes/question_routes';
import sessionRoutes from './routes/session_routes';
import sessionAttributeRoutes from './routes/session_attribute_routes'
import trainingRoutes from './routes/training_routes';
import emotionRoomRoutes from './routes/emotion_room_routes';
import verificationRoutes from './routes/verification_routes';
import loginRoutes from './routes/login_routes';
import patientRoutes from './routes/patient_routes';
import patientByRoutes from './routes/patient_by_routes';
import dniRoutes from "./routes/dni_routes";

import mediasRoutes from "./routes/medias_routes";
import watchLaterRoutes from "./routes/watch_later_routes";
import favoriteRoutes from "./routes/favorite_routes";
import testRoutes from "./routes/test_routes";
import uploadRoutes from "./routes/upload_routes";
import updatePhoneRoutes from "./routes/update_phone_routes";

import psycologyRoutes from "./routes/psycology_routes";
import psychologistRoutes from "./routes/psychologist_routes";
import therapyRoutes from "./routes/therapy_routes";
import noteRoutes from "./routes/notes_routes";

import roomRoutes from "./routes/room_routes";
import thematicRoutes from "./routes/thematic_routes";

import twilioRoutes from "./routes/twilio_routes";

import categoryRoutes from "./routes/category_routes";

import methodRoutes from "./routes/method_routes";

import pollRoutes from "./routes/poll_routes";

import benefitRoutes from "./routes/benefit_routes";

import scheduleRoutes from "./routes/schedule_routes";

import notificationRoutes from "./routes/notification_routes";
import taskRoutes from "./routes/task_routes";
import tokenDeviceRoutes from "./routes/token_device_routes";
import blockedRoutes from "./routes/blocked_routes";
import diaryRoutes from "./routes/diary_routes";
import patientsRoutes from "./routes/patients_routes";
import sessionCasesRoutes from "./routes/session_cases_routes";
import chatRoutes from "./routes/chat_routes";
import statisticsRoutes from "./routes/statistics_routes";
import institutionRoutes from "./routes/institution_routes";

import attributesRoutes from "./routes/attributes_routes";

import protocolRoutes from "./routes/protocol_routes";
import protocolProcessRoutes from "./routes/protocol_process_routes";
import specialtyRoutes from './routes/specialty_routes'

require('./middleware/auth')

var cors = require('cors')

//initializacion
const app= express();
app.use(cors());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    next()
})

//middlewares
app.use(morgan('dev'));
//app.use(json());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(express.json())
//app.use(express.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}))

app.use('/uploads', express.static('uploads'))

app.use('/user', userRoutes)
app.use('/profile', profileroutes)
app.use('/profileAttribute', profileAttributeRoutes)
app.use('/role', roleRoutes); //1

app.use('/verification',verificationRoutes);
app.use('/dni', dniRoutes);
app.use('/password', passwordRoutes);
app.use('/avatar', avatarRoutes); //2
app.use('/availability', availabilityRoutes);
app.use('/secretQuestion', secretQuestionRoutes);
app.use('/patient', patientRoutes);
app.use('/patient-by', patientByRoutes);
app.use('/question',questionRoutes);

app.use('/login',loginRoutes);
app.use('/userSecretQuestion', userSecretQuestionRoutes);

app.use('/patients', patientsRoutes);
app.use('/sessions',sessionRoutes);
app.use('/sessionsAttribute',sessionAttributeRoutes);
app.use('/trainings',trainingRoutes);
app.use('/emotions-room',emotionRoomRoutes);


app.use('/media',mediasRoutes);
app.use('/watch-later',watchLaterRoutes);
app.use('/favorite',favoriteRoutes);

app.use('/test',testRoutes);
app.use('/send-image',uploadRoutes);

app.use('/update-phone', updatePhoneRoutes)

app.use('/psycology', psycologyRoutes);
app.use('/psychologist', psychologistRoutes);
app.use('/therapies', therapyRoutes);
app.use('/notes',noteRoutes);

app.use('/room', roomRoutes);
app.use('/thematic', thematicRoutes);

app.use('/twilio', twilioRoutes);

app.use('/category', categoryRoutes);

app.use('/method', methodRoutes);

app.use('/poll', pollRoutes);

app.use('/benefits', benefitRoutes);
app.use('/task', taskRoutes);

app.use('/schedule', scheduleRoutes);

app.use('/notification', notificationRoutes);
app.use('/tokenDevice', tokenDeviceRoutes);
app.use('/blocked', blockedRoutes);
app.use('/diary', diaryRoutes);
app.use('/sessionCases', sessionCasesRoutes);
app.use('/statistics', statisticsRoutes);
app.use('/institution', institutionRoutes);
app.use('/attributes', attributesRoutes);
app.use('/protocols-processes', protocolProcessRoutes);
app.use('/protocols', protocolRoutes);

app.use('/chat', chatRoutes);

app.use('/specialties', specialtyRoutes)

export default app;